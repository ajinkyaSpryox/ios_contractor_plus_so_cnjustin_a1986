//
//  SocialLoginDetail.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 10/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//


import Foundation

// MARK: - SocialLoginDetail
struct SocialLoginDetail: Codable {
    let success, message: String
    let data: SocialLoginDetailData
}

// MARK: - DataClass
struct SocialLoginDetailData: Codable {
    let token: String
    let userDetails: UserDetails
    let isOtpVerified: String
    let accessDetails: AccessDetails?
    let expiryDate: Bool
    let isProUser: Int
    let role: String
    let settings: Settings?


    enum CodingKeys: String, CodingKey {
        case token
        case settings
        case userDetails = "user_details"
        case isOtpVerified = "is_otp_verified"
        case accessDetails = "access_details"
        case role
        case expiryDate = "expiry_date"
        case isProUser = "is_pro"
    }
}

// MARK: - AccessDetails
struct AccessDetails: Codable {
    let view: [String]
}

// MARK: - UserDetails
struct UserDetails: Codable {
    let id: Int
    let fullName, firstName, lastName, email: String
    let profilePicture: String
    let mobileNumber: String
    let isdCode: String

    enum CodingKeys: String, CodingKey {
        case id
        case fullName = "full_name"
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case profilePicture = "profile_picture"
        case mobileNumber = "mobile_no"
        case isdCode = "isd_code"
        
    }
}

// MARK: - Settings
struct Settings: Codable {
    let brandingSetting: BrandingSetting?
    let estimateSetting: EstimateSetting?
    let invoiceSetting: InvoiceSetting?
    let mileageLogSetting: MileageLogSetting?
    let quickBookSetting: QuickBookSetting?
    let timeClockSetting: TimeClockSetting?
    let shoppingSetting: ShoppingSetting?

    enum CodingKeys: String, CodingKey {
        case brandingSetting = "branding_setting"
        case estimateSetting = "estimate_setting"
        case invoiceSetting = "invoice_setting"
        case mileageLogSetting = "mileage_log_setting"
        case quickBookSetting = "quick_book_setting"
        case timeClockSetting = "time_clock_setting"
        case shoppingSetting = "shopping_setting"
    }
}

// MARK: - BrandingSetting
struct BrandingSetting: Codable {
    let whatToDisplay: String
    let whereToDisplay: [String]
    let logo: String
    let companyName, email, phone: String
    let countriesID, stateID: Int?
    let city, zipCode, address1, address2: String
    let showCoBrand, useAuthentication: Bool?
    let connectionSecurity, smtpHostname: String
    let smtpUser, smtpPassword: String
    let smtpPort: Int?

    enum CodingKeys: String, CodingKey {
        case whatToDisplay = "what_to_display"
        case whereToDisplay = "where_to_display"
        case logo
        case companyName = "company_name"
        case email, phone
        case countriesID = "countries_id"
        case stateID = "state_id"
        case city
        case zipCode = "zip_code"
        case address1 = "address_1"
        case address2 = "address_2"
        case showCoBrand = "show_co_brand"
        case useAuthentication = "use_authentication"
        case connectionSecurity = "connection_security"
        case smtpHostname = "smtp_hostname"
        case smtpPort = "smtp_port"
        case smtpUser = "smtp_user"
        case smtpPassword = "smtp_password"
    }
}

// MARK: - EstimateSetting
struct EstimateSetting: Codable {
    let supplyMarkup, supplyMarkupUnit, defaultLabourRate: String

    enum CodingKeys: String, CodingKey {
        case defaultLabourRate = "default_labour_rate"
        case supplyMarkup = "supply_markup"
        case supplyMarkupUnit = "supply_markup_unit"
    }
}

// MARK: - InvoiceSetting
struct InvoiceSetting: Codable {
    let chargeClientsTaxOn, defaultTaxRate: String
    let schedule: [Schedule]

    enum CodingKeys: String, CodingKey {
        case chargeClientsTaxOn = "charge_clients_tax_on"
        case defaultTaxRate = "default_tax_rate"
        case schedule
    }
}

// MARK: - Schedule
struct Schedule: Codable {
    let duration, amount, unit: String
}

// MARK: - MileageLogSetting
struct MileageLogSetting: Codable {
    let trackRoutes, trackDrivingTime, trackDrivingSpeed: Bool?

    enum CodingKeys: String, CodingKey {
        case trackRoutes = "track_routes"
        case trackDrivingTime = "track_driving_time"
        case trackDrivingSpeed = "track_driving_speed"
    }
}

// MARK: - QuickBookSetting
struct QuickBookSetting: Codable {
    let isSyncFinancialData: Bool?

    enum CodingKeys: String, CodingKey {
        case isSyncFinancialData = "is_sync_financial_data"
    }
}

// MARK: - ShoppingSetting
struct ShoppingSetting: Codable {
    let whereIShopIDS: [String]

    enum CodingKeys: String, CodingKey {
        case whereIShopIDS = "where_i_shop_ids"
    }
}

// MARK: - TimeClockSetting
struct TimeClockSetting: Codable {
    let requireEmployeeClockinClockout: Bool?
    let acceptableDistance: Int?

    enum CodingKeys: String, CodingKey {
        case requireEmployeeClockinClockout = "require_employee_clockin_clockout"
        case acceptableDistance = "acceptable_distance"
    }
}
