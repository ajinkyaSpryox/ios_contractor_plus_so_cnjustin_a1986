//
//  Webservice.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 05/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD
import SwiftyJSON

class Webservices: NSObject {
    

    static let MainBaseUrl = "http://contractorplus.dev.ganniti.com/api/"
    static let register = MainBaseUrl + "register"
    static let verify_otp = MainBaseUrl + "verify-otp"
    static let send_otp = MainBaseUrl + "send-otp"
    static let login = MainBaseUrl + "login"
    static let setMobileNumber = MainBaseUrl + "set-mobile-no"
    
    static let social_login = MainBaseUrl + "social-login"
    
    static let forget_password_verify_otp = MainBaseUrl + "forget-password-verify-otp"
    static let add_client = MainBaseUrl + "add-client"
    static let list_countries = MainBaseUrl + "list-countries"
    static let list_states = MainBaseUrl + "list-states"
    static let list_client_for_estimate = MainBaseUrl + "list-client-for-estimate"
    static let delete_client = MainBaseUrl + "delete-client"
    static let get_client_details = MainBaseUrl + "get-client-details"
    static let send_email_to_customer = MainBaseUrl + "send-mail-to-customer"
    
    //MARK: URLS for Estimates
    static let list_estimate = MainBaseUrl + "list-estimates"
    static let create_estimate = MainBaseUrl + "create-estimate"
    static let delete_estimate = MainBaseUrl + "delete-estimate"
    static let get_estimate_details = MainBaseUrl + "get-estimate-details"
    static let create_edit_estimate_area = MainBaseUrl + "create-area"
    static let list_estimate_areas = MainBaseUrl + "list-estimate-areas"
    static let delete_area = MainBaseUrl + "delete-area"
    static let get_search_supplies_items = MainBaseUrl + "get-supplies"
    static let upload_image_to_server = MainBaseUrl + "add-image"
    static let delete_image_from_server = MainBaseUrl + "delete-image"
    static let create_task = MainBaseUrl + "create-task"
    static let list_estimate_area_tasks = MainBaseUrl + "list-estimate-area-tasks"
    static let delete_task = MainBaseUrl + "delete-task"
    static let save_estimate = MainBaseUrl + "save-estimate"
    
    //MARK: URLS FOR INVOICES
    static let list_invoice = MainBaseUrl + "list-invoices"
    static let create_invoice = MainBaseUrl + "create-invoice"
    static let delete_invoice = MainBaseUrl + "delete-invoice"
    static let invoice_details = MainBaseUrl + "invoice-details"
    
    //MARK: URLS FOR SCHEDULED JOBS
    static let list_scheduled_jobs = MainBaseUrl + "list-scheduled-jobs"
    static let list_team_members = MainBaseUrl + "list-team-members"
    static let schedule_job = MainBaseUrl + "schedule-job"
    static let delete_scheduled_job = MainBaseUrl + "delete-scheduled-job"
    static let scheduled_job_details = MainBaseUrl + "scheduled-job-details"
    
    //MARK: URLS FOR POST INSPECTION
    static let list_post_inspection = MainBaseUrl + "list-post-inspection"
    static let post_inspection_details = MainBaseUrl + "post-inspection-details"
    static let set_post_inspection = MainBaseUrl + "set-post-inspection"
    
    //MARK: URLS USER ACCOUNTS & PROFILE
    static let update_profile = MainBaseUrl + "update-profile"
    static let change_password = MainBaseUrl + "change-password"
    
    //MARK: URLS FOR SETTINGS Module
    static let add_update_estimate_settings = MainBaseUrl + "add-update-estimation-settings"
    static let add_update_mileage_log_settings = MainBaseUrl + "add-update-mileage-log-settings"
    static let list_supply_stores = MainBaseUrl + "list-supply-stores"
    static let add_shopping_settings = MainBaseUrl + "add-shopping-setting"
    static let add_edit_speciality_item = MainBaseUrl + "add-edit-speciality-items"
    static let list_speciality_items = MainBaseUrl + "list-speciality-items"
    static let add_update_timeclock_settings = MainBaseUrl + "add-update-timeclock-settings"
    static let add_update_branding_setting = MainBaseUrl + "add-update-branding-setting"
    
    //MARK: URLS SHOPPING LIST MODULE
    static let list_shopping_lists = MainBaseUrl + "list-shopping-lists"
    static let create_edit_shopping_list = MainBaseUrl + "create-shopping-list"
    static let delete_shopping_list = MainBaseUrl + "delete-shopping-list"
    
    //MARK: URLS FOR TIME CLOCK MODULE
    static let list_time_clock = MainBaseUrl + "list-time-clock"
    static let set_clock_in_out = MainBaseUrl + "set-clock-in-out"
    
    //MARK: URLS FOR ADD TEAM MEMBERS MODULE
    static let get_configurations = MainBaseUrl + "configurations"
    static let add_team_member = MainBaseUrl + "add-team-member"
    
//  http://contractorplus.dev.ganniti.com/api/register
    
//print(MainBaseUrl)
    

}
public typealias APICallback = ( _ response: Any?, _ errorStr: String?) -> Void

class API {
    
    class func callAPI(_ urlStr: String,headers: [String:Any] ,params: [String:Any], showHud: Bool = true, completion: @escaping APICallback) {
        
        
                  if showHud{
                    
                       SVProgressHUD.show()
                       SVProgressHUD.setBackgroundColor(UIColor.white)
                   
                  }
        
        
                let manager = Alamofire.SessionManager.default
                manager.session.configuration.timeoutIntervalForRequest = 120
                manager.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default,headers: (headers as! HTTPHeaders)).responseJSON
                    { (response:DataResponse) in

                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                            //print("Data: \(utf8Text)") // original server data as UTF8 string
                            if let newData = utf8Text.data(using: String.Encoding.utf8){
                                
                                do{
    
                                    let json = try JSON(data: newData)
                                    completion(json, nil)
    
                                   
                                }catch _ as NSError {
                                    // error
                                     completion(nil, "Error")
                                }
                            }
                            //    self.mainTblView.reloadData()
                        }
                        //   self.ProfileTblView.reloadData()
                }
        }

}



//API.getDirectData(Urls.HealthCard, params: dict, showHud: false) { (response, errorStr) in
//
//    guard errorStr == nil else {
//        SVProgressHUD.dismiss()
//        self.errorAlert(message: errorStr!)
//        return
//    }
//
//    if let str = response as? String {
//        print(str)
//        self.healthWebView.loadHTMLString(str, baseURL: nil)
//    }
//}
