//
//  IAPServiceHelper.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 25/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation
import StoreKit

protocol IAPServiceHelperDelegate: class {
    func inAppPurchaseProductsLoaded()
}

class IAPServiceHelper: NSObject {
    static let instance = IAPServiceHelper()
    
    var products = [SKProduct]()
    var productIds = Set<String>()
    var productRequest = SKProductsRequest()
    weak var delegate: IAPServiceHelperDelegate?
    
    func loadProducts() {
        productIdToStringSet()
        requestProducts(forIds: productIds)
    }
    
    func productIdToStringSet() {
        productIds.insert(INAPPConstants.IAP_AUTORENEWABLE_YEARLY_SUBSCRIPTION)
    }
    
    func requestProducts(forIds ids: Set<String>) {
        productRequest.cancel()
        productRequest = SKProductsRequest(productIdentifiers: ids)
        productRequest.delegate = self
        productRequest.start()
    }
    
}

//MARK: SK PRODUCT REQUEST DELEGATE IMPLEMENTATION
extension IAPServiceHelper: SKProductsRequestDelegate {
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.products = response.products
        
        if products.count == 0 {
            requestProducts(forIds: productIds)
        }
        else {
            delegate?.inAppPurchaseProductsLoaded()
            print(products[0].localizedTitle)
        }
        
    }

    
}
