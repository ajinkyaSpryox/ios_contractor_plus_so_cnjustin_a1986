//
//  GenericWebservice.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 27/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import Foundation
import Alamofire

class GenericWebservice {
    
    static let instance = GenericWebservice()
    let connectivityManager = NetworkReachabilityManager()
    let manager = Alamofire.SessionManager()
    
    func getServiceData <T: Codable> (url: String, method: HTTPMethod, parameters: [String:Any], encodingType: ParameterEncoding, headers: [String:String], completion: @escaping (T?, String?) ->()) {
        
        if connectivityManager?.isReachable ?? false {
            //print("Network is Reachable")
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 10
            
            manager.request(url, method: method, parameters: parameters, encoding: encodingType, headers: headers).responseJSON { (response) in
                
                if response.error != nil {
                    //debugPrint(response.error?.localizedDescription ?? "")
                    completion(nil, response.error?.localizedDescription)
                    debugPrint(response.error.debugDescription)
                }
                else {
                    guard let data = response.data else {return completion(nil, response.error?.localizedDescription)}
                    let decoder = JSONDecoder()
                    
                    do{
                        let returnedResponse = try decoder.decode(T.self, from: data)
                        completion(returnedResponse, nil)
                    }catch{
                        debugPrint(error)
                        completion(nil, error.localizedDescription)
                    }
                    
                }
                
            }
            
        }
        else {
            //print("Network Not Reachable")
            completion(nil, "Network Not Reachable")
        }
        
    }
    
}

