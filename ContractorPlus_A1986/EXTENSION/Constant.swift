//
//  Constant.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 05/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import Foundation
import UIKit

let userDefaults = UserDefaults.standard

let AppName = "Contractor Plus"
let appXApiKey = "WFIgqXp8Qyr0AF3wIVGglSKLN7qgw7EtPu5V7mWUbIaoSMGIUppTgaCKqaWh7Gb5Lyrf8L2A177"
let user_id = "user_id"
let isd_code = "isd_code"
let mobile_no = "mobile_no"
let token = "token"
let role = "role"
let is_otp_verified = "is_otp_verified"
let last_name = "last_name"
let first_name = "first_name"
let email = "email"
let full_name = "full_name"
let user_profile_photo = "user_profile_photo"
//let id = "id"
let NetworkError = "Internet connection might be offilne, Please try after some times."
let textfieldEmptyError = "Field's can't be empty."
let UIcolorRed : CGColor = UIColor(red:0.86, green:0.19, blue:0.19, alpha:1.0).cgColor //DC3131
let customeLightGrayColor = UIColor(red:0.85, green:0.86, blue:0.88, alpha:1.0) //DADBE0

let noUserDefaultPhotoUrl = "http://contractorplus.dev.ganniti.com/no-user.png"

let fcmTokenString = "fcmTokenString"
let isFacebookLogin = "isFacebookLogin"
let googleSignInClientId = "540286578116-2gj4fnjj6ts6qqmg52a1i5u7bh7jsv7p.apps.googleusercontent.com"

let formattedDateString = "dd-MM-yyyy"
let serverDateFormatString = "yyyy-MM-dd HH:mm:ss"

struct STORYBOARDNAMES {
    static let LOGIN_STORYBOARD = UIStoryboard(name: "LOGIN", bundle: nil)
    static let ESTIMATES_STORYBOARD = UIStoryboard(name: "Estimates", bundle: nil)
    static let INVOICES_STORYBOARD = UIStoryboard(name: "Invoices", bundle: nil)
    static let CLIENTS_STORYBOARD = UIStoryboard(name: "Clients", bundle: nil)
    static let SETTINGS_STORYBOARD = UIStoryboard(name: "Settings", bundle: nil)
    static let COMMON_VIEW_STORYBOARD = UIStoryboard(name: "CommonView", bundle: nil)
    static let IN_APP_PURCHASE_STORYBOARD = UIStoryboard(name: "InAppPurchase", bundle: nil)
    static let SCHEDULE_STORYBOARD = UIStoryboard(name: "Schedule", bundle: nil)
    static let POST_INSPECTION_STORYBOARD = UIStoryboard(name: "PostInspection", bundle: nil)
    static let USER_PROFILE_STORYBOARD = UIStoryboard(name: "UserProfile", bundle: nil)
    static let SHOPPING_LIST_STORYBOARD = UIStoryboard(name: "ShoppingList", bundle: nil)
    static let TIME_CLOCK_STORYBOARD = UIStoryboard(name: "TimeClock", bundle: nil)
    static let ADD_TEAM_MEMBER_STORYBOARD = UIStoryboard(name: "TeamMember", bundle: nil)
}

enum SocialLogin: String {
    case FACEBOOK = "facebook"
    case GOOGLE = "google"
}

struct INAPPConstants {
    static let IAP_AUTORENEWABLE_YEARLY_SUBSCRIPTION = "com.spryox.ContractorPlus.ios.autorenewable.yearly"
}

struct EstimateSettingsSavedDefaults {
    static let defaultLabourRate = "defaultLabourRate"
    static let supplyMarkupRate = "supplyMarkupRate"
    static let supplyMarkupUnit = "supply_markup_unit"
}

struct MileageSettingsSavedDefaults {
    static let trackRoutes = "trackRoutes"
    static let trackDrivingTime = "trackDrivingTime"
    static let trackDrivingSpeed = "trackDrivingSpeed"
}

struct TimeClockSettingsSavedDefaults {
    static let requiredEmployeeClockInOut = "requiredEmployeeClockInOut"
    static let acceptableDistance = "acceptableDistance"
}


