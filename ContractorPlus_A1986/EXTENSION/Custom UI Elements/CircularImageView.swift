//
//  CircularImageView.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 25/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

@IBDesignable
class CircularImageView: UIImageView {

    @IBInspectable
    var imageViewBorderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = imageViewBorderWidth
        }
    }
    
    @IBInspectable
    var imageViewBorderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = imageViewBorderColor.cgColor
        }
    }
    
    @IBInspectable
    var imageViewCornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = imageViewCornerRadius
        }
    }
    
}
