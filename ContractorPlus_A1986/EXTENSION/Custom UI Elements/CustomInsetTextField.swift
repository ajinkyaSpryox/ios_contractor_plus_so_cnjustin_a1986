//
//  CustomTextField.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 26/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

@IBDesignable
class CustomInsetTextField: UITextField {
    
    private var offSet: CGFloat = 0.0
    private var padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView() {
        let placeholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        self.attributedPlaceholder = placeholder
    }

    @IBInspectable
    var textFieldBorderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = textFieldBorderWidth
        }
    }
    
    @IBInspectable
    var textFieldBorderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = textFieldBorderColor.cgColor
        }
    }
    
    @IBInspectable
    var textFieldCornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = textFieldCornerRadius
        }
    }
    
}

extension CustomInsetTextField {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
}
