//
//  CustomView.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 25/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

@IBDesignable
class CustomView: UIView {

    @IBInspectable var viewborderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = viewborderWidth
        }
    }
    
    @IBInspectable var viewborderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = viewborderColor.cgColor
        }
    }
    
    @IBInspectable var viewCornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = viewCornerRadius
        }
    }
    
}
