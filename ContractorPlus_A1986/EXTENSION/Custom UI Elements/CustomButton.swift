//
//  CustomButton.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 25/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

@IBDesignable
class CustomButton: UIButton {
    
    @IBInspectable
    var buttonBorderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = buttonBorderWidth
        }
    }
    
    @IBInspectable
    var buttonBorderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = buttonBorderColor.cgColor
        }
    }
    
    @IBInspectable
    var buttonCornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = buttonCornerRadius
        }
    }
    
}
