//
//  Configuration.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 09/04/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - Configuration
struct Configuration: Codable {
    let success, message: String
    let data: ConfigurationData
}

// MARK: - DataClass
struct ConfigurationData: Codable {
    var modulesPermissions: [ModulesPermission]

    enum CodingKeys: String, CodingKey {
        case modulesPermissions = "modules_permissions"
    }
}

// MARK: - ModulesPermission
struct ModulesPermission: Codable {
    let codeName, name: String
    var permissions: [Permission]

    enum CodingKeys: String, CodingKey {
        case codeName = "code_name"
        case name, permissions
    }
}

// MARK: - Permission
//struct Permission: Codable {
//    let codeName, name: String
//    var status: Bool
//
//    enum CodingKeys: String, CodingKey {
//        case codeName = "code_name"
//        case name, status
//    }
//}

class Permission: Codable {
    let codeName, name: String
    var status: Bool

    enum CodingKeys: String, CodingKey {
        case codeName = "code_name"
        case name, status
    }
}

