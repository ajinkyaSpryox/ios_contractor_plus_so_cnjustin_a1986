//
//  AddTeamMemberVC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 09/04/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class AddTeamMemberVC: UIViewController {
    
    @IBOutlet weak var permissionView: UIView!
    @IBOutlet weak var employeeNameTextField: CustomIconTextField!
    @IBOutlet weak var employeeEmailTextField: CustomIconTextField!
    @IBOutlet weak var payTextField: CustomIconTextField!
    @IBOutlet weak var employeeImageView: UIImageView!
    
    //Variables
    var estimatePermissionsArr: [Permission] = []
    var invoicePermissionsArr: [Permission] = []
    var postInvoicePermissionsArr: [Permission] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    
    private func initialSetup() {
        title = "ADD TEAM MEMBER"
        permissionView.layer.cornerRadius = 16
        permissionView.layer.masksToBounds = true
        
        guard let addTeamMemberPermissionVC = storyboard?.instantiateViewController(withIdentifier: "AddTeamMemberPermissionVC") as? AddTeamMemberPermissionVC else {return}
        
        //TODO: - GET INDIVIDUAL PERMISSIONS AS PER SELECTION - COMPLETION HANDLER
        addTeamMemberPermissionVC.addPermissionCompletion = { [weak self] (permissions, type) in
            
            guard let self = self else {return}
            
            switch type {
            
            case "estimates":
                self.estimatePermissionsArr = permissions
            
            case "invoices":
                self.invoicePermissionsArr = permissions
            
            case "post_inspections":
                self.postInvoicePermissionsArr = permissions
            
            default:
                break
            }
            
        }
        
        //TODO: GET NOTIFIED ON ADD TEAM MEMBER BUTTON TAP - COMPLETION
        addTeamMemberPermissionVC.addTeamMemberBtnTappedCompletion = { [weak self] () in
            guard let self = self else {return}
            self.addTeamMember()
        }
        
        add(childViewController: addTeamMemberPermissionVC, inView: permissionView)
        
    }
    
    
    
    @IBAction func addImageBtnTapped(_ sender: UIButton) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }
            else {
                self.view.makeToast("Camera Not Available", duration: 2.0, position: .bottom)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
        
    }

}

//MARK: - TEXT FIELD DELEGATE IMPLEMENTATION
extension AddTeamMemberVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

//MARK: - IMAGE PICKER IMPLEMENTATION
extension AddTeamMemberVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        employeeImageView.image = image
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
}


//MARK: - SERVICE IMPLEMENTATION
extension AddTeamMemberVC {
    
    func addTeamMember() {
        
        guard employeeNameTextField.text != "", employeeEmailTextField.text != "", payTextField.text != "" else {
            view.makeToast("Values Cant be empty", duration: 2.0, position: .bottom)
            return
        }
        
        guard let imageData = employeeImageView.image!.jpegData(compressionQuality: 0.5) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        
        let header = ["Content-Type": "multipart/form-data",
                      "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)",
            "Accept": "application/json"]
        
        var parameters: [String: Any] = [
            "user_id": "\(UserDefaults.standard.string(forKey: user_id)!)",
            "employee_name": employeeNameTextField.text!,
            "email_address": employeeEmailTextField.text!,
            "pay": payTextField.text!,
            "profile_pic": "file.jpeg",
        ]
        
        estimatePermissionsArr.enumerated().forEach { (index, value) in
            parameters["permissions[estimate]\([index])"] = value.codeName
        }

        invoicePermissionsArr.enumerated().forEach { (index, value) in
            parameters["permissions[invoice]\([index])"] = value.codeName
        }

        postInvoicePermissionsArr.enumerated().forEach { (index, value) in
            parameters["permissions[post_inspection]\([index])"] = value.codeName
        }
        
        
        Alamofire.upload(multipartFormData: { (multipartData) in
            
            for(key, value) in parameters {
                multipartData.append("\(value)".data(using: .utf8)!, withName: key)
            }
            
            multipartData.append(imageData, withName: "profile_pic", fileName: "file.jpeg", mimeType: "image/jpeg")
            
            }, to: Webservices.add_team_member,
               method: .post,
               headers: header,
               encodingCompletion: {result in
                
                switch result {
                    
                case .success(let upload, _, _):
                    
                    self.view.makeToastActivity(.center)
                    
                    upload.uploadProgress(closure: { (progress) in
                        //print("Upload Progress: \(progress.fractionCompleted)")
                        //let progressPercent = Int(progress.fractionCompleted * 100)
                        //print(progressPercent)
                    })
                    
                    upload.responseJSON { response in
                        //print(response.result.value)
                        
                        guard let data = response.data else {return}
                        let decoder = JSONDecoder()
                        
                        do{
                            let returnedResponse = try decoder.decode(DeleteEstimate.self, from: data)
                            
                            if returnedResponse.success == "true" {
                                print(returnedResponse.message)
                                DispatchQueue.main.async {
                                    self.view.hideToastActivity()
                                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                                }
                                
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.view.hideToastActivity()
                                    print(returnedResponse.message)
                                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                                }
                            }
                            
                        }
                        catch let jsonError {
                            //print(jsonError.localizedDescription)
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.view.makeToast(jsonError.localizedDescription, duration: 2.0, position: .bottom)
                            }
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    self.view.hideToastActivity()
                    self.view.makeToast(encodingError.localizedDescription, duration: 2.0, position: .bottom)
                }

                
        })
        
    }
    
}
