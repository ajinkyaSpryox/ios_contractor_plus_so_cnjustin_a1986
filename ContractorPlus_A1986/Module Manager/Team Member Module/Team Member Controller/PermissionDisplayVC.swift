//
//  PermissionDisplayVC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 10/04/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class PermissionDisplayVC: UIViewController {
    
    @IBOutlet weak var permissionTableView: UITableView!
    @IBOutlet weak var permissionTableHeight: NSLayoutConstraint!
    
    //Variables
    var permissionsDataArr: [Permission]!
    var selectedPermissionsArr: [Permission] = []
    
    //Completion
    var addPermissionCompletion: ((_ permissionList: [Permission]) -> Void)?
    var addTeamMemberBtnTappedCompletion: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    private func initialSetup() {
        permissionTableView.delegate = self
        permissionTableView.dataSource = self
        permissionTableView.register(UINib(nibName: "PermissionTblCell", bundle: nil), forCellReuseIdentifier: "PermissionTblCell")
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        permissionTableHeight.constant = permissionTableView.contentSize.height
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    @IBAction func addTeamMemberBtnTapped(_ sender: UIButton) {
        guard let addTeamMemberCompletion = addTeamMemberBtnTappedCompletion else {return}
        addTeamMemberCompletion()
    }

}


//MARK: - TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension PermissionDisplayVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return permissionsDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PermissionTblCell") as? PermissionTblCell else {return UITableViewCell()}
        cell.configureCell(permission: permissionsDataArr[indexPath.row], delegate: self, indexpath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

//MARK: - Permission Table Cell Delegate Implementation
extension PermissionDisplayVC: PermissionTblCellDelegate {
    
    func didTapPermissionCheckBoxBtn(permission: Permission, at indexPath: IndexPath) {
        permission.status = !permission.status
        permissionTableView.reloadRows(at: [indexPath], with: .automatic)
        
        if permission.status {
            selectedPermissionsArr.append(permission)
        }
        else {
            if let index = selectedPermissionsArr.lastIndex(where: {$0.status == false}) {
                selectedPermissionsArr.remove(at: index)
            }
        }
        
        guard let addPermissionCompletion = addPermissionCompletion else {return}
        addPermissionCompletion(selectedPermissionsArr)
        
    }
    
}
