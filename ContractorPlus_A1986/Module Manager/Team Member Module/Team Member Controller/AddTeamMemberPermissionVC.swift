//
//  AddTeamMemberPermissionVC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 09/04/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import CarbonKit
import Alamofire
import Toast_Swift

class AddTeamMemberPermissionVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var permissionLabel: UILabel!
    
    //Variables
    var carbonTapSwipeNavigation: CarbonTabSwipeNavigation?
    var menuItems: [String] = []
    var configurationDataArr: [ModulesPermission] = []
    
    //Completion
    var addPermissionCompletion: ((_ permissionList: [Permission], _ forType: String) -> Void)?
    var addTeamMemberBtnTappedCompletion: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    private func initialSetup() {
        serviceCalls()
    }
    
    private func serviceCalls() {
        getConfigurations()
    }
    
}

extension AddTeamMemberPermissionVC: CarbonTabSwipeNavigationDelegate {
    
    
    private func configureCarbonKit() {
        menuItems = configurationDataArr.map({$0.name})
        carbonTapSwipeNavigation = CarbonTabSwipeNavigation(items: menuItems, delegate: self)
        carbonTapSwipeNavigation?.insert(intoRootViewController: self)
        
        guard let carbonTapSwipeNavigation = carbonTapSwipeNavigation else {return}
        carbonTapSwipeNavigation.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            carbonTapSwipeNavigation.view.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            carbonTapSwipeNavigation.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        ])
        carbonTapSwipeNavigation.setTabBarHeight(60)
        carbonTapSwipeNavigation.setIndicatorColor(.systemRed)
        carbonTapSwipeNavigation.setNormalColor(.systemGray, font: UIFont(name: "NotoSans-Regular", size: 18)!)
        carbonTapSwipeNavigation.setSelectedColor(.black, font: UIFont(name: "NotoSans-Bold", size: 18)!)
        carbonTapSwipeNavigation.carbonTabSwipeScrollView.backgroundColor = .white
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        let index = Int(index)
        guard let permissionDisplayVC = storyboard?.instantiateViewController(withIdentifier: "PermissionDisplayVC") as? PermissionDisplayVC else {return UIViewController()}
        
        permissionDisplayVC.permissionsDataArr = configurationDataArr[index].permissions
        
        permissionDisplayVC.addPermissionCompletion = { [weak self] (permissions) in
            guard let self = self else {return}
            guard let addPermissionCompletion = self.addPermissionCompletion else {return}
            addPermissionCompletion(permissions, self.configurationDataArr[index].codeName)
        }
        
        permissionDisplayVC.addTeamMemberBtnTappedCompletion = { [weak self] () in
            guard let self = self else {return}
            guard let addTeamMemberCompletion = self.addTeamMemberBtnTappedCompletion else {return}
            addTeamMemberCompletion()
        }
        
        return permissionDisplayVC
    }
    
    
}

//MARK: - SERVICES IMPLEMENTATION
extension AddTeamMemberPermissionVC {
    
    func getConfigurations() {
        
        let headers = [
            "x-api-key": appXApiKey,
            "Accept": "application/json"
        ]
        
        GenericWebservice.instance.getServiceData(
            url: Webservices.get_configurations,
            method: .post,
            parameters: [String : Any](),
            encodingType: JSONEncoding.default,
            headers: headers) { [weak self] (configuration: Configuration!, errorMessage) in
                
                guard let self = self else {return}
                
                if let error = errorMessage {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(error, duration: 2.0, position: .bottom)
                    }
                }
                else {
                    if let returnedResponse = configuration {
                        
                        if returnedResponse.success == "true" {
                            //Success
                            self.configurationDataArr = returnedResponse.data.modulesPermissions
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.configureCarbonKit()
                            }
                        }
                        else {
                            //Failure
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            }
                        }
                        
                    }
                }
                
        }
        
    }
    
}
