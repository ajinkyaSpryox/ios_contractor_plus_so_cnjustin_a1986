//
//  PermissionTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 09/04/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

protocol PermissionTblCellDelegate: class {
    func didTapPermissionCheckBoxBtn(permission: Permission, at indexPath: IndexPath)
}

class PermissionTblCell: UITableViewCell {
    
    @IBOutlet weak var permissionTitleLbl: UILabel!
    @IBOutlet weak var checkBoxBtn: UIButton!
    
    weak var delegate: PermissionTblCellDelegate?
    var selectedPermission: Permission!
    var selectedIndexPath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(permission: Permission, delegate: PermissionTblCellDelegate, indexpath: IndexPath) {
        
        self.delegate = delegate
        self.selectedPermission = permission
        self.selectedIndexPath = indexpath
        
        permissionTitleLbl.text = permission.name
        
        if permission.status {
            checkBoxBtn.setImage(UIImage(named: "ic_checkboxselected"), for: .normal)
        }
        else {
            checkBoxBtn.setImage(UIImage(named: "ic_checkbox"), for: .normal)
        }
        
    }
    
    @IBAction func permissionCheckBoxBtnTapped(_ sender: UIButton) {
        delegate?.didTapPermissionCheckBoxBtn(permission: selectedPermission, at: selectedIndexPath)
    }
    
}
