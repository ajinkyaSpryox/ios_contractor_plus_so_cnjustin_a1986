

import UIKit

class HomeScreenListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var mainUiView: UIView!
    
    
    @IBOutlet weak var listNameLbl: UILabel!
    @IBOutlet weak var listImgView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
