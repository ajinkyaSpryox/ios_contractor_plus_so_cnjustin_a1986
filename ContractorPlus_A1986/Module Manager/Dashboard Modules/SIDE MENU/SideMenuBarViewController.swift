
import UIKit
import FacebookLogin
import FacebookCore
import GoogleSignIn
import Kingfisher

class SideMenuBarViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{

    
    @IBOutlet weak var sideMenuTblView: UITableView!
    
    var listnameArray = [String]()
    var listslugArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        listnameArray = ["My Account","Contractor Plus PRO","Help","About","Rate the App","Share App","Logout"]
        
        listslugArray = ["My-account.png","Contractor-Plus-PRO.png","Help.png","About.png","Rate-the-App.png","Share--App.png","Logout.png"]
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listnameArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! sideMenuListTableViewCell
        cell.iconImgView.image = UIImage(named: listslugArray[indexPath.row])
        
        cell.listnameLbl.text = listnameArray[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! sideMenuHeaderTableViewCell
        cell.usernameLbl.text = UserDefaults.standard.string(forKey: first_name)
        let profilePicImage = URL(string: UserDefaults.standard.string(forKey: user_profile_photo) ?? "image1")
        cell.profilePicImgView.kf.indicatorType = .activity
        cell.profilePicImgView.kf.setImage(with: profilePicImage, placeholder: UIImage(named: "image1"))
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 0:
            //Navigate to User Profile My Account Screen
            guard let myAccountVc = STORYBOARDNAMES.USER_PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "MyAccountVC") as? MyAccountVC else {return}
            navigationController?.pushViewController(myAccountVc, animated: true)
            
        case 1:
            //Navigate to Pro Upgrade Screen
            guard let upgradeProvc = STORYBOARDNAMES.IN_APP_PURCHASE_STORYBOARD.instantiateViewController(withIdentifier: "UpgradeToProVC") as? UpgradeToProVC else {return}
            navigationController?.pushViewController(upgradeProvc, animated: true)
            
        case 2:
            //Navigate to Add Team Member Screen - Test
            guard let addTeamMemberVC = STORYBOARDNAMES.ADD_TEAM_MEMBER_STORYBOARD.instantiateViewController(withIdentifier: "AddTeamMemberVC") as? AddTeamMemberVC else {return}
            navigationController?.pushViewController(addTeamMemberVC, animated: true)
            
        case 6:
            
            if AccessToken.current != nil {
                //Logout from facebook
                print("Facebook Logout")
                let loginManager = LoginManager()
                loginManager.logOut()
                self.dismiss(animated: true) {
                    logoutUser()
                }
            }
            else if (GIDSignIn.sharedInstance()?.currentUser != nil) {
                //Logout from Google
                print("Google Logout")
                GIDSignIn.sharedInstance()?.signOut()
                self.dismiss(animated: true) {
                    logoutUser()
                }
            }
            else {
                //Normal Logout
                self.dismiss(animated: true) {
                    logoutUser()
                }
                
            }
            
            
        default:
            return
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }
}
