//
//  PostInspectionAreaTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 02/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

protocol PostInspectionAreaTblCellDelegate: class {
    func didTapAddAfterImageBtn(taskDetails: Task, at indexpath: IndexPath, areaIndexPath: IndexPath)
    func didTapDeleteAfterImageBtn(taskIndex: Int, areaIndexPath: IndexPath, afterImageIndexPath: IndexPath)
    func didTapCompletionMarkingBtns(taskDetail: Task, at indexpath: IndexPath, areaIndexPath: IndexPath, completionBtnTag: Int)
    
}

class PostInspectionAreaTblCell: UITableViewCell {
    
    @IBOutlet weak var postInspectionAreaNameLbl: UILabel!
    @IBOutlet weak var postInspectionTaskTblView: UITableView!
    @IBOutlet weak var postInspectionTaskTblHeight: NSLayoutConstraint!
    
    weak var delegate: PostInspectionAreaTblCellDelegate?
    var selectedAreaDetails: Area!
    var selectedAreaIndex: IndexPath!
    
    var postInspectionDetailsData: PostInspectionDetailsData! {
        didSet {
    
            
            postInspectionTaskTblView.register(UINib(nibName: "PostInspectionTaskTblCell", bundle: nil), forCellReuseIdentifier: "PostInspectionTaskTblCell")
            postInspectionTaskTblView.delegate = self
            postInspectionTaskTblView.dataSource = self
            postInspectionTaskTblView.reloadData()
            updateConstraints()
            
        }
    }
    
    override func updateConstraints() {
        postInspectionTaskTblHeight.constant = postInspectionTaskTblView.contentSize.height
        postInspectionTaskTblView.layoutIfNeeded()
        super.updateConstraints()
    }
    
    func configureCell(areaDetails: Area, delegate: PostInspectionAreaTblCellDelegate, indexpath: IndexPath) {
        self.delegate = delegate
        self.selectedAreaDetails = areaDetails
        self.selectedAreaIndex = indexpath
        
        postInspectionAreaNameLbl.text = areaDetails.name
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}

//MARK: POST INSPECTION TASK TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension PostInspectionAreaTblCell: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let tasks = postInspectionDetailsData.areas[self.tag].tasks else {return 0}
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let tasks = postInspectionDetailsData.areas[self.tag].tasks else {return UITableViewCell()}
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PostInspectionTaskTblCell") as? PostInspectionTaskTblCell else {return UITableViewCell()}
        cell.configureCell(taskDetails: tasks[indexPath.row], delegate: self, indexpath: indexPath)
        cell.taskArr = tasks
        cell.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    
}

//MARK: POST INSPECTION TASK TABLE CELL DELEGATE IMPLEMENTATION
extension PostInspectionAreaTblCell: PostInspectionTaskTblCellDelegate {
    
    func didTapCompletionMarkingsBtn(taskDetails: Task, at indexpath: IndexPath, completionButtonTag: Int) {
        delegate?.didTapCompletionMarkingBtns(taskDetail: taskDetails, at: indexpath, areaIndexPath: selectedAreaIndex, completionBtnTag: completionButtonTag)
    }
    
    
    func didTapDeleteAfterImageBtn(taskIndex: Int, afterImageIndexpath: IndexPath) {
        delegate?.didTapDeleteAfterImageBtn(taskIndex: taskIndex, areaIndexPath: selectedAreaIndex, afterImageIndexPath: afterImageIndexpath)
    }
    
    
    func didTapAddAfterImageBtn(taskDetails: Task, at indexpath: IndexPath) {
        delegate?.didTapAddAfterImageBtn(taskDetails: taskDetails, at: indexpath, areaIndexPath: selectedAreaIndex)
    }
    
}
