//
//  ListPostInspectionHeaderTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 02/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class ListPostInspectionHeaderTblCell: UITableViewCell {
    
    @IBOutlet weak var postInspectionCreationDateLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(postInspectionDetails: PostInspectionList) {
        postInspectionCreationDateLbl.text = postInspectionDetails.date
    }
    
}
