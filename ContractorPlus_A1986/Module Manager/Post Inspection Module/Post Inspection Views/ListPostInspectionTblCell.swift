//
//  ListPostInspectionTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 02/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

protocol ListPostInspectionTblCellDelegate : class {
    func didTapEditPostInspectionBtn(postInspection: PostInspection, at indexpath: IndexPath)
    func didTapDeletePostInspectionBtn(postInspection: PostInspection, at indexpath: IndexPath)
    func didTapSendEmailToCustomerBtn(postInspection: PostInspection, at indexpath: IndexPath)
}

class ListPostInspectionTblCell: UITableViewCell {
    
    @IBOutlet weak var bottomOptionsView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var clientAddressLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var postInspectionGeneratorImage: UIImageView!
    @IBOutlet weak var postInspectionGeneratorNameLbl: UILabel!
    
    weak var delegate: ListPostInspectionTblCellDelegate?
    var selectedPostInspection: PostInspection!
    var selectedIndexpath: IndexPath!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func configureCell(postInspectionDetails: PostInspection, delegate: ListPostInspectionTblCellDelegate, indexpath: IndexPath) {
        
        self.delegate = delegate
        self.selectedPostInspection = postInspectionDetails
        self.selectedIndexpath = indexpath
        
        nameLabel.text = postInspectionDetails.clientName
        clientAddressLabel.text = postInspectionDetails.clientAddress
        postInspectionGeneratorNameLbl.text = postInspectionDetails.generateBy.name
        statusLabel.text = postInspectionDetails.status
        statusLabel.textColor = UIColor(hexString: postInspectionDetails.statusColor)
        
        guard let postInspectorImageUrl = URL(string: postInspectionDetails.generateBy.picture) else {return}
        postInspectionGeneratorImage.kf.indicatorType = .activity
        postInspectionGeneratorImage.kf.setImage(with: postInspectorImageUrl, placeholder: UIImage(named: "image1"))
        
    }
    
    @IBAction func editPostInspectionBtnTapped(_ sender: UIButton) {
        delegate?.didTapEditPostInspectionBtn(postInspection: selectedPostInspection, at: selectedIndexpath)
    }
    
    @IBAction func deletePostInspectionBtnTapped(_ sender: UIButton) {
        delegate?.didTapDeletePostInspectionBtn(postInspection: selectedPostInspection, at: selectedIndexpath)
    }
    
    @IBAction func sendEmailToCustomerBtnTapped(_ sender: UIButton) {
        delegate?.didTapSendEmailToCustomerBtn(postInspection: selectedPostInspection, at: selectedIndexpath)
    }
    
}
