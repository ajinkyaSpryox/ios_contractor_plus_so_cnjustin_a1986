//
//  PostInspection_AterPhotos_CollectionCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 03/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

protocol PostInspection_AterPhotos_CollectionCellDelegate: class {
    func didTapDeleteImageBtn(postInspectionAfterImage: Images, at indexpath: IndexPath)
}

class PostInspection_AterPhotos_CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var afterPhotoImageView: UIImageView!
    
    weak var delegate: PostInspection_AterPhotos_CollectionCellDelegate?
    var selectedAfterPostInspectionImage: Images!
    var selectedIndexPath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(postInspectionAfterImage: Images, delegate: PostInspection_AterPhotos_CollectionCellDelegate, indexpath: IndexPath) {
        
        self.delegate = delegate
        self.selectedAfterPostInspectionImage = postInspectionAfterImage
        self.selectedIndexPath = indexpath
        
        guard let imageUrl = URL(string: postInspectionAfterImage.image) else {return}
        afterPhotoImageView.kf.indicatorType = .activity
        afterPhotoImageView.kf.setImage(with: imageUrl, placeholder: UIImage(named: "image1"))
    }
    
    @IBAction func deleteImageBtnTapped(_ sender: UIButton) {
        delegate?.didTapDeleteImageBtn(postInspectionAfterImage: selectedAfterPostInspectionImage, at: selectedIndexPath)
    }

}
