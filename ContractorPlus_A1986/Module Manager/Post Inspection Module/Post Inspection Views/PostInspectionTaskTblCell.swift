//
//  PostInspectionTaskTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 02/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

protocol PostInspectionTaskTblCellDelegate: class {
    func didTapAddAfterImageBtn(taskDetails: Task, at indexpath: IndexPath)
    func didTapDeleteAfterImageBtn(taskIndex: Int, afterImageIndexpath: IndexPath)
    func didTapCompletionMarkingsBtn(taskDetails: Task, at indexpath: IndexPath, completionButtonTag: Int)
}

class PostInspectionTaskTblCell: UITableViewCell {
    
    @IBOutlet weak var postInspectiontaskNameLbl: UILabel!
    @IBOutlet weak var postInspectionBeforeCollectionView: UICollectionView!
    @IBOutlet weak var postInspectionBeforeCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var postInspectionMarkCompleteToggleBtn: UIButton!
    @IBOutlet weak var postInspectionMarkIncompleteToggleBtn: UIButton!
    @IBOutlet weak var postInspectionAfterCollectionView: UICollectionView!
    
    weak var delegate: PostInspectionTaskTblCellDelegate?
    var selectedPostInspectionTask: Task!
    var selectedTaskIndex: IndexPath!
    
    var taskArr: [Task]! {
        didSet {
            
            postInspectionBeforeCollectionView.register(UINib(nibName: "ListTask_BeforePhoto_CollectionCell", bundle: nil), forCellWithReuseIdentifier: "ListTask_BeforePhoto_CollectionCell")
            postInspectionBeforeCollectionView.delegate = self
            postInspectionBeforeCollectionView.dataSource = self
            postInspectionBeforeCollectionView.reloadData()
            
            postInspectionAfterCollectionView.register(UINib(nibName: "PostInspection_AterPhotos_CollectionCell", bundle: nil), forCellWithReuseIdentifier: "PostInspection_AterPhotos_CollectionCell")
            postInspectionAfterCollectionView.delegate = self
            postInspectionAfterCollectionView.dataSource = self
            postInspectionAfterCollectionView.reloadData()
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(taskDetails: Task, delegate: PostInspectionTaskTblCellDelegate, indexpath: IndexPath) {
        
        self.delegate = delegate
        self.selectedPostInspectionTask = taskDetails
        self.selectedTaskIndex = indexpath
        
        postInspectiontaskNameLbl.text = taskDetails.name
        
        guard let isCompleted = taskDetails.isCompleted else {return}
        
        if isCompleted {
            postInspectionMarkCompleteToggleBtn.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            postInspectionMarkIncompleteToggleBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
        else {
            postInspectionMarkIncompleteToggleBtn.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            postInspectionMarkCompleteToggleBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
        
    }
    
    @IBAction func addAfterPhotoBtnTapped(_ sender: UIButton) {
        //Upload image from here
        delegate?.didTapAddAfterImageBtn(taskDetails: selectedPostInspectionTask, at: selectedTaskIndex)
        
    }
    
    @IBAction func markCompletionRadioBtns(_ sender: UIButton) {
        //Tag 1 = Completed & 2 = Incomplete
        delegate?.didTapCompletionMarkingsBtn(taskDetails: selectedPostInspectionTask, at: selectedTaskIndex, completionButtonTag: sender.tag)
        
    }
    
}


//MARK: COLLECTION VIEW DELEGATE AND DATA SOURCE
extension PostInspectionTaskTblCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
            
        case postInspectionBeforeCollectionView:
            return taskArr[self.tag].beforeImages.count
            
        case postInspectionAfterCollectionView:
            return taskArr[self.tag].afterImages!.count
            
        default:
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
            
        case postInspectionBeforeCollectionView:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListTask_BeforePhoto_CollectionCell", for: indexPath) as? ListTask_BeforePhoto_CollectionCell else {return UICollectionViewCell()}
            cell.configureCell(beforeImageData: taskArr[self.tag].beforeImages[indexPath.item])
            return cell
            
        case postInspectionAfterCollectionView:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostInspection_AterPhotos_CollectionCell", for: indexPath) as? PostInspection_AterPhotos_CollectionCell else {return UICollectionViewCell()}
            cell.configureCell(postInspectionAfterImage: taskArr[self.tag].afterImages![indexPath.row], delegate: self, indexpath: indexPath)
            return cell
            
        default:
            return UICollectionViewCell()
        }
        
        
  
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
          
            case postInspectionBeforeCollectionView:
                return CGSize(width: 100, height: 100)
                
            case postInspectionAfterCollectionView:
                return CGSize(width: 100, height: 100)
                
            default:
                return CGSize(width: 100, height: 100)
            }
            
        }
    
    
}

//MARK: POST INSPECTION AFTER PHOTOS COLLECTION CELL DELEGATE IMPLEMENTATION
extension PostInspectionTaskTblCell: PostInspection_AterPhotos_CollectionCellDelegate {
    
    func didTapDeleteImageBtn(postInspectionAfterImage: Images, at indexpath: IndexPath) {
        delegate?.didTapDeleteAfterImageBtn(taskIndex: self.tag, afterImageIndexpath: indexpath)
        postInspectionAfterCollectionView.reloadData()
    }
    
}
