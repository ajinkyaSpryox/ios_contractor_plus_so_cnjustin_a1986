//
//  ListPostInspectionVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 02/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ListPostInspectionVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var emptyContentStackView: UIStackView!
    @IBOutlet weak var postInspectionListTableView: UITableView!
    @IBOutlet weak var fromDateStackView: UIStackView!
    @IBOutlet weak var toDateStackView: UIStackView!
    
    //From Date Outlets
    @IBOutlet weak var fromDateLbl: UILabel!
    @IBOutlet weak var fromMonthYearLbl: UILabel!
    @IBOutlet weak var fromWeekDayLbl: UILabel!
    
    //To Date Outlets
    @IBOutlet weak var toDateLbl: UILabel!
    @IBOutlet weak var toMonthYearLbl: UILabel!
    @IBOutlet weak var toWeekDayLbl: UILabel!
    
    //Variables
    var postInspectionListDataArr = [PostInspectionList]()
    let dateFormatter = DateFormatter()
    var selectedFromDateString = ""
    var selectedToDateString = ""
    
    //Pagination Variables
    var currentPageNumber = 1
    var maxPageNumber = 0
    var isLoadingMoreInvoices = false
    var cellHeights = [IndexPath: CGFloat]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        serviceCalls()
        setupInitialUI()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        postInspectionListDataArr.removeAll()
        currentPageNumber = 1
    }
    
    private func setupInitialUI() {
        title = "Post Inspection"
        
        postInspectionListTableView.register(UINib(nibName: "ListPostInspectionTblCell", bundle: nil), forCellReuseIdentifier: "ListPostInspectionTblCell")
        postInspectionListTableView.register(UINib(nibName: "ListPostInspectionHeaderTblCell", bundle: nil), forCellReuseIdentifier: "ListPostInspectionHeaderTblCell")
        postInspectionListTableView.delegate = self
        postInspectionListTableView.dataSource = self
        
        let fromDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectFromDate(_:)))
        fromDateStackView.addGestureRecognizer(fromDateTapGesture)
        
        let toDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectToDate(_:)))
        toDateStackView.addGestureRecognizer(toDateTapGesture)
        
        setCurrentDateOnFromDate()
        setTomorrowDateOnToDate()
        
    }
    
    private func serviceCalls() {
        listPostInspectionsFor(pageNumber: currentPageNumber, fromDate: "", toDate: "")
    }

}

//MARK: ALL DATE SELECTION LOGIC HERE
extension ListPostInspectionVC {
    
    @objc
    func selectFromDate(_ sender: UITapGestureRecognizer) {
        //print("From Date To Be Set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = true
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    @objc
    func selectToDate(_ sender: UITapGestureRecognizer) {
        //print("To Date to be set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = false
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    func setCurrentDateOnFromDate() {
        
        let todaysDate = Date()
        
        dateFormatter.dateFormat = "d"
        let currentDate = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let currentMonthYear = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "EEEE"
        let currentWeekday = dateFormatter.string(from: todaysDate)
        
        fromDateLbl.text = currentDate
        fromMonthYearLbl.text = currentMonthYear
        fromWeekDayLbl.text = currentWeekday
        dateFormatter.dateFormat = formattedDateString
        selectedFromDateString = dateFormatter.string(from: todaysDate)
        
    }
    
    func setTomorrowDateOnToDate() {
        
        let nextDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        
        dateFormatter.dateFormat = "d"
        let tomorrowDate = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let tomorrowMonthYear = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "EEEE"
        let tomorrowWeekday = dateFormatter.string(from: nextDate!)
        
        toDateLbl.text = tomorrowDate
        toMonthYearLbl.text = tomorrowMonthYear
        toWeekDayLbl.text = tomorrowWeekday
        dateFormatter.dateFormat = formattedDateString
        selectedToDateString = dateFormatter.string(from: nextDate!)
        
        
    }
    
    
}

//MARK: COMMON DATE PICKER VIEW CONTROLLER DELEGATE IMPLEMENTATION
extension ListPostInspectionVC: CommonDatePickerVCDelegate {
    
    func getSelectedDateFromPicker(date: String, monthYear: String, weekDay: String, isFromDate: Bool, originalSelectedDate: String) {
        
        if isFromDate {
            
            if originalSelectedDate == "" {
                setCurrentDateOnFromDate()
            }
            else {
                fromDateLbl.text = date
                fromMonthYearLbl.text = monthYear
                fromWeekDayLbl.text = weekDay
                selectedFromDateString = originalSelectedDate
            }
            
        }
        else {
            
            if originalSelectedDate == "" {
                setTomorrowDateOnToDate()
            }
            else {
                toDateLbl.text = date
                toMonthYearLbl.text = monthYear
                toWeekDayLbl.text = weekDay
                selectedToDateString = originalSelectedDate
            }
            
        }
        
        if selectedFromDateString != "" || selectedToDateString != "" {
            currentPageNumber = 1
            isLoadingMoreInvoices = false
            self.postInspectionListDataArr.removeAll()
            listPostInspectionsFor(pageNumber: currentPageNumber, fromDate: selectedFromDateString, toDate: selectedToDateString)
            
        }
        
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListPostInspectionVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return postInspectionListDataArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postInspectionListDataArr[section].postInspections.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListPostInspectionHeaderTblCell") as! ListPostInspectionHeaderTblCell
        
        cell.configureCell(postInspectionDetails: postInspectionListDataArr[section])
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListPostInspectionTblCell") as? ListPostInspectionTblCell else {return UITableViewCell()}
        let postInspectionData = postInspectionListDataArr[indexPath.section].postInspections[indexPath.row]
        cell.configureCell(postInspectionDetails: postInspectionData, delegate: self, indexpath: indexPath)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if currentPageNumber < maxPageNumber && !isLoadingMoreInvoices {
            currentPageNumber += 1
            listPostInspectionsFor(pageNumber: currentPageNumber, fromDate: selectedFromDateString, toDate: selectedToDateString)
        }
        
    }
    
}

//MARK: ListPostInspectionTblCell Delegate Implementation
extension ListPostInspectionVC: ListPostInspectionTblCellDelegate {
    
    func didTapEditPostInspectionBtn(postInspection: PostInspection, at indexpath: IndexPath) {
        guard let addEditPostInspectionVc = storyboard?.instantiateViewController(withIdentifier: "AddEditPostInspectionVC") as? AddEditPostInspectionVC else {return}
        addEditPostInspectionVc.postInspectionJobId = postInspection.jobID
        navigationController?.pushViewController(addEditPostInspectionVc, animated: true)
    }
    
    func didTapDeletePostInspectionBtn(postInspection: PostInspection, at indexpath: IndexPath) {
        print(postInspection)
    }
    
    func didTapSendEmailToCustomerBtn(postInspection: PostInspection, at indexpath: IndexPath) {
        sendMailToCustomer(forType: "post_inspection", postInspectionId: postInspection.jobID)
    }
    
}



//MARK: API SERVICES INTEGRATION
extension ListPostInspectionVC {
    
    func listPostInspectionsFor(pageNumber: Int, fromDate: String, toDate: String) {
        
        view.makeToastActivity(.bottom)
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["page_number": pageNumber, "from_date": fromDate, "to_date": toDate]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_post_inspection, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { (postInspection: ListPostInspection!, errorMessage) in
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = postInspection {
                    //Success
                    if returnedResponse.success == "true" {
                        
                        if let returnedData = returnedResponse.data {
                            //Success
                            self.maxPageNumber = returnedData.pageCount
                            
                            returnedData.postInspectionList.forEach { (postInspection) in
                                self.postInspectionListDataArr.append(postInspection)
                            }
                            
                            //print(self.postInspectionListDataArr)
                            
                            DispatchQueue.main.async {
                                self.emptyContentStackView.isHidden = self.postInspectionListDataArr.isEmpty ? false : true
                                self.view.hideToastActivity()
                                self.postInspectionListTableView.reloadData()
                            }
                            
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.postInspectionListTableView.reloadData()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            self.emptyContentStackView.isHidden = self.postInspectionListDataArr.isEmpty ? false : true
                            
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //TODO: Send Email To Customer Service
    func sendMailToCustomer(forType type: String, postInspectionId: Int) {
        
        let parameters: [String:Any] = ["id" : postInspectionId, "type": type]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(
            url: Webservices.send_email_to_customer,
            method: .post, parameters: parameters,
            encodingType: JSONEncoding.default,
            headers: headers) { [weak self] (returnedResponse: DeleteEstimate!, errorMessage) in
                guard let self = self else {return}
                
                self.view.makeToastActivity(.center)
                
                if let error = errorMessage {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(error, duration: 2.0, position: .bottom)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                }
                
        }
        
    }
    
}
