//
//  AddEditPostInspectionVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 02/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class AddEditPostInspectionVC: UIViewController {
    
    @IBOutlet weak var emptyContentStackView: UIStackView!
    @IBOutlet weak var areaTableView: UITableView!
    @IBOutlet weak var areaTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var areaAddressLbl: UILabel!
    @IBOutlet weak var notesTextView: UITextView!
    
    //Required Variables
    var postInspectionJobId: Int!
    
    //Variables
    var postInspectioDetailsData: PostInspectionDetailsData!
    var postInspectionResultDict = [String: Any]()
    var inspectionResultArr = [[String:Any]]()
    
    var selectedTask: Task!
    var selectedTaskIndex: IndexPath!
    var selectedAreaIndex: IndexPath!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Post Inspection Details"
        serviceCalls()
    }
    
    private func serviceCalls() {
        getPostInspectionDetailsWith(jobId: postInspectionJobId)
    }
    
    private func setupPostInspectionUIWith(postInspectionDataDetails: PostInspectionDetailsData) {
        areaTableView.register(UINib(nibName: "PostInspectionAreaTblCell", bundle: nil), forCellReuseIdentifier: "PostInspectionAreaTblCell")
        areaTableView.delegate = self
        areaTableView.dataSource = self
        areaAddressLbl.text = postInspectionDataDetails.address
        
    }
    
    override func updateViewConstraints() {
        areaTableViewHeight.constant = areaTableView.contentSize.height
        super.updateViewConstraints()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }

}

//MARK: IB-ACTIONS IMPLEMENTATION
extension AddEditPostInspectionVC {
    
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        
        postInspectionResultDict.removeAll()
        inspectionResultArr.removeAll()
        
        postInspectioDetailsData.areas.forEach { (area) in
            area.tasks?.forEach({ (task) in
                postInspectionResultDict = task.dictionaryRepresentation
                inspectionResultArr.append(postInspectionResultDict)
            })
        }
        
        setPostInspection()
        
    }
    
}


//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension AddEditPostInspectionVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postInspectioDetailsData.areas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PostInspectionAreaTblCell") as? PostInspectionAreaTblCell else {return UITableViewCell()}
        cell.configureCell(areaDetails: postInspectioDetailsData.areas[indexPath.row], delegate: self, indexpath: indexPath)
        cell.tag = indexPath.row
        cell.postInspectionDetailsData = self.postInspectioDetailsData
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: POST INSPECTION AREA TABLE CELL DELEGATE IMPLEMENTATION
extension AddEditPostInspectionVC: PostInspectionAreaTblCellDelegate {
    
    func didTapCompletionMarkingBtns(taskDetail: Task, at indepath: IndexPath, areaIndexPath: IndexPath, completionBtnTag: Int) {
        
        taskDetail.isCompleted = completionBtnTag == 1
        areaTableView.reloadData()
        
    }
    
    
    func didTapDeleteAfterImageBtn(taskIndex: Int, areaIndexPath: IndexPath, afterImageIndexPath: IndexPath) {
        
        deleteImageFromServer(imageId: (postInspectioDetailsData.areas[areaIndexPath.row].tasks![taskIndex].afterImages?[afterImageIndexPath.row].id)!)
        
        postInspectioDetailsData.areas[areaIndexPath.row].tasks![taskIndex].afterImages?.remove(at: afterImageIndexPath.row)
        
    }
    
    func didTapAddAfterImageBtn(taskDetails: Task, at indexpath: IndexPath, areaIndexPath: IndexPath) {
        
        self.selectedTask = taskDetails
        self.selectedTaskIndex = indexpath
        self.selectedAreaIndex = areaIndexPath
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }
            else {
                self.view.makeToast("Camera Not Available", duration: 2.0, position: .bottom)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
}

//MARK: IMAGE PICKER CONTROLLER DELEGATE IMPLEMENTATION
extension AddEditPostInspectionVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        upload(image: image, purposeString: "post-inspection")
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension AddEditPostInspectionVC {
    
    //Todo: Get Post Inspection Details
    func getPostInspectionDetailsWith(jobId: Int) {
        
        view.makeToastActivity(.center)
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String: Any] = ["job_id": jobId]
        
        GenericWebservice.instance.getServiceData(url: Webservices.post_inspection_details, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { (postInspectionDetails: PostInspectionDetails!, errorMessage) in
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = postInspectionDetails {
                    
                    if returnedResponse.success == "true" {
                        
                        if let returnedData = returnedResponse.data {
                            //Success
                            self.postInspectioDetailsData = returnedData
                            self.setupPostInspectionUIWith(postInspectionDataDetails: self.postInspectioDetailsData)
                            
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.updateViewConstraints()
                                self.areaTableView.reloadData()
                                self.notesTextView.text = self.postInspectioDetailsData.notes
                            }
                        }
                        else {
                            //Failure
                            DispatchQueue.main.async {
                                self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            }
                        }
                        
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
    //TODO: UPLOAD SELECTED AREA IMAGE TO SERVER
    func upload(image: UIImage, purposeString: String) {
        
        let headers = [
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        guard let imageData = image.jpegData(compressionQuality: 0.5) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        
        print(imageData)
        
        let parameters: Parameters = [
            "image" : "file.jpeg",
            "purpose" : purposeString
        ]
        
        Alamofire.upload(multipartFormData: {multipartData in
            for(key,value) in parameters {
                multipartData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            
            multipartData.append(imageData, withName: "image", fileName: "file.jpeg", mimeType: "image/jpeg")
        },
                         //usingThreshold: UInt64.init(),
            to: Webservices.upload_image_to_server,
            method: .post,
            headers: headers,
            encodingCompletion: {result in
                
                switch result {
                case .success(let upload, _, _):
                    
                    self.view.makeToastActivity(.center)
                    
                    upload.uploadProgress(closure: { (progress) in
                        //print("Upload Progress: \(progress.fractionCompleted)")
                        //let progressPercent = Int(progress.fractionCompleted * 100)
                        //print(progressPercent)
                    })
                    
                    upload.responseJSON { response in
                        //print(response.result.value)
                        
                        guard let data = response.data else {return}
                        let decoder = JSONDecoder()
                        
                        do{
                            let returnedResponse = try decoder.decode(ImageUpload.self, from: data)
                            
                            if returnedResponse.success == "true" {
                                
                                self.postInspectioDetailsData.areas[self.selectedAreaIndex.row].tasks![self.selectedTaskIndex.row].afterImages!.append(Images(id: returnedResponse.data.id, image: returnedResponse.data.name))
                                
                                DispatchQueue.main.async {
                                    self.areaTableView.reloadData()
                                    self.view.hideToastActivity()
                                    
                                }
                                
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.view.hideToastActivity()
                                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                                }
                            }
                            //print(self.areaImageUploadedImagesArr)
                        }
                        catch let jsonError {
                            //print(jsonError.localizedDescription)
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.view.makeToast(jsonError.localizedDescription, duration: 2.0, position: .bottom)
                            }
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    self.view.hideToastActivity()
                    self.view.makeToast(encodingError.localizedDescription, duration: 2.0, position: .bottom)
                }
                
        })
        
    }
    
    func setPostInspection() {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        
        let parameters: [String: Any] = ["job_id": self.postInspectionJobId!, "inspection_result": inspectionResultArr, "notes": notesTextView.text]
        
        view.makeToastActivity(.center)
        
        print(parameters)
        
        
        GenericWebservice.instance.getServiceData(url: Webservices.set_post_inspection, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (postInspection: DeleteEstimate!, errorMessage) in

            guard let self = self else {return}

            if let error = errorMessage {
                self.view.hideToastActivity()
                self.view.makeToast(error, duration: 2.0, position: .bottom)
            }
            else {

                if let returnedResponse = postInspection {

                    self.view.hideToastActivity()
                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    self.areaTableView.reloadData()

                }

            }

        }
        
    }
    
    //TODO: DELETE IMAGE FROM SERVER
    func deleteImageFromServer(imageId: Int) {
        
        view.makeToastActivity(.center)
        
        let parameters = ["id" : imageId]
        
        let headers = ["Content-Type": "application/json", "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"]
        
        GenericWebservice.instance.getServiceData(url: Webservices.delete_image_from_server, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (deletedImageResponse: DeleteServerImage!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = deletedImageResponse {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                }
            }
            
        }
        
    }
    
}
