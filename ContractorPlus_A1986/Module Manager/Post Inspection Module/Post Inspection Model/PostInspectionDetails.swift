//
//  PostInspectionDetails.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 02/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - PostInspectionDetails
struct PostInspectionDetails: Codable {
    let success, message: String
    let data: PostInspectionDetailsData?
}

// MARK: - DataClass
struct PostInspectionDetailsData: Codable {
    let address, notes: String
    let jobID: Int
    var areas: [Area]

    enum CodingKeys: String, CodingKey {
        case address, notes
        case jobID = "job_id"
        case areas
    }
}
