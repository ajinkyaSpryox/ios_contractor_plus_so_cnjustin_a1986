//
//  ListPostInspection.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 02/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let listPostInspection = try? newJSONDecoder().decode(ListPostInspection.self, from: jsonData)

import Foundation

// MARK: - ListPostInspection
struct ListPostInspection: Codable {
    let success, message: String
    let data: ListPostInspectionData?
}

// MARK: - DataClass
struct ListPostInspectionData: Codable {
    let postInspectionList: [PostInspectionList]
    let pageCount: Int

    enum CodingKeys: String, CodingKey {
        case postInspectionList = "post_inspection_list"
        case pageCount = "page_count"
    }
}

// MARK: - PostInspectionList
struct PostInspectionList: Codable {
    let date: String
    let postInspections: [PostInspection]

    enum CodingKeys: String, CodingKey {
        case date
        case postInspections = "post_inspections"
    }
}

// MARK: - PostInspection
struct PostInspection: Codable {
    let jobID: Int
    let clientName, clientAddress: String
    let generateBy: GenerateBy
    let status: String
    let statusColor: String
    let statusCode: String

    enum CodingKeys: String, CodingKey {
        case jobID = "job_id"
        case clientName = "client_name"
        case clientAddress = "client_address"
        case generateBy = "generate_by"
        case status
        case statusColor = "status_color"
        case statusCode = "status_code"
    }
}

// MARK: - GenerateBy
struct GenerateBy: Codable {
    let id: Int
    let name: String
    let picture: String
}
