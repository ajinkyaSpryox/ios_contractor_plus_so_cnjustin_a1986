//
//  TimeClockInOutVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 17/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class TimeClockInOutVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var emptyMessageView: UIView!
    @IBOutlet weak var logTableView: UITableView!
    @IBOutlet weak var logTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var clockInOutBtn: UIButton!
    @IBOutlet weak var totalTimeWorkedStack: UIStackView!
    @IBOutlet weak var dateSelectionStack: UIStackView!
    @IBOutlet weak var contractorFilterationTextField: UITextField!
    
    @IBOutlet weak var totalTimeWorkedLbl: UILabel!
    @IBOutlet weak var clockInDateLbl: UILabel!
    @IBOutlet weak var clockInTimeLbl: UILabel!
    
    //Date Outlets
    @IBOutlet weak var fromDateStackView: UIStackView!
    @IBOutlet weak var toDateStackView: UIStackView!
    
    //From Date Outlets
    @IBOutlet weak var fromDateLbl: UILabel!
    @IBOutlet weak var fromMonthYearLbl: UILabel!
    @IBOutlet weak var fromWeekDayLbl: UILabel!
    
    //To Date Outlets
    @IBOutlet weak var toDateLbl: UILabel!
    @IBOutlet weak var toMonthYearLbl: UILabel!
    @IBOutlet weak var toWeekDayLbl: UILabel!
    
    //Received Variables
    var jobDetails: Job!
    var selectedWorkerId = ""
    
    //Variables
    var userRole = UserDefaults.standard.string(forKey: role)
    var isClockedIn = false
    var timeClockId = ""
    var isTimeDateStackHiden = true
    
    var pickerView = UIPickerView()
    
    var listTimeClockData: ListTimeClockData!
    var logArr = [Log]()
    
    //Date Variables
    let dateFormatter = DateFormatter()
    var selectedFromDateString = ""
    var selectedToDateString = ""
    
    //Pagination Variables
    var currentPageNumber = 1
    var maxPageNumber = 0
    var isLoadingMoreList = false
    var cellHeights = [IndexPath: CGFloat]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        totalTimeWorkedStack.isHidden = isTimeDateStackHiden
        dateSelectionStack.isHidden = isTimeDateStackHiden
        
        guard let userRole = userRole else {return}
        contractorFilterationTextField.isHidden = userRole != "contractor"
        
        setupInitialUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        logArr.removeAll()
        currentPageNumber = 1
    }
    
    private func setupInitialUI() {
        title = "TIME CLOCK"
        contractorFilterationTextField.delegate = self
        pickerView.delegate = self
        pickerView.dataSource = self
        logTableView.delegate = self
        logTableView.dataSource = self
        logTableView.register(UINib(nibName: "TimeClockListHeaderTblCell", bundle: nil), forCellReuseIdentifier: "TimeClockListHeaderTblCell")
        logTableView.register(UINib(nibName: "TimeClockListTblCell", bundle: nil), forCellReuseIdentifier: "TimeClockListTblCell")
        
        let fromDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectFromDate(_:)))
        fromDateStackView.addGestureRecognizer(fromDateTapGesture)
        
        let toDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectToDate(_:)))
        toDateStackView.addGestureRecognizer(toDateTapGesture)
        
        setCurrentDateOnFromDate()
        setTomorrowDateOnToDate()
        
        serviceCalls()
    }
    
    private func serviceCalls() {
        listTimeClock(pageNumber: currentPageNumber, fromDate: "", toDate: "", jobId: jobDetails.id, workerId: "")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        logTableViewHeight.constant = logTableView.contentSize.height
    }
    
}

//MARK: IB-ACTIONS IMPLEMENTATION
extension TimeClockInOutVC {
    
    @IBAction func addBtnTapped(_ sender: UIButton) {
        
        isTimeDateStackHiden = !isTimeDateStackHiden
        
        dateSelectionStack.isHidden = isTimeDateStackHiden
        totalTimeWorkedStack.isHidden = isTimeDateStackHiden
    }
    
    @IBAction func clockInOutBtnTapped(_ sender: UIButton) {
        
        //isClockedIn = !isClockedIn
        
        setClockInOut(timeClockId: timeClockId, isClockedIn: isClockedIn, jobId: "\(jobDetails.id)")
        
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension TimeClockInOutVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return logArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return logArr[section].data.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TimeClockListHeaderTblCell") as? TimeClockListHeaderTblCell else {return UITableViewCell()}
        cell.configureCell(log: logArr[section])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TimeClockListTblCell") as? TimeClockListTblCell else {return UITableViewCell()}
        cell.configureCell(logDetails: logArr[indexPath.section].data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
}


//MARK: TEXT FIELD DELEGATE IMPLEMENTATION
extension TimeClockInOutVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        jobDetails.assignedTeamMember.count > 0 ? contractorFilterationTextField.inputView = pickerView : view.makeToast("No Workers to show", duration: 2.0, position: .center)
        
    }
    
}

extension TimeClockInOutVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return jobDetails.assignedTeamMember.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return jobDetails.assignedTeamMember[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedWorkerId = "\(jobDetails.assignedTeamMember[row].id)"
        contractorFilterationTextField.text = jobDetails.assignedTeamMember[row].name
        
        currentPageNumber = 1
        logArr.removeAll()
        listTimeClock(pageNumber: currentPageNumber, fromDate: "", toDate: "", jobId: jobDetails.id, workerId: "\(selectedWorkerId)")
    }
    
}

//MARK: ALL DATE SELECTION LOGIC HERE
extension TimeClockInOutVC {
    
    @objc
    func selectFromDate(_ sender: UITapGestureRecognizer) {
        //print("From Date To Be Set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = true
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    @objc
    func selectToDate(_ sender: UITapGestureRecognizer) {
        //print("To Date to be set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = false
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    func setCurrentDateOnFromDate() {
        
        let todaysDate = Date()
        
        dateFormatter.dateFormat = "d"
        let currentDate = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let currentMonthYear = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "EEEE"
        let currentWeekday = dateFormatter.string(from: todaysDate)
        
        fromDateLbl.text = currentDate
        fromMonthYearLbl.text = currentMonthYear
        fromWeekDayLbl.text = currentWeekday
        dateFormatter.dateFormat = formattedDateString
        selectedFromDateString = dateFormatter.string(from: todaysDate)
        
    }
    
    func setTomorrowDateOnToDate() {
        
        let nextDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        
        dateFormatter.dateFormat = "d"
        let tomorrowDate = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let tomorrowMonthYear = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "EEEE"
        let tomorrowWeekday = dateFormatter.string(from: nextDate!)
        
        toDateLbl.text = tomorrowDate
        toMonthYearLbl.text = tomorrowMonthYear
        toWeekDayLbl.text = tomorrowWeekday
        dateFormatter.dateFormat = formattedDateString
        selectedToDateString = dateFormatter.string(from: nextDate!)
        
    }
    
    
}

//MARK: COMMON DATE PICKER VIEW CONTROLLER DELEGATE IMPLEMENTATION
extension TimeClockInOutVC: CommonDatePickerVCDelegate {
    
    func getSelectedDateFromPicker(date: String, monthYear: String, weekDay: String, isFromDate: Bool, originalSelectedDate: String) {
        
        if isFromDate {
            
            if originalSelectedDate == "" {
                setCurrentDateOnFromDate()
            }
            else {
                fromDateLbl.text = date
                fromMonthYearLbl.text = monthYear
                fromWeekDayLbl.text = weekDay
                selectedFromDateString = originalSelectedDate
            }
            
        }
        else {
            
            if originalSelectedDate == "" {
                setTomorrowDateOnToDate()
            }
            else {
                toDateLbl.text = date
                toMonthYearLbl.text = monthYear
                toWeekDayLbl.text = weekDay
                selectedToDateString = originalSelectedDate
            }
            
        }
        
        
        if selectedFromDateString != "" || selectedToDateString != "" {
            currentPageNumber = 1
            isLoadingMoreList = false
            self.logArr.removeAll()
            listTimeClock(pageNumber: currentPageNumber, fromDate: selectedFromDateString, toDate: selectedToDateString, jobId: jobDetails.id, workerId: "\(selectedWorkerId)")
            
        }
        
    }
    
}

//MARK: API SERICES IMPLEMENTATION
extension TimeClockInOutVC {
    
    func listTimeClock(pageNumber: Int, fromDate: String, toDate: String, jobId: Int, workerId: String) {
        
        self.view.makeToastActivity(.bottom)
        
        isLoadingMoreList = true
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["page_number": pageNumber, "from_date": fromDate, "to_date": toDate]
        
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_time_clock, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [weak self] (listEstimates: ListTimeClock!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = listEstimates {
                    
                    if returnedResponse.success == "true" {
                        //success
                        
                        self.listTimeClockData = returnedResponse.data
                        
                        self.maxPageNumber = returnedResponse.data.pageCount
                        self.isClockedIn = returnedResponse.data.isClockedIn
                        self.timeClockId = "\(returnedResponse.data.timeClockId)"
                        
                        
                        if let returnedLogData = returnedResponse.data.log {
                            print("Log is Empty")
                            returnedLogData.forEach { (logDetail) in
                                self.logArr.append(logDetail)
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.emptyMessageView.isHidden = self.logArr.isEmpty ? false : true
                            self.logTableView.reloadData()
                            self.updateViewConstraints()
                            self.totalTimeWorkedLbl.text = returnedResponse.data.totalHrWorked
                            self.clockInDateLbl.text = returnedResponse.data.clockInDate
                            self.clockInTimeLbl.text = returnedResponse.data.clockInTime
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                        }
                    }
                }
            }
            
            self.isLoadingMoreList = false
        }
        
        
    }
    
    //Set Clock In Out
    func setClockInOut(timeClockId: String, isClockedIn: Bool, jobId: String) {
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["job_id" : jobId, "is_clocked_in": isClockedIn, "time_clock_id": timeClockId]
        
        GenericWebservice.instance.getServiceData(url: Webservices.set_clock_in_out, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { (setClockInOut: SetClockInOut!, errorMessage) in
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = setClockInOut {
                    
                    if returnedResponse.success == true {
                        //Success
                        
                        self.isClockedIn = returnedResponse.data.isClockedIn
                        
                        DispatchQueue.main.async {
                            
                            if isClockedIn {
                                self.clockInOutBtn.setTitle("Clock Out", for: .normal)
                                self.clockInOutBtn.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                                
                            }
                            else {
                                self.clockInOutBtn.setTitle("Clock In", for: .normal)
                                self.clockInOutBtn.backgroundColor = #colorLiteral(red: 0.1873606443, green: 0.4590486288, blue: 0.6895448565, alpha: 1)
                            }
                            
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
}
