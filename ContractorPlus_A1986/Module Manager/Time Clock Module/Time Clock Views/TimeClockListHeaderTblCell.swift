//
//  TimeClockListHeaderTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 24/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class TimeClockListHeaderTblCell: UITableViewCell {
    
    @IBOutlet weak var dateLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(log: Log) {
        dateLbl.text = log.date
    }
    
}
