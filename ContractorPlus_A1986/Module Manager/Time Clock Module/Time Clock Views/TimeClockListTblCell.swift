//
//  TimeClockListTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 24/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class TimeClockListTblCell: UITableViewCell {
    
    @IBOutlet weak var timeInLbl: UILabel!
    @IBOutlet weak var timeOutLbl: UILabel!
    @IBOutlet weak var jobSiteLbl: UILabel!
    @IBOutlet weak var totalTimeLbl: UILabel!
    @IBOutlet weak var bottomOptionsView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func configureCell(logDetails: LogDetails) {
        timeInLbl.text = logDetails.timeIn
        timeOutLbl.text = logDetails.timeOut
        jobSiteLbl.text = logDetails.jobSite
        totalTimeLbl.text = logDetails.totalTime
    }
    
}
