//
//  ListTimeClock.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 24/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListTimeClock
struct ListTimeClock: Codable {
    let success, message: String
    let data: ListTimeClockData
}

// MARK: - DataClass
struct ListTimeClockData: Codable {
    let totalHrWorked: String
    let isClockedIn: Bool
    let clockInDate, clockInTime: String
    let log: [Log]? = []
    let pageCount: Int
    let timeClockId: Int

    enum CodingKeys: String, CodingKey {
        case totalHrWorked = "total_hr_worked"
        case isClockedIn = "is_clocked_in"
        case clockInDate = "clock_in_date"
        case clockInTime = "clock_in_time"
        case timeClockId = "time_clock_id"
        case log
        case pageCount = "page_count"
    }
}

// MARK: - Log
struct Log: Codable {
    let date: String
    let data: [LogDetails]
}

// MARK: - Datum
struct LogDetails: Codable {
    let id: Int
    let timeIn, timeOut, jobSite, totalTime: String

    enum CodingKeys: String, CodingKey {
        case id
        case timeIn = "time_in"
        case timeOut = "time_out"
        case jobSite = "Job_site"
        case totalTime = "total_time"
    }
}

