//
//  SetClockInOut.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 25/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - SetClockInOut
struct SetClockInOut: Codable {
    let success: Bool
    let data: SetClockInOutData
    let message: String
}

// MARK: - DataClass
struct SetClockInOutData: Codable {
    let userID: Int
    let date, time: String
    let isClockedIn, isClockedOut: Bool

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case date, time
        case isClockedIn = "is_clocked_in"
        case isClockedOut = "is_clocked_out"
    }
}
