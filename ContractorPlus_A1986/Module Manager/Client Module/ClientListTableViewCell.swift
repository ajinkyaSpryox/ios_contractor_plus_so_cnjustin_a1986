

import UIKit

class ClientListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var clientname: UILabel!
    @IBOutlet weak var contactNumberLbl: UILabel!
    @IBOutlet weak var emailIdlbl: UILabel!
    @IBOutlet weak var estimateval: UILabel!
    @IBOutlet weak var amountBilledValLbl: UILabel!
    @IBOutlet weak var deleteUiView: UIView!
    @IBOutlet weak var editUiView: UIView!
     @IBOutlet weak var mainUiView: UIView!
    @IBOutlet weak var optionUIView: UIView!
    @IBOutlet weak var countBtn: UIButton!
    @IBOutlet weak var countUiView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
