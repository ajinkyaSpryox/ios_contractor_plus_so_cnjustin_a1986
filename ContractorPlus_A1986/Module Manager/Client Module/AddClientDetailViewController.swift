//
//  AddClientDetailViewController.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 12/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import Photos
import CountryList
import iOSDropDown

class AddClientDetailViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CountryListDelegate,UITextFieldDelegate{

    var picker: UIImagePickerController? = UIImagePickerController()
    
    @IBOutlet weak var clientImageView: UIImageView!
    @IBOutlet weak var AddImageView: UIImageView!
    
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var firstNameTextfield: UITextField!
    @IBOutlet weak var lastNameTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var countryCode: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var countryTextfield: DropDown!
    @IBOutlet weak var stateTextfield: DropDown!
    @IBOutlet weak var zipTextfield: UITextField!
    @IBOutlet weak var address1Textfield: UITextField!
    @IBOutlet weak var address2Textfield: UITextField!
    @IBOutlet weak var cityTextfield: UITextField!
    @IBOutlet weak var businessNameTextField: UITextField!
    
    @IBOutlet weak var saveBtn: UIButton!
    
    var countryArray = [CountryModel]()
    var stateArray = [StateModel]()
    var seletedCountryId : String!
    var seletedStateId : String!
    @IBAction func addClientActionBtn(_ sender: Any) {
        
        if firstNameTextfield.text != "" && lastNameTextfield.text != "" && emailTextfield.text != "" && countryCode.text != "" && phoneTextfield.text != "" && countryTextfield.text != "" && stateTextfield.text != "" && zipTextfield.text != "" && address1Textfield.text != "" && address2Textfield.text != "" && cityTextfield.text != "" {
            if from_edit == true{
                self.uploadImage(id: client_id  , image: clientImageView.image!)
            }else{
            self.uploadImage(id: "", image: clientImageView.image!)
            }
        }else{
            
            errorAlert(message: textfieldEmptyError)
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if  from_edit == true {
            client_detail_Function()
        }
      
        self.stateTextfield.optionArray = ["1","2","3"]
        
        countryTextfield.didSelect{(selectedText , index ,id) in
                   print("Selected String: \(selectedText) \n index: \(index)")
                 
//                   self.RelationShipLbl.text = ""

            self.seletedCountryId = "\(self.countryArray[index].id)"
            self.state_api_Function(countryId: self.countryArray[index].id)
        }

        
        stateTextfield.didSelect{(selectedText , index ,id) in
                         print("Selected String: \(selectedText) \n index: \(index)")
                       
//                         self.RelationShipLbl.text = ""
            self.seletedStateId = "\(self.stateArray[index].id)"

        }
        
    }
    @IBAction func selecteCountryBtnAction(_ sender: UIButton) {
      
             let navController = UINavigationController(rootViewController: countryList)
               self.present(navController, animated: true, completion: nil)
      
      }
      var countryList = CountryList()
      func selectedCountry(country: Country) {
              
          countryCode.text = "+\(country.phoneExtension)"
      }
    override func viewDidLoad() {
        super.viewDidLoad()

        
         picker?.delegate = self
         checkPermission()
         country_api_Function()
         saveBtn.cornerRadius1(value: 12.0, shadowColor: UIColor.red.cgColor, borderColor: UIColor.clear.cgColor)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapFrom(recognizer:)))
           
            AddImageView.addGestureRecognizer(tapGestureRecognizer)
        }

    func state_api_Function(countryId : String){
                
                let header = ["Accept" : "application/json",
                              "x-api-key": appXApiKey]
                
                let parameter : [String : Any] = [
                               
        //                       "email" : emailTF.text!, //"akashp.spryox@gmail.com",//Constant.phone,  //UserDefaults.standard.value(forKey: "user_id")!
        //                       "password" : passwordTF.text! //  "12345678" //Constant.UUID! as Any
                    
                   "user_id": "\(UserDefaults.standard.value(forKey: user_id)!)",
                    "token": "Bearer \(UserDefaults.standard.value(forKey: token)!)",
                    "country_id": countryId
                           
                ]
                
                
            API.callAPI(Webservices.list_states, headers: header, params: parameter) { (response, errorStr) in
                    
                    guard errorStr == nil else {
                        SVProgressHUD.dismiss()
                        self.errorAlert(message: errorStr!)
                        return
                    }
                    
                    if let json = response as? JSON {
                        
                        print(json)
                        SVProgressHUD.dismiss()
                       
        //                 let json = try JSON(data: newData)
                           let postdata = json["data"]
                           let Success = json["success"].boolValue
                           let Message = json["message"].stringValue
                           let eventsData = json["data"]["events"]
                          
                        var stateDataArray = [String]()
                           if(Success == true){
                            for j in 0..<postdata.count{
                                stateDataArray.append(postdata[j]["name"].stringValue)
                                self.stateArray.append(StateModel.init(id: postdata[j]["id"].stringValue, stateName: postdata[j]["stateName"].stringValue, country_id: postdata[j]["country_id"].stringValue))
                            }
                            self.stateTextfield.optionArray = stateDataArray
    //
//                            self.successAlert(message: Message)
                           }else{
                            self.errorAlert(message: Message)
                        }
                        
                    }
                }
                
            }
    
        func client_detail_Function(){
                
                 let header = ["Accept" : "application/json",
                                            "x-api-key": appXApiKey,
                                          "Authorization": "Bearer \(UserDefaults.standard.value(forKey: token)!)"]
                              
                
                let parameter : [String : Any] = [
                               
        //                       "email" : emailTF.text!, //"akashp.spryox@gmail.com",//Constant.phone,  //UserDefaults.standard.value(forKey: "user_id")!
        //                       "password" : passwordTF.text! //  "12345678" //Constant.UUID! as Any
                    
                   "client_id": client_id
                   
                           
                ]
                
                
            API.callAPI(Webservices.get_client_details, headers: header, params: parameter) { (response, errorStr) in
                    
                    guard errorStr == nil else {
                        SVProgressHUD.dismiss()
                        self.errorAlert(message: errorStr!)
                        return
                    }
                    
                    if let json = response as? JSON {
                        
    //
                        SVProgressHUD.dismiss()
                       
        //                 let json = try JSON(data: newData)
                           let postdata = json["data"]
                           let Success = json["success"].boolValue
                           let Message = json["message"].stringValue
                           let eventsData = json["data"]["events"]
                          
                        
                        print(postdata)
                        
                        var countryDataArray = [String]()
                           if(Success == true){
                            for j in 0..<postdata.count{
                               
                            }
                          
    //
    //                        self.successAlert(message: Message)
                           }else{
                            self.errorAlert(message: Message)
                        }
                        
                    }
                }
                
            }
     
    func country_api_Function(){
            
            let header = ["Accept" : "application/json",
                          "x-api-key": appXApiKey]
            
            let parameter : [String : Any] = [
                           
    //                       "email" : emailTF.text!, //"akashp.spryox@gmail.com",//Constant.phone,  //UserDefaults.standard.value(forKey: "user_id")!
    //                       "password" : passwordTF.text! //  "12345678" //Constant.UUID! as Any
                
               "user_id": "\(UserDefaults.standard.value(forKey: user_id)!)",
                "token": "Bearer \(UserDefaults.standard.value(forKey: token)!)",
                       
            ]
            
            
        API.callAPI(Webservices.list_countries, headers: header, params: parameter) { (response, errorStr) in
                
                guard errorStr == nil else {
                    SVProgressHUD.dismiss()
                    self.errorAlert(message: errorStr!)
                    return
                }
                
                if let json = response as? JSON {
                    
//
                    SVProgressHUD.dismiss()
                   
    //                 let json = try JSON(data: newData)
                       let postdata = json["data"]
                       let Success = json["success"].boolValue
                       let Message = json["message"].stringValue
                       let eventsData = json["data"]["events"]
                      
                    
                    var countryDataArray = [String]()
                       if(Success == true){
                        for j in 0..<postdata.count{
                            countryDataArray.append(postdata[j]["name"].stringValue)
                            self.countryArray.append(CountryModel.init(id: postdata[j]["id"].stringValue, countryName: postdata[j]["name"].stringValue, isd_code: postdata[j]["isd_code"].stringValue))
                        }
                        self.countryTextfield.optionArray = countryDataArray
//
//                        self.successAlert(message: Message)
                       }else{
                        self.errorAlert(message: Message)
                    }
                    
                }
            }
            
        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func checkPermission() {
           let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
           switch photoAuthorizationStatus {
           case .authorized:
               print("Access is granted by user")
           case .notDetermined:
               PHPhotoLibrary.requestAuthorization({
                   (newStatus) in
                   print("status is \(newStatus)")
                   if newStatus ==  PHAuthorizationStatus.authorized {
                       /* do stuff here */
                       print("success")
                   }
               })
               print("It is not determined until now")
           case .restricted:
               // same same
               print("User do not have access to photo album.")
           case .denied:
               // same same
               print("User has denied the permission.")
           }
       }
       
     @objc func handleTapFrom(recognizer : UITapGestureRecognizer)
        {
           self.view.endEditing(true)
           
           let alert: UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
           
           let cameraAction = UIAlertAction(title: "Camera", style: .default){
               UIAlertAction in
               
               self.openCamera()
           }
           alert.addAction(cameraAction)
           
           let gallaryAction = UIAlertAction(title: "Gallery", style: .default){
               UIAlertAction in
               
               self.openGallary()
           }
           alert.addAction(gallaryAction)
           
           let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
               UIAlertAction in
               
           }
           alert.addAction(cancelAction)
           if let popoverController = alert.popoverPresentationController {
               popoverController.sourceView = self.view
               popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
               popoverController.permittedArrowDirections = []
           }
           self.present(alert, animated: true, completion: nil)
       }
    

        func openCamera() {
            
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
                picker!.sourceType = UIImagePickerController.SourceType.camera
                picker!.allowsEditing = false
                self.present(picker!, animated: true, completion: nil)
            } else {
                
                openGallary()
            }
        }
        
        //Open Galary
        func openGallary() {
            
            picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
            picker!.allowsEditing = false
            self.present(picker!, animated: true, completion: nil)
        }
        
        
        
        @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {


            var selectedImageFromPicker: UIImage?
            if let editedImage = info[.editedImage] as? UIImage {
                selectedImageFromPicker = editedImage
            } else if let originalImage = info[.originalImage] as? UIImage {
                selectedImageFromPicker = originalImage
            }

            print("selectedImageFromPicker : \(selectedImageFromPicker?.pngData())" )

    //        let selectedImage = UIImage(data:(selectedImageFromPicker?.pngData())!) //compressImage(image: selectedImageFromPicker!)
    //        let selectedImage = compressImage(image: selectedImageFromPicker!)
          
            let selectedImage = selectedImageFromPicker!.resized(withPercentage: 0.3)
            var orgFilename : String!
            if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset{
                var resources = PHAssetResource.assetResources(for: asset)
                orgFilename = (resources[0]).originalFilename
                print("File Name: ",orgFilename!)
            }
            clientImageView.image = selectedImage
           
            dismiss(animated: true, completion: nil)
        }
        
        
        
        func compressImage(image:UIImage) -> UIImage {
            // Reducing file size to a 10th
            
            var actualHeight : CGFloat = image.size.height
            var actualWidth : CGFloat = image.size.width
            let maxHeight : CGFloat = 1300//1200//930//1600
            let maxWidth : CGFloat = 1200//930//1200
            var imgRatio : CGFloat = actualWidth/actualHeight
            let maxRatio : CGFloat = maxWidth/maxHeight
            let compressionQuality : CGFloat = 0.0//0.2..20 percent compression
            
            if (actualHeight > maxHeight || actualWidth > maxWidth){
                if(imgRatio < maxRatio){
                    //adjust width according to maxHeight
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = imgRatio * actualWidth;
                    actualHeight = maxHeight;
                }
                else if(imgRatio > maxRatio){
                    //adjust height according to maxWidth
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = imgRatio * actualHeight;
                    actualWidth = maxWidth;
                }
                else{
                    actualHeight = maxHeight;
                    actualWidth = maxWidth;
                    //compressionQuality = 1;
                }
            }
            
            let rect = CGRect(x:0.0, y:0.0, width:actualWidth, height:actualHeight);
            UIGraphicsBeginImageContext(rect.size);
            image.draw(in: rect)
            let img = UIGraphicsGetImageFromCurrentImageContext();
            // let imageData = UIImageJPEGRepresentation(img!, compressionQuality);
            let imageData = img!.jpegData(compressionQuality: compressionQuality)
            UIGraphicsEndImageContext();
            
            return UIImage(data: imageData!)!
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
          
            self.dismiss(animated: true, completion: nil)
        }
        

    func uploadImage(id: String ,image: UIImage){
        // the image in UIImage typeid
        
        print("Yes! internet is available.")
        print(id)
        // do some tasks..
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: token)!)",
            //            "cache-control": "no-cache",
            //            "Postman-Token": "996c4d82-1644-471c-b6f9-6934e6167c4f"
        ]
        
        
        let parameters: [String: Any] = [
            // "id": "13",//id,//  UserDefaults.standard.value(forKey: "sellerid")!,
            //                "sellerid": "7340742", // UserDefaults.standard.value(forKey: "sellerid")
            
            "client_id": id,
            "user_id": "\(UserDefaults.standard.value(forKey: user_id)!)",
            "token": "Bearer \(UserDefaults.standard.value(forKey: token)!)",
            "title":"",

            "first_name": firstNameTextfield.text!,
            "last_name":lastNameTextfield.text!,
            "email":emailTextfield.text!,
            "isd_code": countryCode.text!,
            "mobile_no": phoneTextfield.text!,
            "address_1": address1Textfield.text!,
            "address_2": address2Textfield.text!,
            "country_id":seletedCountryId!,
            "state_id":seletedStateId!,
            "city": cityTextfield.text!,
            "zipcode":zipTextfield.text!,
            "profile_picture": "file",
            "business_name" : businessNameTextField.text!
//            "added_by_user_id":""
            
            
            
        ] //Optional for extra parameter
        
        
       
        SVProgressHUD.show()
        print(parameters)
        
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
            
            //             multipartFormData.append(imageData, withName: "file[]", fileName: "image.jpg", mimeType: "image/jpeg")
            for (key,value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            
//            for image1 in image{
                
                multipartFormData.append(image.pngData()!, withName: "profile_picture", fileName: "file", mimeType: "image/jpeg")
//            }
            
        },
                         to:Webservices.add_client,headers: headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseString { response in
//                                            debugPrint(response)
                    
                    if((response.result.value) != nil) {
                        //                            let swiftyJsonVar = JSON(response.result.value!)
                        
                        //  self.compalinListArray.removeAll()
                        
                        //     print(swiftyJsonVar)
                        SVProgressHUD.dismiss()
                       
                        //                            self.dismiss(animated: true, completion: nil)
                        
                         print(response.result.value)
                        
                        self.successAlert(AppName, message: "Client added") {
                            from_edit = false
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                    
                    
                }
            case .failure(let encodingError):
                print(encodingError)
                //                    SVProgressHUD.dismiss()
                //                    self.ShowAlert(Constants.appName, AlertMessage: "Something Went Wrong",AlertTag: 1001)
            }
        }
        
        
        
        
    }
}
extension AddClientDetailViewController {
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        if textField.tag == 101 || textField.tag == 102{
            self.countryTextfield.resignFirstResponder()
            
            textField.resignFirstResponder()
        }
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
}
