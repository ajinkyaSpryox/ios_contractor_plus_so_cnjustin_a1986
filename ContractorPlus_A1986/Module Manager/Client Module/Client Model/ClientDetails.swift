//
//  ClientDetails.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 02/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ClientDetails
struct ClientDetails: Codable {
    let success, message: String
    let data: ClientDetailsData?
}

// MARK: - DataClass
struct ClientDetailsData: Codable {
    let id: Int
    let name, firstName, lastName, email: String?
    let isdCode, mobileNo: String?
    let profilePicture: String?
    let businessName: String?
    let numberOfEstimate: Int?
    let amountBilled: String?
    let addressDetails: [AddressDetail]?


    
    enum CodingKeys: String, CodingKey {
        case id, name
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case isdCode = "isd_code"
        case mobileNo = "mobile_no"
        case profilePicture = "profile_picture"
        case businessName = "business_name"
        case addressDetails = "address_details"
        case numberOfEstimate = "number_of_estimate"
        case amountBilled = "amount_billed"
    }
}

// MARK: - AddressDetail
struct AddressDetail: Codable {
    let address1, address2: String
    let countryID, stateID, addressID: Int
    let country, state, city, zipcode: String
    
    enum CodingKeys: String, CodingKey {
        case address1 = "address_1"
        case address2 = "address_2"
        case countryID = "country_id"
        case stateID = "state_id"
        case addressID = "address_id"
        case country, state, city, zipcode
    }
}
