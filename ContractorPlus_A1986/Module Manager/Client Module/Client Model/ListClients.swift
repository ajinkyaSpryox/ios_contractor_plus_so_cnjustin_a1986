//
//  ListClients.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 02/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListClients
struct ListClients: Codable {
    let success, message: String
    let data: ListClientsData?
}

// MARK: - DataClass
struct ListClientsData: Codable {
    let clientList: [ClientList]
    let pageCount: Int

    enum CodingKeys: String, CodingKey {
        case clientList = "client_list"
        case pageCount = "page_count"
    }
}

// MARK: - ClientList
struct ClientList: Codable {
    let date: String
    var clientDetails: [ClientDetailsData]

    enum CodingKeys: String, CodingKey {
        case date
        case clientDetails = "client_details"
    }
}
