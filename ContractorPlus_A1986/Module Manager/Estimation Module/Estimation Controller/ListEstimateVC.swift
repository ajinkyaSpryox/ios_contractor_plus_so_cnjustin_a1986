//
//  AddEstimateVC.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 25/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ListEstimateVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var emptyContentStackView: UIStackView!
    @IBOutlet weak var estimateTableView: UITableView!
    @IBOutlet weak var fromDateStackView: UIStackView!
    @IBOutlet weak var toDateStackView: UIStackView!
    
    //From Date Outlets
    @IBOutlet weak var fromDateLbl: UILabel!
    @IBOutlet weak var fromMonthYearLbl: UILabel!
    @IBOutlet weak var fromWeekDayLbl: UILabel!
    
    //To Date Outlets
    @IBOutlet weak var toDateLbl: UILabel!
    @IBOutlet weak var toMonthYearLbl: UILabel!
    @IBOutlet weak var toWeekDayLbl: UILabel!
    
    //Variables
    var listEstimatesDataArr = [ListEstimateDetailsData]()
    let dateFormatter = DateFormatter()
    var selectedFromDateString = ""
    var selectedToDateString = ""
    
    //Pagination Variables
    var currentPageNumber = 1
    var maxPageNumber = 0
    var isLoadingMoreInvoices = false
    var cellHeights = [IndexPath: CGFloat]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        serviceCalls()
        setupInitialUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        listEstimatesDataArr.removeAll()
        currentPageNumber = 1
    }
    
    fileprivate func setupInitialUI() {
        title = "Estimates"
        estimateTableView.register(UINib(nibName: "EstimationDetailTblCell", bundle: nil), forCellReuseIdentifier: "EstimationDetailTblCell")
        estimateTableView.register(UINib(nibName: "EstimationHeaderTblCell", bundle: nil), forCellReuseIdentifier: "EstimationHeaderTblCell")
        estimateTableView.delegate = self
        estimateTableView.dataSource = self
        
        let fromDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectFromDate(_:)))
        fromDateStackView.addGestureRecognizer(fromDateTapGesture)
        
        let toDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectToDate(_:)))
        toDateStackView.addGestureRecognizer(toDateTapGesture)
        
        setCurrentDateOnFromDate()
        setTomorrowDateOnToDate()
        
    }
    
    fileprivate func serviceCalls() {
        getEstimateListFor(pageNumber: currentPageNumber, fromDate: "", toDate: "")
    }
    
    //IB_ACTIONS
    @IBAction func addEstimateBtnTapped(_ sender: UIButton)  {
        //Go to create new estimate view controller
        guard let createNewEstimateVc = storyboard?.instantiateViewController(withIdentifier: "CreateNewEstimateVC") as? CreateNewEstimateVC else {return}
        navigationController?.pushViewController(createNewEstimateVc, animated: true)
    }
    
}

//MARK: ALL DATE SELECTION LOGIC HERE
extension ListEstimateVC {
    
    @objc
    func selectFromDate(_ sender: UITapGestureRecognizer) {
        //print("From Date To Be Set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = true
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    @objc
    func selectToDate(_ sender: UITapGestureRecognizer) {
        //print("To Date to be set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = false
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    func setCurrentDateOnFromDate() {
        
        let todaysDate = Date()
        
        dateFormatter.dateFormat = "d"
        let currentDate = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let currentMonthYear = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "EEEE"
        let currentWeekday = dateFormatter.string(from: todaysDate)
        
        fromDateLbl.text = currentDate
        fromMonthYearLbl.text = currentMonthYear
        fromWeekDayLbl.text = currentWeekday
        dateFormatter.dateFormat = formattedDateString
        selectedFromDateString = dateFormatter.string(from: todaysDate)
        
    }
    
    func setTomorrowDateOnToDate() {
        
        let nextDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        
        dateFormatter.dateFormat = "d"
        let tomorrowDate = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let tomorrowMonthYear = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "EEEE"
        let tomorrowWeekday = dateFormatter.string(from: nextDate!)
        
        toDateLbl.text = tomorrowDate
        toMonthYearLbl.text = tomorrowMonthYear
        toWeekDayLbl.text = tomorrowWeekday
        dateFormatter.dateFormat = formattedDateString
        selectedToDateString = dateFormatter.string(from: nextDate!)
        
        
    }
    
    
}

//MARK: COMMON DATE PICKER VIEW CONTROLLER DELEGATE IMPLEMENTATION
extension ListEstimateVC: CommonDatePickerVCDelegate {
    
    func getSelectedDateFromPicker(date: String, monthYear: String, weekDay: String, isFromDate: Bool, originalSelectedDate: String) {
        
        if isFromDate {
            
            if originalSelectedDate == "" {
                setCurrentDateOnFromDate()
            }
            else {
                fromDateLbl.text = date
                fromMonthYearLbl.text = monthYear
                fromWeekDayLbl.text = weekDay
                selectedFromDateString = originalSelectedDate
            }
            
        }
        else {
            
            if originalSelectedDate == "" {
                setTomorrowDateOnToDate()
            }
            else {
                toDateLbl.text = date
                toMonthYearLbl.text = monthYear
                toWeekDayLbl.text = weekDay
                selectedToDateString = originalSelectedDate
            }
            
        }
        
        
        if selectedFromDateString != "" || selectedToDateString != "" {
            currentPageNumber = 1
            isLoadingMoreInvoices = false
            self.listEstimatesDataArr.removeAll()
            getEstimateListFor(pageNumber: currentPageNumber, fromDate: selectedFromDateString, toDate: selectedToDateString)
            
        }
        
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListEstimateVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listEstimatesDataArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listEstimatesDataArr[section].estimates.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EstimationHeaderTblCell") as! EstimationHeaderTblCell
        
        cell.configureCell(estimateDate: listEstimatesDataArr[section])
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EstimationDetailTblCell") as? EstimationDetailTblCell else {return UITableViewCell()}
        let estimateData = listEstimatesDataArr[indexPath.section].estimates[indexPath.row]
        cell.configureCell(estimateDetail: estimateData, delegate: self, indexpath: indexPath)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if currentPageNumber < maxPageNumber && !isLoadingMoreInvoices {
            currentPageNumber += 1
            getEstimateListFor(pageNumber: currentPageNumber, fromDate: selectedFromDateString, toDate: selectedToDateString)
        }
        
    }
    
}

//MARK: EstimateDetails Table Cell Delegate Implementation
extension ListEstimateVC: EstimationDetailTblCellDelegate {
    
    
    func didTapEstimateEditBtn(estimateDetail: Estimate, at indexpath: IndexPath) {
        guard let createNewEstimatevc = storyboard?.instantiateViewController(withIdentifier: "CreateNewEstimateVC") as? CreateNewEstimateVC else {return}
        createNewEstimatevc.isEstimateToEdit = true
        createNewEstimatevc.estimateId = estimateDetail.id
        navigationController?.pushViewController(createNewEstimatevc, animated: true)
        
    }
    
    func didTapDeleteEstimateBtn(estimateDetail: Estimate, at indexpath: IndexPath) {
        
        //Todo: Calls Common alert and then continues delete functionality inside closure
        
        guard let commonAlertvc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonAlertVC") as? CommonAlertVC else {return}
        
        commonAlertvc.onOkBtnTap = { [weak self] (success: Bool) -> () in
            guard let self = self else {return}
            
            self.listEstimatesDataArr[indexpath.section].estimates.remove(at: indexpath.row)
            let indexPath = IndexPath(row: indexpath.row, section: indexpath.section)
            self.estimateTableView.deleteRows(at: [indexPath], with: .fade)
            self.deleteEstimateFor(estimateId: estimateDetail.id)
            
            if indexPath.section == 0 && indexPath.row == 0 {
                self.listEstimatesDataArr.remove(at: indexPath.section)
                self.estimateTableView.reloadData()
            }
            
        }
        commonAlertvc.alertTitle = "Delete Estimate"
        commonAlertvc.alertMessage = "Are you sure about deleting this estimate?"
        commonAlertvc.actionButtonTitle = "YES"
        commonAlertvc.modalPresentationStyle = .overCurrentContext
        commonAlertvc.modalTransitionStyle = .crossDissolve
        self.present(commonAlertvc, animated: true, completion: nil)
        
    }
    
    func didTapEmailToCustomerBtn(estimateDetail: Estimate, at indexpath: IndexPath) {
        sendMailToCustomer(forType: "estimate", estimateId: estimateDetail.id)
    }
    
}


//MARK: API SERVICES IMPLEMENTATION
extension ListEstimateVC {
    
    //MARK: GET ESTIMATE LIST SERVICE
    
    func getEstimateListFor(pageNumber: Int, fromDate: String, toDate: String) {
        
        self.view.makeToastActivity(.bottom)
        
        isLoadingMoreInvoices = true
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["page_number": pageNumber, "from_date": fromDate, "to_date": toDate]
        
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_estimate, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [weak self] (listEstimates: ListEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = listEstimates {
                    
                    if returnedResponse.success == "true" {
                        //success
                        
                        if let data = returnedResponse.data {
                            
                            self.maxPageNumber = data.pageCount
                            
                            returnedResponse.data?.estimatesDetailData.forEach({ (estimate) in
                                self.listEstimatesDataArr.append(estimate)
                            })
                            
                            DispatchQueue.main.async {
                                self.emptyContentStackView.isHidden = self.listEstimatesDataArr.isEmpty ? false : true
                                self.view.hideToastActivity()
                                self.estimateTableView.reloadData()
                            }
                            
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.estimateTableView.reloadData()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            self.emptyContentStackView.isHidden = self.listEstimatesDataArr.isEmpty ? false : true
                            
                        }
                    }
                }
            }
            
            self.isLoadingMoreInvoices = false
        }
        
    }
    
    //TODO: Delete Estimate Service
    
    func deleteEstimateFor(estimateId: Int) {
        
        let parameters = ["estimate_id" : estimateId]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.delete_estimate, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (deleteEstimate: DeleteEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = deleteEstimate {
                    
                    if returnedResponse.success == "true" {
                        
                        //Success
                        DispatchQueue.main.async {
                            self.emptyContentStackView.isHidden = self.listEstimatesDataArr.isEmpty ? false : true
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                        
                    }
                        
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                }
                
            }
        }
        
    }
    
    //TODO: Send Email To Customer Service
    func sendMailToCustomer(forType type: String, estimateId: Int) {
        
        let parameters: [String:Any] = ["id" : estimateId, "type": type]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(
            url: Webservices.send_email_to_customer,
            method: .post, parameters: parameters,
            encodingType: JSONEncoding.default,
            headers: headers) { [weak self] (returnedResponse: DeleteEstimate!, errorMessage) in
                guard let self = self else {return}
                
                self.view.makeToastActivity(.center)
                
                if let error = errorMessage {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(error, duration: 2.0, position: .bottom)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                }
                
        }
        
    }
    
}
