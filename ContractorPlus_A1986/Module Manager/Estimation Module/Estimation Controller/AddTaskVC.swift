//
//  AddTaskVC.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 27/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import Photos
import SVProgressHUD

class AddTaskVC: UIViewController {
    
    //IB-Outlets
    @IBOutlet weak var taskNameTextField: UITextField!
    
    @IBOutlet weak var timeSelectionSegment: UISegmentedControl!
    @IBOutlet weak var addEstimatedTimeTextField: UITextField!
    @IBOutlet weak var estimatedTimeUnitLbl: UILabel!
    @IBOutlet weak var commentsTextView: UITextView!
    
    @IBOutlet weak var supplySearchTableView: UITableView!
    @IBOutlet weak var supplyDisplayTableView: UITableView!
    @IBOutlet weak var addSuppliesTextField: CustomInsetTextField!
    @IBOutlet weak var supplyDisplayTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var supplySearchTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var beforePhotosCollectionView: UICollectionView!
    @IBOutlet weak var addTaskBtn: UIButton!
    
    //Received Variables
    var estimateId: Int!
    var areaId: Int!
    
    //Received For Edit
    var taskToEdit: Task?
    
    //Variables
    var searchSupplyItemsArr = [SearchSupplyItemData]()
    var addedSupplyItemsArr = [AddedSupplyItem]()
    var areaImageUploadedImagesArr = [ImageUploadData]()
    
    var selectedTimeUnit = "hours"
    
    //Searching logic Variables
    var isSearching = false
    var searchSupplyItemFilterArr = [SearchSupplyItemData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupInitialView()
    }
    
    fileprivate func setupInitialView() {
        
        if let editTask = taskToEdit {
            setupEditTaskInitialView(editTaskDetails: editTask)
        }
        else {
            self.title = "ADD TASK"
            timeSelectionSegment.selectedSegmentIndex = 1
            addEstimatedTimeTextField.underlined()
            commentsTextView.text = "Enter Additional Comments"
            commentsTextView.textColor = .lightGray
        }
        
        setupDelegates()
        supplySearchTableView.isHidden = true
        customizeTableViewDesign(tableView: supplySearchTableView)
        supplyDisplayTableView.layer.cornerRadius = 8.0
        registerCells()
        addSuppliesTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: .editingChanged)
        serviceCalls()
        
    }
    
    fileprivate func setupDelegates() {
        commentsTextView.delegate = self
        supplyDisplayTableView.delegate = self
        supplyDisplayTableView.dataSource = self
        supplySearchTableView.delegate = self
        supplySearchTableView.dataSource = self
        beforePhotosCollectionView.dataSource = self
        beforePhotosCollectionView.delegate = self
        addSuppliesTextField.delegate = self
    }
    
    fileprivate func registerCells() {
        supplyDisplayTableView.register(UINib(nibName: "SupplyDisplayTblCell", bundle: nil), forCellReuseIdentifier: "SupplyDisplayTblCell")
        supplySearchTableView.register(UINib(nibName: "SearchSuppliesItemTblCell", bundle: nil), forCellReuseIdentifier: "SearchSuppliesItemTblCell")
        beforePhotosCollectionView.register(UINib(nibName: "BeforePhotosCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BeforePhotosCollectionCell")
    }
    
    
    
    private func serviceCalls() {
        getSearchSupplyItemsList()
    }
    
    private func setupEditTaskInitialView(editTaskDetails: Task) {
        
        self.title = "EDIT TASK"
        
        addTaskBtn.setTitle("EDIT TASK", for: .normal)
        
        taskNameTextField.text = editTaskDetails.name
        addEstimatedTimeTextField.text = "\(editTaskDetails.timeEstimate ?? 0)"
        
        addEstimatedTimeTextField.underlined()
        commentsTextView.text = editTaskDetails.notes
        commentsTextView.textColor = .darkText
        
        selectedTimeUnit = editTaskDetails.timeEstimateUnit ?? ""
        timeSelectionSegment.selectedSegmentIndex = selectedTimeUnit == "minutes" ? 0 : 1
        estimatedTimeUnitLbl.text = timeSelectionSegment.selectedSegmentIndex == 0 ? "Mins" : "Hrs"
        
        editTaskDetails.supplies?.forEach { (supplyDetail) in
            addedSupplyItemsArr.append(AddedSupplyItem(id: supplyDetail.id, itemName: supplyDetail.name, supplierName: supplyDetail.name, itemPrice: supplyDetail.price, itemImage: supplyDetail.image, itemCount: supplyDetail.quantity))
        }
        
        editTaskDetails.beforeImages.forEach { (imageDetail) in
            areaImageUploadedImagesArr.append(ImageUploadData(id: imageDetail.id, name: imageDetail.image))
        }
        
        
    }
    
    
    //MARK: MANAGE TABLE VIEW HEIGHT DYNAMICALLY
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        supplyDisplayTableViewHeight.constant = supplyDisplayTableView.contentSize.height
        supplySearchTableViewHeight.constant = supplySearchTableView.contentSize.height
        
    }
    
    //MARK: IB_ACTIONS
    @IBAction func addTaskBtnTapped(_ sender: UIButton) {
        
        guard let taskname = taskNameTextField.text, let estimatedTime = addEstimatedTimeTextField.text else {return}
        
        if taskname != "" && estimatedTime != "" && !areaImageUploadedImagesArr.isEmpty &&  !addedSupplyItemsArr.isEmpty {
            if let editTask = taskToEdit {
                createEditTask(taskID: "\(editTask.id)")
            }
            else {
                createEditTask()
            }
            
        }
        else {
            view.makeToast("Task name, Estimated time, Area Images & Supply Items are mandatory", duration: 2.0, position: .bottom)
        }
        
    }
    
    @IBAction func uploadBeforeImage(_ sender: UIButton) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }
            else {
                self.view.makeToast("Camera Not Available", duration: 2.0, position: .bottom)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    @IBAction func segmentControllerTapped(_ sender: UISegmentedControl) {
        selectedTimeUnit = sender.selectedSegmentIndex == 0 ? "minutes" : "hours"
        estimatedTimeUnitLbl.text = sender.selectedSegmentIndex == 0 ? "Mins" : "Hrs"
    }
    
}

//MARK: IMAGE PICKER CONTROLLER DELEGATE IMPLEMENTATION
extension AddTaskVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        upload(image: image, purposeString: "estimate")
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: Text Field Delegate Implementation
extension AddTaskVC: UITextFieldDelegate {
    
    @objc
    func textFieldEditingDidChange(_ textField: UITextField) {
        
        switch textField {
        case addSuppliesTextField:
            //unhide the supply search table view
            guard let text = textField.text else {return}
            supplySearchTableView.isHidden = text.isEmpty ? true : false
            
            if !text.isEmpty {
                isSearching = true
                searchSupplyItemFilterArr = searchSupplyItemsArr.filter({$0.name.lowercased().contains(text.lowercased())})
                supplySearchTableView.reloadData()
                updateViewConstraints()
            }
            else {
                isSearching = false
                supplySearchTableView.reloadData()
                updateViewConstraints()
                addSuppliesTextField.resignFirstResponder()
            }
            
        default:
            return
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.text = ""
        supplySearchTableView.reloadData()
        return true
    }
    
}

//MARK: TEXT VIEW DELEGATE IMPLEMENTATION
extension AddTaskVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Enter Additional Comments"
            commentsTextView.textColor = .lightGray
        }
        
    }
    
}

//MARK: TableView Delegate & Data Source Implementation
extension AddTaskVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case supplyDisplayTableView:
            return addedSupplyItemsArr.count
            
        default:
            if isSearching {
                return searchSupplyItemFilterArr.count
            }
            else {
                return searchSupplyItemsArr.count
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case supplyDisplayTableView:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SupplyDisplayTblCell") as? SupplyDisplayTblCell else {return UITableViewCell()}
            cell.configureCell(addedItem: addedSupplyItemsArr[indexPath.row], delegate: self, indexpath: indexPath)
            return cell
            
        default:
            
            if isSearching {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchSuppliesItemTblCell") as? SearchSuppliesItemTblCell else {return UITableViewCell()}
                cell.configureCell(searchSupplyItem: searchSupplyItemFilterArr[indexPath.row])
                return cell
            }
            else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchSuppliesItemTblCell") as? SearchSuppliesItemTblCell else {return UITableViewCell()}
                cell.configureCell(searchSupplyItem: searchSupplyItemsArr[indexPath.row])
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
            
        case supplySearchTableView:
            
            let selectedItem = searchSupplyItemFilterArr[indexPath.row]
            
            //Checks for alreay existing items
            if addedSupplyItemsArr.contains(where: {$0.id == selectedItem.id}) {
                addSuppliesTextField.resignFirstResponder()
                view.makeToast("You already have this item in List", duration: 1.0, position: .bottom)
            }
            else {
                
                //Adds new Items to the List
                addedSupplyItemsArr.append(AddedSupplyItem(id: selectedItem.id, itemName: selectedItem.name, supplierName: selectedItem.supplierName, itemPrice: selectedItem.price, itemImage: selectedItem.image, itemCount: 1))
                
                searchSupplyItemFilterArr.removeAll()
                addSuppliesTextField.resignFirstResponder()
                addSuppliesTextField.text = ""
                supplySearchTableView.isHidden = true
                supplySearchTableView.reloadData()
                supplyDisplayTableView.reloadData()
                updateViewConstraints()
            }
            
        default:
            return
        }
    }
    
}

//MARK: SupplyDisplay TABLE CELL DELEGATE IMPLEMENTATION
extension AddTaskVC: SupplyDisplayTblCellDelegate {
    
    func didTapAddItemBtn(addedItem: AddedSupplyItem, at index: IndexPath) {
        addedItem.itemCount += 1
    }
    
    func didTapRemoveItemBtn(addedItem: AddedSupplyItem, at index: IndexPath) {
        
        if addedItem.itemCount > 1 {
            addedItem.itemCount -= 1
        }
        else {
            view.makeToast("Already at the lowest quantity", duration: 1.0, position: .bottom)
        }
        
    }
    
    func didTapDeleteItemBtn(addedItem: AddedSupplyItem, at index: IndexPath) {
        
        //Todo: Calls Common alert and then continues delete functionality inside closure
        
        guard let commonAlertvc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonAlertVC") as? CommonAlertVC else {return}
        
        commonAlertvc.onOkBtnTap = { [weak self] (success: Bool) -> () in
            guard let self = self else {return}
            
            print("This will remove item completely")
            self.addedSupplyItemsArr.remove(at: index.row)
            self.supplyDisplayTableView.reloadData()
            self.updateViewConstraints()
            
        }
        commonAlertvc.alertTitle = "Delete Supply Item"
        commonAlertvc.alertMessage = "Are you sure about deleting this Supply Item?"
        commonAlertvc.actionButtonTitle = "YES"
        commonAlertvc.modalPresentationStyle = .overCurrentContext
        commonAlertvc.modalTransitionStyle = .crossDissolve
        self.present(commonAlertvc, animated: true, completion: nil)
        
    }
    
}

//MARK: BEFORE PHOTOS COLLECTION VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension AddTaskVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return areaImageUploadedImagesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BeforePhotosCollectionCell", for: indexPath) as? BeforePhotosCollectionCell else {return UICollectionViewCell()}
        cell.configureCell(areaImageDetails: areaImageUploadedImagesArr[indexPath.item], delegate: self, indexpath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let cellWidth = (width - 10)/2
        let cellHeight = collectionView.bounds.height
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
}



//MARK: Before Photos Collection Cell Delegate Implementation
extension AddTaskVC: BeforePhotosCollectionCellDelegate {
    
    func didTapDeleteImageBtn(areaImageDetails: ImageUploadData, at indexpath: IndexPath) {
        
        deleteImageFromServer(imageId: areaImageDetails.id)
        areaImageUploadedImagesArr.remove(at: indexpath.row)
        beforePhotosCollectionView.reloadData()
        
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension AddTaskVC {
    
    func getSearchSupplyItemsList() {
        
        view.makeToastActivity(.center)
        
        let parameters = [String:Any]()
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.get_search_supplies_items, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (searchSupplies: SearchSupplyItem!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResonse = searchSupplies {
                    
                    if returnedResonse.success == "true" {
                        //Success
                        
                        if let returnedData = returnedResonse.data {
                            self.searchSupplyItemsArr = returnedData
                        }
                        else {
                            self.view.makeToast(returnedResonse.message, duration: 2.0, position: .bottom)
                        }
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.supplySearchTableView.reloadData()
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResonse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //TODO: UPLOAD SELECTED AREA IMAGE TO SERVER
    func upload(image: UIImage, purposeString: String) {
        
        let headers = [
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        guard let imageData = image.jpegData(compressionQuality: 0.5) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        
        print(imageData)
        
        let parameters: Parameters = [
            "image" : "file.jpeg",
            "purpose" : purposeString
        ]
        
        Alamofire.upload(multipartFormData: {multipartData in
            for(key,value) in parameters {
                multipartData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            
            multipartData.append(imageData, withName: "image", fileName: "file.jpeg", mimeType: "image/jpeg")
        },
                         //usingThreshold: UInt64.init(),
            to: Webservices.upload_image_to_server,
            method: .post,
            headers: headers,
            encodingCompletion: {result in
                
                switch result {
                case .success(let upload, _, _):
                    
                    self.view.makeToastActivity(.center)
                    
                    upload.uploadProgress(closure: { (progress) in
                        //print("Upload Progress: \(progress.fractionCompleted)")
                        //let progressPercent = Int(progress.fractionCompleted * 100)
                        //print(progressPercent)
                    })
                    
                    upload.responseJSON { response in
                        //print(response.result.value)
                        
                        guard let data = response.data else {return}
                        let decoder = JSONDecoder()
                        
                        do{
                            let returnedResponse = try decoder.decode(ImageUpload.self, from: data)
                            
                            if returnedResponse.success == "true" {
                                self.areaImageUploadedImagesArr.append(returnedResponse.data)
                                DispatchQueue.main.async {
                                    self.beforePhotosCollectionView.reloadData()
                                    self.view.hideToastActivity()
                                }
                                
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.view.hideToastActivity()
                                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                                }
                            }
                            //print(self.areaImageUploadedImagesArr)
                        }
                        catch let jsonError {
                            //print(jsonError.localizedDescription)
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.view.makeToast(jsonError.localizedDescription, duration: 2.0, position: .bottom)
                            }
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    self.view.hideToastActivity()
                    self.view.makeToast(encodingError.localizedDescription, duration: 2.0, position: .bottom)
                }
                
        })
        
    }
    
    //TODO: DELETE IMAGE FROM SERVER
    func deleteImageFromServer(imageId: Int) {
        
        view.makeToastActivity(.center)
        
        let parameters = ["id" : imageId]
        
        let headers = ["Content-Type": "application/json", "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"]
        
        GenericWebservice.instance.getServiceData(url: Webservices.delete_image_from_server, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (deletedImageResponse: DeleteServerImage!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = deletedImageResponse {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                }
            }
            
        }
        
    }
    
    //Todo: Create Edit Task Id - Pass Task ID as string only while edit
    func createEditTask(taskID: String = "") {
        
        //view.makeToastActivity(.center)
        
        if commentsTextView.text == "Enter Additional Comments" {
            commentsTextView.text = ""
        }
        
        
        var serverSupplyItemDictArr = [[String:Any]]()
        var serverSupplyItemDict = [String:Any]()
        
        addedSupplyItemsArr.forEach { (item) in
            serverSupplyItemDict["supply_id"] = item.id
            serverSupplyItemDict["quantity"] = item.itemCount
            serverSupplyItemDictArr.append(serverSupplyItemDict)
        }
        
        //print(serverSupplyItemDictArr)
        
        let parameters: [String:Any] = [
            "task_id": taskID, "area_id": self.areaId, "estimate_id": self.estimateId, "name": taskNameTextField.text!, "time_estimate": addEstimatedTimeTextField.text!, "time_estimate_unit": self.selectedTimeUnit, "supplies": serverSupplyItemDictArr, "before_images": areaImageUploadedImagesArr.map({$0.id}), "notes": commentsTextView.text!
        ]
        
        //print(parameters)
        
        let headers = ["Content-Type": "application/json", "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"]
        
        GenericWebservice.instance.getServiceData(url: Webservices.create_task, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (createdTask: CreateTask!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = createdTask {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        //print(returnedResponse.data)
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 1.5, position: .bottom)
                            self.commentsTextView.text = "Enter Additional Comments"
                            
                            if self.taskToEdit != nil {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                                    guard let manageAreavc = self.storyboard?.instantiateViewController(withIdentifier: "SaveEstimationVC") as? SaveEstimationVC else {return}
                                    manageAreavc.estimateId = self.estimateId
                                    self.navigationController?.pushViewController(manageAreavc, animated: true)
                                }
                                
                            }
                            else {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                            
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
            }
            
        }
        
    }
    
}
