//
//  SaveEstimationVC.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 26/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class SaveEstimationVC: UIViewController {
    
    //IBOutlets
    @IBOutlet weak var areaNameTextField: UITextField!
    @IBOutlet weak var areaTableView: UITableView!
    @IBOutlet weak var areaTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollEmbededView: CustomView!
    
    @IBOutlet weak var laborTotalPriceLbl: UILabel!
    @IBOutlet weak var supplyTotalPriceLbl: UILabel!
    @IBOutlet weak var grandTotalPriceLbl: UILabel!
    
    //Received Variables
    var estimateId: Int!
    var isEstimateToEdit = false
    
    //Variables
    var addedEstimateAreaArr = [Area]()
    var savedEstimateData: SaveEstimateData?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        serviceCalls()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    fileprivate func setupInitialUI() {
        title = "Manage Area"
        areaTableView.register(UINib(nibName: "TaskDetailTblCell", bundle: nil), forCellReuseIdentifier: "TaskDetailTblCell")
        areaTableView.delegate = self
        areaTableView.dataSource = self
        
    }
    
    fileprivate func serviceCalls() {
        listEstimateAreas()
    }
    
    override func updateViewConstraints() {
        areaTableViewHeight.constant = areaTableView.contentSize.height
        super.updateViewConstraints()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    
    
}

//MARK: IB-Actions IMPLEMENTATIONS
extension SaveEstimationVC {
    
    @IBAction func saveEstimateBtnTapped(_ sender: UIButton) {
        createEstimateForArea()
    }
    
    @IBAction func savePdfEstimateBtnTapped(_ sender: UIButton) {
        saveEstimateForPrinting(estimateId: self.estimateId)
    }
    
    @IBAction func sendToCustomerBtnTapped(_ sender: UIButton) {
        guard let savedEstimate = savedEstimateData else {return view.makeToast("Save Estimate Before Sending", duration: 1.0, position: .bottom)}
        print("Perform Action on Send to Customer with saved Estimate: \(savedEstimate)")
    }
    
    @IBAction func printPdfBtnTapped(_ sender: UIButton) {
        
        guard let savedEstimate = savedEstimateData else {return view.makeToast("Save Estimate Before Printing", duration: 1.0, position: .bottom)}
        print("Perform Action on PrintPDF with saved Estimate: \(savedEstimate)")
        
    }
    
}

//MARK: Task Table View Implementation
extension SaveEstimationVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addedEstimateAreaArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TaskDetailTblCell") as? TaskDetailTblCell else {return UITableViewCell()}
        cell.configureCell(areaDetail: addedEstimateAreaArr[indexPath.row], delegate: self, indexpath: indexPath)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: TASK DETAIL TABLE CELL DELEGATE IMPLEMENTATION
extension SaveEstimationVC: TaskDetailTblCellDelegate {
    
    
    func didTapDeleteAreaBtn(areaDetail: Area, at indexpath: IndexPath) {
        
        //Todo: Calls Common alert and then continues delete functionality inside closure
        
        guard let commonAlertvc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonAlertVC") as? CommonAlertVC else {return}
        
        commonAlertvc.onOkBtnTap = { [weak self] (success: Bool) -> () in
            guard let self = self else {return}
            
            self.deleteTaskWith(areaId: areaDetail.id ?? 0)
        }
        commonAlertvc.alertTitle = "Delete Area"
        commonAlertvc.alertMessage = "Are you sure about deleting this Area?"
        commonAlertvc.actionButtonTitle = "YES"
        commonAlertvc.modalPresentationStyle = .overCurrentContext
        commonAlertvc.modalTransitionStyle = .crossDissolve
        self.present(commonAlertvc, animated: true, completion: nil)
        
    }
    
    
    func didTapClickToAddTaskBtn(areaDetail: Area, at indexpath: IndexPath) {
        guard let addTaskVc = storyboard?.instantiateViewController(withIdentifier: "AddTaskVC") as? AddTaskVC else {return}
        addTaskVc.estimateId = self.estimateId
        addTaskVc.areaId = areaDetail.id
        navigationController?.pushViewController(addTaskVc, animated: true)
    }
    
    func didTapViewAreaBtn(areaDetail: Area, at indexpath: IndexPath) {
        //Todo: Go to View Task List
        guard let listTaskvc = storyboard?.instantiateViewController(withIdentifier: "ListTaskVC") as? ListTaskVC else {return}
        listTaskvc.estimateId = self.estimateId
        listTaskvc.areaId = areaDetail.id
        navigationController?.pushViewController(listTaskvc, animated: true)
    }

}

//MARK: API SERVICES IMPLEMENTATION
extension SaveEstimationVC {
    
    //Todo: Create a new area list
    func createEstimateForArea() {
        
        guard let areaName = areaNameTextField.text else
        {view.makeToast("Please enter area name", duration: 2.0, position: .bottom)
            return}
        
        view.makeToastActivity(.center)
        
        let parameters: [String:Any] = ["area_id": "", "estimate_id": self.estimateId, "name": areaName]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.create_edit_estimate_area, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (CreateArea: CreateEditArea!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = CreateArea {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        //print(returnedResponse.data)
                        self.addedEstimateAreaArr.removeAll()
                        guard let returnedData = returnedResponse.data else {return}
                        self.addedEstimateAreaArr = returnedData.areas
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.areaTableView.reloadData()
                            self.areaNameTextField.text = ""
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //Todo: List Previously added areas during edit
    func listEstimateAreas() {
        
        view.makeToastActivity(.center)
        
        let parameters: [String:Any] = ["estimate_id": self.estimateId]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_estimate_areas, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (CreateArea: CreateEditArea!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = CreateArea {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        //print(returnedResponse.data)
                        guard let returnedData = returnedResponse.data else {return}
                        self.addedEstimateAreaArr = returnedData.areas
                        
                        DispatchQueue.main.async {
                            
                            self.view.hideToastActivity()
                            self.areaTableView.reloadData()
                            self.updateViewConstraints()
                            self.scrollEmbededView.backgroundColor = .groupTableViewBackground
                            
                            self.laborTotalPriceLbl.text = "$\(returnedData.labourTotal)"
                            self.supplyTotalPriceLbl.text = "$\(returnedData.suppliesTotal)"
                            self.grandTotalPriceLbl.text = "$\(returnedData.grandTotal)"
                            
                            
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func deleteTaskWith(areaId: Int) {
        
        view.makeToastActivity(.center)
        
        let parameters: [String:Any] = ["area_id": areaId]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.delete_area, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (deleteArea: DeleteEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                    
                }
            }
            else {
                
                if let returnedResponse = deleteArea {
                    
                    if returnedResponse.success == "true" {
                        self.listEstimateAreas()
                    }
                    
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //Todo: Save Edtimate for PDF Printing
    
    func saveEstimateForPrinting(estimateId: Int) {
        
        let parameters: [String:Any] = ["estimate_id": estimateId]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.save_estimate, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { (saveEstimate: SaveEstimate!, errorMessage) in
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returedResponse = saveEstimate {
                    
                    if returedResponse.success == "true" {
                        
                        //Success
                        
                        if let data = returedResponse.data {
                            self.savedEstimateData = data
                        }
                        
                        DispatchQueue.main.async {
                            self.view.makeToast(returedResponse.message, duration: 2.0, position: .bottom)
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
    
}
