//
//  ListTaskVC.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 30/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ListTaskVC: UIViewController {
    
    //IB-Outlets
    @IBOutlet weak var listTaskTableView: UITableView!
    
    //Received Variables
    var estimateId: Int!
    var areaId: Int!
    
    //Variables
    var taskListArr = [Task]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
    }
    
    fileprivate func setupInitialView() {
        title = "List Task"
        
        serviceCalls()
        listTaskTableView.delegate = self
        listTaskTableView.dataSource = self
        listTaskTableView.register(UINib(nibName: "ListTaskTblCell", bundle: nil), forCellReuseIdentifier: "ListTaskTblCell")
        
    }
    
    fileprivate func serviceCalls() {
        listEstimateAreaTasksWith(estimateId: estimateId, areaId: areaId)
    }
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListTaskVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListTaskTblCell") as? ListTaskTblCell else {return UITableViewCell()}
        cell.configureCell(taskDetails: taskListArr[indexPath.row], delegate: self, indexPath: indexPath)
        cell.tag = indexPath.row
        cell.taskListArr = self.taskListArr
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: LIST TASK CELL DELEGATE IMPLEMENTATION
extension ListTaskVC: ListTaskTblCellDelegate {
    
    func didTapEditTaskBtn(taskDetails: Task, at indexpath: IndexPath) {
        //Go To Add Task And Make it Editable
        //print(taskDetails)
        guard let addTaskvc = storyboard?.instantiateViewController(withIdentifier: "AddTaskVC") as? AddTaskVC else {return}
        addTaskvc.taskToEdit = taskDetails
        addTaskvc.estimateId = self.estimateId
        addTaskvc.areaId = self.areaId
        navigationController?.pushViewController(addTaskvc, animated: true)
    }
    
    func didTapDeletetaskBtn(taskDetails: Task, at indexpath: IndexPath) {
        
        //Todo: Calls Common alert and then continues delete functionality inside closure
        
        guard let commonAlertvc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonAlertVC") as? CommonAlertVC else {return}
        
        commonAlertvc.onOkBtnTap = { [weak self] (success: Bool) -> () in
            guard let self = self else {return}
            
            //Delete Task Service
            self.deleteTaskWith(taskId: taskDetails.id)
            
        }
        commonAlertvc.alertTitle = "Delete Task"
        commonAlertvc.alertMessage = "Are you sure about deleting this Task?"
        commonAlertvc.actionButtonTitle = "YES"
        commonAlertvc.modalPresentationStyle = .overCurrentContext
        commonAlertvc.modalTransitionStyle = .crossDissolve
        self.present(commonAlertvc, animated: true, completion: nil)
        
    }
    
}

//MARK: API SERVICES IMPLEMENTATIONS
extension ListTaskVC {
    
    //Todo: Get All Task List inside Area of Estimate
    func listEstimateAreaTasksWith(estimateId: Int, areaId: Int) {
        
        view.makeToastActivity(.center)
        
        let parameters = ["estimate_id": estimateId, "area_id": areaId]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_estimate_area_tasks, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (areaTaskDetails: ListAreaTasks!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = areaTaskDetails {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        guard let data = returnedResponse.data else {return}
                        self.taskListArr = data.tasks
                        print(self.taskListArr)
                        
                        DispatchQueue.main.async {
                            self.listTaskTableView.reloadData()
                            self.view.hideToastActivity()
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
            }
            
        }
        
    }
    
    //Todo: Delete Task
    
    func deleteTaskWith(taskId: Int) {
        
        let parameters = ["task_id": taskId]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.delete_task, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { (deleteTask: DeleteEstimate!, errorMessage) in
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = deleteTask {
                    
                    if returnedResponse.success == "true" {
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            self.listEstimateAreaTasksWith(estimateId: self.estimateId, areaId: self.areaId)
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
