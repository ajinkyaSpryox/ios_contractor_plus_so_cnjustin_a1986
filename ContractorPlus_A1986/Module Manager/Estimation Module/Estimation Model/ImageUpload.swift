//
//  AreaImageUpload.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 19/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - AreaImageUpload
struct ImageUpload: Codable {
    let success, message: String
    let data: ImageUploadData
}

// MARK: - DataClass
struct ImageUploadData: Codable {
    let id: Int
    let name: String
}



// MARK: - DeleteServerImage
struct DeleteServerImage: Codable {
    let success, message: String
}
