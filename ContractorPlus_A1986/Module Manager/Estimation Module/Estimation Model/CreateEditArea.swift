//
//  AddEditArea.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 18/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - CreateEditArea
struct CreateEditArea: Codable {
    let success, message: String
    let data: CreateEditAreaData?
}

// MARK: - DataClass
struct CreateEditAreaData: Codable {
    let areas: [Area]
    let labourTotal, suppliesTotal, grandTotal: Int

    enum CodingKeys: String, CodingKey {
        case areas
        case labourTotal = "labour_total"
        case suppliesTotal = "supplies_total"
        case grandTotal = "grand_total"
    }
}
