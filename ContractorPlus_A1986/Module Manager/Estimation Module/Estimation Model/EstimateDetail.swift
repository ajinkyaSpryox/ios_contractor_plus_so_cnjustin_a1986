//
//  EstimateDetail.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 17/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let estimateDetail = try? newJSONDecoder().decode(EstimateDetail.self, from: jsonData)

import Foundation

// MARK: - EstimateDetail
struct EstimateDetail: Codable {
    let success, message: String
    var data: EstimateDetailData?
}

// MARK: - DataClass
struct EstimateDetailData: Codable {
    let id: Int
    let estimateNo: String
    let clientID, clientAddressID: Int
    let clientName, address1, address2: String
    let countryID, stateID: Int
    let country, state, city, zipcode: String
    let laborRate: Int
    let supplyMarkupUnit: String
    let supplyMarkupValue: Int
    let pdf: String
    let status, comment, date: String
    var areas: [Area]?
    let labourTotal, suppliesTotal, grandTotal: Int

    enum CodingKeys: String, CodingKey {
        case id
        case estimateNo = "estimate_no"
        case clientID = "client_id"
        case clientAddressID = "client_address_id"
        case clientName = "client_name"
        case address1 = "address_1"
        case address2 = "address_2"
        case countryID = "country_id"
        case stateID = "state_id"
        case country, state, city, zipcode
        case laborRate = "labor_rate"
        case supplyMarkupUnit = "supply_markup_unit"
        case supplyMarkupValue = "supply_markup_value"
        case pdf, status, comment, date, areas
        case labourTotal = "labour_total"
        case suppliesTotal = "supplies_total"
        case grandTotal = "grand_total"
    }
}

// MARK: - Area
struct Area: Codable {
    let id: Int?
    let name: String
    let labourTotal, suppliesTotal, noOfTasks: Int?
    var tasks: [Task]?

    enum CodingKeys: String, CodingKey {
        case id, name
        case labourTotal = "labour_total"
        case suppliesTotal = "supplies_total"
        case noOfTasks = "no_of_tasks"
        case tasks
    }
}

// MARK: - Task
class Task: Codable {
    let id: Int
    let name: String
    let timeEstimate: Int?
    let timeEstimateUnit: String?
    let notes: String?
    let supplies: [Supply]?
    let beforeImages: [Images]
    var afterImages: [Images]?
    var isCompleted: Bool?
    
    
    var dictionaryRepresentation: [String: Any] {
        return [
            "is_completed" : isCompleted ?? false,
            "task_id" : id,
            "after_images" : afterImages?.map({$0.id}) ?? []
        ]
    }

    enum CodingKeys: String, CodingKey {
        case id, name
        case timeEstimate = "time_estimate"
        case timeEstimateUnit = "time_estimate_unit"
        case notes, supplies
        case beforeImages = "before_images"
        case afterImages = "after_images"
        case isCompleted = "is_completed"
        
    }
}



// MARK: - BeforeImage
struct BeforeImage: Codable {
    let id: Int
    let image: String
}

// MARK: - Image
struct Images: Codable {
    let id: Int
    let image: String
}

// MARK: - Supply
struct Supply: Codable {
    let id: Int
    let name, supplyDescription: String
    let image: String
    let price, quantity: Int

    enum CodingKeys: String, CodingKey {
        case id, name
        case supplyDescription = "description"
        case image, price, quantity
    }
}


