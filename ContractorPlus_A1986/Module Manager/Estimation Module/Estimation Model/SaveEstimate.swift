//
//  SaveEstimate.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 26/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - SaveEstimate
struct SaveEstimate: Codable {
    let success, message: String
    let data: SaveEstimateData?
}

// MARK: - SaveEstimateData
struct SaveEstimateData: Codable {
    let estimateID: String
    let pdfURL: String

    enum CodingKeys: String, CodingKey {
        case estimateID = "estimate_id"
        case pdfURL = "pdf_url"
    }
}
