//
//  ListAreaTasks.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 20/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListAreaTasks
struct ListAreaTasks: Codable {
    let success, message: String
    let data: ListAreaTasksData?
}

// MARK: - DataClass
struct ListAreaTasksData: Codable {
    let tasks: [Task]
}
