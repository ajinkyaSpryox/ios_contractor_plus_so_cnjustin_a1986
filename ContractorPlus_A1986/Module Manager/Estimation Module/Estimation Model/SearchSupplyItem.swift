//
//  SearchSupplyItem.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 18/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - SearchSupplyItem
struct SearchSupplyItem: Codable {
    let success, message: String
    let data: [SearchSupplyItemData]?
}

// MARK: - SearchSupplyItemData
struct SearchSupplyItemData: Codable {
    let id: Int
    let supplierName, name: String
    let image: String
    let price: Int
    let quantity: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case supplierName = "supplier_name"
        case name, image, price
        case quantity
    }
}

// MARK: - AddedSupplyItem
class AddedSupplyItem {
    let id: Int
    let supplierName, name: String
    let image: String
    let price: Int
    var itemCount: Int = 1
    
    var computedPrice: Int {
        return price * itemCount
    }
    
    var dictionaryRepresentation: [String: Any] {
        return [
            "id" : id,
            "quantity" : itemCount,
        ]
    }
    
    init(id: Int, itemName: String, supplierName: String, itemPrice: Int, itemImage: String, itemCount: Int) {
        self.id = id
        self.name = itemName
        self.supplierName = supplierName
        self.price = itemPrice
        self.image = itemImage
        self.itemCount = itemCount
    }
    
}
