//
//  CreateTask.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 19/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - CreateTask
struct CreateTask: Codable {
    let success, message: String
    let data: CreateTaskData?
}

// MARK: - DataClass
struct CreateTaskData: Codable {
    let taskID: Int
    let name: String
    let timeEstimate: String
    let notes: String
    let supplies: [Supply]
    let beforeImages: [BeforeImage]

    enum CodingKeys: String, CodingKey {
        case taskID = "task_id"
        case name
        case timeEstimate = "time_estimate"
        case notes, supplies
        case beforeImages = "before_images"
    }
}

//// MARK: - BeforeImage
//struct BeforeImageData: Codable {
//    let id: Int
//    let image: String
//}

//// MARK: - Supply
//struct SupplyData: Codable {
//    let id: Int
//    let name, supplyDescription: String
//    let image: String
//    let price, quantity: Int
//
//    enum CodingKeys: String, CodingKey {
//        case id, name
//        case supplyDescription = "description"
//        case image, price, quantity
//    }
//}

