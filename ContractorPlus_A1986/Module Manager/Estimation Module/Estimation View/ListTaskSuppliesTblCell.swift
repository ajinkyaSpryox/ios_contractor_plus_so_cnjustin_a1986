//
//  ListTaskSuppliesTblCell.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 30/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

class ListTaskSuppliesTblCell: UITableViewCell {
    
    @IBOutlet weak var supplyItemImage: UIImageView!
    @IBOutlet weak var supplyItemNameLbl: UILabel!
    @IBOutlet weak var supplyItemSupplierNameLbl: UILabel!
    @IBOutlet weak var supplyItemPriceLbl: UILabel!
    @IBOutlet weak var supplyItemQuantityLbl: UILabel!
    
    @IBOutlet weak var bottomSeperatorLine: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(supplyItemDetail: Supply) {
        
        guard let url = URL(string: supplyItemDetail.image) else {return}
        supplyItemImage.kf.indicatorType = .activity
        supplyItemImage.kf.setImage(with: url, placeholder: UIImage(named: "image1"))
        
        supplyItemNameLbl.text = supplyItemDetail.name
        
        if supplyItemDetail.supplyDescription == "" {
            supplyItemSupplierNameLbl.text = "No Supplier Specified"
        }
        else {
          supplyItemSupplierNameLbl.text = supplyItemDetail.supplyDescription
        }
        
        supplyItemPriceLbl.text = "$ \(supplyItemDetail.price)"
        supplyItemQuantityLbl.text = "\(supplyItemDetail.quantity)"
    }
    
}
