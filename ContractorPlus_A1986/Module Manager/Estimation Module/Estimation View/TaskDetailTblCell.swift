//
//  AddTaskTblCell.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 26/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

protocol TaskDetailTblCellDelegate: class {
    func didTapClickToAddTaskBtn(areaDetail: Area, at indexpath: IndexPath)
    func didTapViewAreaBtn(areaDetail: Area, at indexpath: IndexPath)
    func didTapDeleteAreaBtn(areaDetail: Area, at indexpath: IndexPath)
}

class TaskDetailTblCell: UITableViewCell {
    
    @IBOutlet weak var contentHolderView: UIView!
    @IBOutlet weak var bottomOptionsView: UIView!
    
    @IBOutlet weak var areaNameLbl: UILabel!
    @IBOutlet weak var numberOftasksLbl: UILabel!
    @IBOutlet weak var suppliesTotalLbl: UILabel!
    @IBOutlet weak var laborTotalLbl: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    weak var delegate: TaskDetailTblCellDelegate?
    var selectedArea: Area!
    var selectedIndexpath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        contentHolderView.clipsToBounds = true
        contentHolderView.layer.cornerRadius = 10
        contentHolderView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func configureCell(areaDetail: Area, delegate: TaskDetailTblCellDelegate, indexpath: IndexPath) {
        self.delegate = delegate
        self.selectedArea = areaDetail
        self.selectedIndexpath = indexpath
        
        areaNameLbl.text = areaDetail.name
        suppliesTotalLbl.text = "$ \(areaDetail.suppliesTotal ?? 0)"
        laborTotalLbl.text = "$ \(areaDetail.labourTotal ?? 0)"
        
        switch areaDetail.noOfTasks {
        case 0:
            numberOftasksLbl.text = "No Task Added"
        case 1:
            numberOftasksLbl.text = "\(areaDetail.noOfTasks ?? 0) task"
        default:
            numberOftasksLbl.text = "\(areaDetail.noOfTasks ?? 0) tasks"
        }
        
    }
    
    @IBAction func clickToAddTaskBtnTapped(_ sender: UIButton) {
        delegate?.didTapClickToAddTaskBtn(areaDetail: selectedArea, at: selectedIndexpath)
    }
    
    @IBAction func viewAreaDetailsBtnTapped(_ sender: UIButton) {
        delegate?.didTapViewAreaBtn(areaDetail: selectedArea, at: selectedIndexpath)
    }
    
    @IBAction func deleteAreaBtnTapped(_ sender: UIButton) {
        delegate?.didTapDeleteAreaBtn(areaDetail: selectedArea, at: selectedIndexpath)
    }
    
}
