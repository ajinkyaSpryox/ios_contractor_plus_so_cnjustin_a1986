//
//  BeforePhotosCollectionCell.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 30/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

protocol BeforePhotosCollectionCellDelegate: class {
    func didTapDeleteImageBtn(areaImageDetails: ImageUploadData, at indexpath: IndexPath)
}

class BeforePhotosCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var beforePhotoImageView: UIImageView!
    
    weak var delegate: BeforePhotosCollectionCellDelegate?
    var selectedAreaDetails: ImageUploadData!
    var selectedIndexPath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(areaImageDetails: ImageUploadData, delegate: BeforePhotosCollectionCellDelegate, indexpath: IndexPath) {
        self.delegate = delegate
        self.selectedIndexPath = indexpath
        self.selectedAreaDetails = areaImageDetails
        
        guard let imageUrl = URL(string: areaImageDetails.name) else {return}
        beforePhotoImageView.kf.indicatorType = .activity
        beforePhotoImageView.kf.setImage(with: imageUrl, placeholder: UIImage(named: "image1"))
    }
    
    @IBAction func deleteImageBtnTapped(_ sender: UIButton) {
        delegate?.didTapDeleteImageBtn(areaImageDetails: selectedAreaDetails, at: selectedIndexPath)
    }

}
