//
//  SupplyDisplayTblCell.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 30/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

protocol SupplyDisplayTblCellDelegate: class {
    func didTapAddItemBtn(addedItem: AddedSupplyItem, at index: IndexPath)
    func didTapRemoveItemBtn(addedItem: AddedSupplyItem, at index: IndexPath)
    func didTapDeleteItemBtn(addedItem: AddedSupplyItem, at index: IndexPath)
}

class SupplyDisplayTblCell: UITableViewCell {
    
    @IBOutlet weak var addedItemNameLbl: UILabel!
    @IBOutlet weak var addedItemImage: UIImageView!
    @IBOutlet weak var addedItemSupplierNameLbl: UILabel!
    @IBOutlet weak var addedItemPriceLbl: UILabel!
    @IBOutlet weak var addedItemQuantityLbl: UILabel!
    
    weak var delegate: SupplyDisplayTblCellDelegate?
    var selectedItem: AddedSupplyItem!
    var selectedIndexpath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(addedItem: AddedSupplyItem, delegate: SupplyDisplayTblCellDelegate, indexpath: IndexPath) {
        
        self.delegate = delegate
        self.selectedItem = addedItem
        self.selectedIndexpath = indexpath
        
        guard let url = URL(string: addedItem.image) else {return}
        addedItemImage.kf.indicatorType = .activity
        addedItemImage.kf.setImage(with: url, placeholder: UIImage(named: "image1"))
        
        addedItemNameLbl.text = addedItem.name
        addedItemSupplierNameLbl.text = addedItem.supplierName
        addedItemPriceLbl.text = "$ \(addedItem.price)"
        addedItemQuantityLbl.text = "\(addedItem.itemCount)"
    }
    
    @IBAction func removeItemBtnTapped(_ sender: UIButton) {
        delegate?.didTapRemoveItemBtn(addedItem: selectedItem, at: selectedIndexpath)
        addedItemQuantityLbl.text = "\(selectedItem.itemCount)"
    }
    
    @IBAction func addItemBtnTapped(_ sender: UIButton) {
        delegate?.didTapAddItemBtn(addedItem: selectedItem, at: selectedIndexpath)
        addedItemQuantityLbl.text = "\(selectedItem.itemCount)"
    }
    
    @IBAction func deleteItemBtnTapped(_ sender: UIButton) {
        delegate?.didTapDeleteItemBtn(addedItem: selectedItem, at: selectedIndexpath)
    }
    
}
