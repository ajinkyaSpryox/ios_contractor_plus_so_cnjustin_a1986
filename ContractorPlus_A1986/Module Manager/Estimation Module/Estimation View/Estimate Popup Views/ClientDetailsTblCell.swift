//
//  ClientDetailsTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 31/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class ClientDetailsTblCell: UITableViewCell {
    
    @IBOutlet weak var address1Lbl: UILabel!
    @IBOutlet weak var address2Lbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var zipcodeLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(addressDetails: AddressDetail) {
        address1Lbl.text = addressDetails.address1
        address2Lbl.text = addressDetails.address2
        cityLbl.text = addressDetails.city
        stateLbl.text = addressDetails.state
        countryLbl.text = addressDetails.country
        zipcodeLbl.text = addressDetails.zipcode
    }
    
    
}
