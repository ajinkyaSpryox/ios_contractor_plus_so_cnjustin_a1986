//
//  SearchSuppliesItemTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 18/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

class SearchSuppliesItemTblCell: UITableViewCell {
    
    @IBOutlet weak var supplyItemImage: UIImageView!
    @IBOutlet weak var supplyItemNameLbl: UILabel!
    @IBOutlet weak var supplyItemSupplierNameLbl: UILabel!
    @IBOutlet weak var supplyItemPriceLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(searchSupplyItem: SearchSupplyItemData) {
        guard let url = URL(string: searchSupplyItem.image) else {return}
        supplyItemImage.kf.setImage(with: url, placeholder: UIImage(named: "image1"))
        
        supplyItemNameLbl.text = searchSupplyItem.name
        supplyItemSupplierNameLbl.text = searchSupplyItem.supplierName
        supplyItemPriceLbl.text = "$ \(searchSupplyItem.price)"
    }
    
    
    
}
