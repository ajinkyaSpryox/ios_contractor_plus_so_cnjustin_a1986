//
//  ListTask_BeforePhoto_CollectionCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 20/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class ListTask_BeforePhoto_CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var beforeTaskImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(beforeImageData: Images) {
        guard let imageUrl = URL(string: beforeImageData.image) else {return}
        beforeTaskImageView.kf.indicatorType = .activity
        beforeTaskImageView.kf.setImage(with: imageUrl, placeholder: UIImage(named: "image1"))
    }

}
