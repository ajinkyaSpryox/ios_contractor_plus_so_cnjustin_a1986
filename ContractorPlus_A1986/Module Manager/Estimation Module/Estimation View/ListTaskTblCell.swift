//
//  ListTaskTblCell.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 30/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

protocol ListTaskTblCellDelegate: class {
    func didTapEditTaskBtn(taskDetails: Task, at indexpath: IndexPath)
    func didTapDeletetaskBtn(taskDetails: Task, at indexpath: IndexPath)
}

class ListTaskTblCell: UITableViewCell {
    
    @IBOutlet weak var listTaskSuppliesTblView: UITableView!
    @IBOutlet weak var listTaskSuppliesTblHeight: NSLayoutConstraint!
    @IBOutlet weak var listTaskSuppliesPhotoCollectionView: UICollectionView!
    @IBOutlet weak var bottomOptionsView: UIView!
    
    @IBOutlet weak var taskNameLbl: UILabel!
    @IBOutlet weak var taskEstimatedTimeLbl: UILabel!
    
    var taskListArr: [Task]! {

        didSet {

            listTaskSuppliesTblView.delegate = self
            listTaskSuppliesTblView.dataSource = self
            
            listTaskSuppliesPhotoCollectionView.delegate = self
            listTaskSuppliesPhotoCollectionView.dataSource = self
            
            listTaskSuppliesTblView.register(UINib(nibName: "ListTaskSuppliesTblCell", bundle: nil), forCellReuseIdentifier: "ListTaskSuppliesTblCell")
            listTaskSuppliesPhotoCollectionView.register(UINib(nibName: "ListTask_BeforePhoto_CollectionCell", bundle: nil), forCellWithReuseIdentifier: "ListTask_BeforePhoto_CollectionCell")
            
            listTaskSuppliesTblView.reloadData()
            listTaskSuppliesPhotoCollectionView.reloadData()
            updateConstraints()
        }

    }
    
    var selectedTask: Task!
    var selectedTaskIndex: IndexPath!
    weak var delegate: ListTaskTblCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
    }
    
    func configureCell(taskDetails: Task, delegate: ListTaskTblCellDelegate, indexPath: IndexPath) {
        self.selectedTask = taskDetails
        self.delegate = delegate
        self.selectedTaskIndex = indexPath
        
        taskNameLbl.text = taskDetails.name
        taskEstimatedTimeLbl.text = "\(taskDetails.timeEstimate ?? 0) \(taskDetails.timeEstimateUnit ?? "")"
        
    }
    
    override func updateConstraints() {
        listTaskSuppliesTblHeight.constant = listTaskSuppliesTblView.contentSize.height
        //print(taskListArr)
        listTaskSuppliesTblView.layoutIfNeeded()
        super.updateConstraints()
    }
    
}

//MARK: IB-ACTIONS IMPLEMENTATION
extension ListTaskTblCell {
    
    @IBAction func editTaskBtnTapped(_ sender: UIButton) {
        delegate?.didTapEditTaskBtn(taskDetails: selectedTask, at: selectedTaskIndex)
    }
    
    @IBAction func deleteTaskBtnTapped(_ sender: UIButton) {
        delegate?.didTapDeletetaskBtn(taskDetails: selectedTask, at: selectedTaskIndex)
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListTaskTblCell: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskListArr[self.tag].supplies?.count ?? 0

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListTaskSuppliesTblCell") as? ListTaskSuppliesTblCell else {return UITableViewCell()}
        guard let suppliesData = taskListArr[self.tag].supplies else {return UITableViewCell()}
        cell.configureCell(supplyItemDetail: suppliesData[indexPath.row])
        cell.bottomSeperatorLine.isHidden = indexPath.row == suppliesData.count - 1 ? true : false
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }

}

//MARK: COLLECTION VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListTaskTblCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return taskListArr[self.tag].beforeImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListTask_BeforePhoto_CollectionCell", for: indexPath) as? ListTask_BeforePhoto_CollectionCell else {return UICollectionViewCell()}
        cell.configureCell(beforeImageData: taskListArr[self.tag].beforeImages[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
}
