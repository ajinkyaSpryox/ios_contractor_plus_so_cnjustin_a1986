//
//  ShoppingListDetailsTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 12/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

protocol ShoppingListDetailsTblCellDelegate: class {
    func didTapEditShoppingListBtn(shoppingList: ShoppingList, at indexpath: IndexPath)
    func didTapDeleteShoppingListBtn(shoppingList: ShoppingList, at indexpath: IndexPath)
}

class ShoppingListDetailsTblCell: UITableViewCell {
    
    @IBOutlet weak var shoppingListNumberLbl: UILabel!
    @IBOutlet weak var estimateNumberStack: UIStackView!
    @IBOutlet weak var estimateNumberLbl: UILabel!
    @IBOutlet weak var dateStack: UIStackView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var clientStack: UIStackView!
    @IBOutlet weak var clientNameLbl: UILabel!
    @IBOutlet weak var totalItemsLbl: UILabel!
    
    @IBOutlet weak var bottomOptionsView: UIView!
    
    weak var delegate: ShoppingListDetailsTblCellDelegate?
    var selectedShoppingList: ShoppingList!
    var selectedIndexpath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func configureCell(shoppingListDetails: ShoppingList, delegate: ShoppingListDetailsTblCellDelegate, indexpath: IndexPath) {
        
        self.delegate = delegate
        self.selectedShoppingList = shoppingListDetails
        self.selectedIndexpath = indexpath
        
        shoppingListNumberLbl.text = shoppingListDetails.shoppingListNo
        estimateNumberStack.isHidden = shoppingListDetails.estimateNo == ""
        estimateNumberLbl.text = shoppingListDetails.estimateNo
        dateLbl.text = shoppingListDetails.date
        clientStack.isHidden = shoppingListDetails.clientName == ""
        clientNameLbl.text = shoppingListDetails.clientName
        totalItemsLbl.text = "\(shoppingListDetails.totalItems)"
        
    }
    
    @IBAction func editShoppingListBtnTapped(_ sender: UIButton) {
        delegate?.didTapEditShoppingListBtn(shoppingList: selectedShoppingList, at: selectedIndexpath)
    }
    
    @IBAction func deleteShoppingListBtnTapped(_ sender: UIButton) {
        delegate?.didTapDeleteShoppingListBtn(shoppingList: selectedShoppingList, at: selectedIndexpath)
    }
    
}
