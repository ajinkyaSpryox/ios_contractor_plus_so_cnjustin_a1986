//
//  ShoppingSuppliesListVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 11/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import WebKit

class ShoppingSuppliesListVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var emptyMessageStack: UIStackView!
    @IBOutlet weak var shoppingSuppliesTableView: UITableView!
    @IBOutlet weak var shoppingSuppliesTableHeight: NSLayoutConstraint!
    @IBOutlet weak var grandTotalPriceLbl: UILabel!
    
    //Receieved Variables
    var existingShoppingListID: String?
    var estimateID: String?
    
    //Variables
    var shoppingSuppliesListArr = [AddedSupplyItem]()
    var generatedPDFLink: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if existingShoppingListID != nil || estimateID != nil {
            getSearchSupplyItemsList(existingShoppingListId: existingShoppingListID ?? "", existingEstimateId: estimateID ?? "")
        }
        
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "SHOPPING SUPPLIES LIST "
        self.emptyMessageStack.isHidden = self.shoppingSuppliesListArr.isEmpty ? false : true
        shoppingSuppliesTableView.register(UINib(nibName: "SupplyDisplayTblCell", bundle: nil), forCellReuseIdentifier: "SupplyDisplayTblCell")
        shoppingSuppliesTableView.delegate = self
        shoppingSuppliesTableView.dataSource = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        shoppingSuppliesTableHeight.constant = shoppingSuppliesTableView.contentSize.height
    }
    
}

//MARK: IB-ACTIONS IMPLEMENTATION
extension ShoppingSuppliesListVC: WKNavigationDelegate {
    
    @IBAction func addSuppliesBtnTapped(_ sender: UIButton) {
        
        guard let addShoppingSuppliesVc = storyboard?.instantiateViewController(withIdentifier: "AddShoppingSuppliesVC") as? AddShoppingSuppliesVC else {return}
        
        addShoppingSuppliesVc.finalizeAddedSupplies = { [weak self] (finalizedSuppliesArr: [AddedSupplyItem]) -> () in
            guard let self = self else {return}
            
            
            finalizedSuppliesArr.forEach { (supplyItem) in
                if self.shoppingSuppliesListArr.contains(where: {$0.id == supplyItem.id}) {
                    return
                }
                else {
                    self.shoppingSuppliesListArr.append(supplyItem)
                }
            }
            
            self.shoppingSuppliesTableView.reloadData()
            self.updateViewConstraints()
            self.view.layoutIfNeeded()
            self.emptyMessageStack.isHidden = self.shoppingSuppliesListArr.isEmpty ? false : true
            self.grandTotalPriceLbl.text = "$ \(self.shoppingSuppliesListArr.reduce(0) {$0 + $1.computedPrice})"
            
        }
        navigationController?.pushViewController(addShoppingSuppliesVc, animated: true)
    }
    
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        
        var suppliesArrToServer = [[String:Any]]()
        
        shoppingSuppliesListArr.forEach { (supplies) in
            suppliesArrToServer.append(supplies.dictionaryRepresentation)
            //print(suppliesArrToServer)
            
        }
        
        createEditShoppingList(existingShoppingListId: existingShoppingListID ?? "", existingEstimateId: estimateID ?? "", existingSuppliesArr: suppliesArrToServer)
    }
    
    @IBAction func printPDFBtnTapped(_ sender: UIButton) {
       //Print PDF HERE
        
        guard let generatedPDFLink = generatedPDFLink else {
            view.makeToast("Save the list before printing", duration: 2.0, position: .bottom)
            return }
        
        guard let commonWebViewController = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonWebviewController") as? CommonWebviewController else {return}
        commonWebViewController.urlLinkToOpen = generatedPDFLink
        commonWebViewController.title = "Shopping List PDF"
        navigationController?.pushViewController(commonWebViewController, animated: true)
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ShoppingSuppliesListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shoppingSuppliesListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SupplyDisplayTblCell") as? SupplyDisplayTblCell else {return UITableViewCell()}
        cell.configureCell(addedItem: shoppingSuppliesListArr[indexPath.row], delegate: self, indexpath: indexPath)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
}

//MARK: SupplyDisplay TABLE CELL DELEGATE IMPLEMENTATION
extension ShoppingSuppliesListVC: SupplyDisplayTblCellDelegate {
    
    func didTapAddItemBtn(addedItem: AddedSupplyItem, at index: IndexPath) {
        addedItem.itemCount += 1
        self.grandTotalPriceLbl.text = "$ \(self.shoppingSuppliesListArr.reduce(0) {$0 + $1.computedPrice})"
    }
    
    func didTapRemoveItemBtn(addedItem: AddedSupplyItem, at index: IndexPath) {
        
        if addedItem.itemCount > 1 {
            addedItem.itemCount -= 1
            self.grandTotalPriceLbl.text = "$ \(self.shoppingSuppliesListArr.reduce(0) {$0 + $1.computedPrice})"
        }
        else {
            view.makeToast("Already at the lowest quantity", duration: 1.0, position: .bottom)
        }
        
    }
    
    func didTapDeleteItemBtn(addedItem: AddedSupplyItem, at index: IndexPath) {
        
        //Todo: Calls Common alert and then continues delete functionality inside closure
        
        guard let commonAlertvc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonAlertVC") as? CommonAlertVC else {return}
        
        commonAlertvc.onOkBtnTap = { [weak self] (success: Bool) -> () in
            guard let self = self else {return}
            
            print("This will remove item completely")
            self.shoppingSuppliesListArr.remove(at: index.row)
            self.shoppingSuppliesTableView.reloadData()
            self.updateViewConstraints()
            self.emptyMessageStack.isHidden = self.shoppingSuppliesListArr.isEmpty ? false : true
            self.grandTotalPriceLbl.text = "$ \(self.shoppingSuppliesListArr.reduce(0) {$0 + $1.computedPrice})"
            
        }
        commonAlertvc.alertTitle = "Delete Supply Item"
        commonAlertvc.alertMessage = "Are you sure about deleting this Supply Item?"
        commonAlertvc.actionButtonTitle = "YES"
        commonAlertvc.modalPresentationStyle = .overCurrentContext
        commonAlertvc.modalTransitionStyle = .crossDissolve
        self.present(commonAlertvc, animated: true, completion: nil)
        
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension ShoppingSuppliesListVC {
    
    //Todo: Get Existing Search Supplies Item with estimate or shopping id
    func getSearchSupplyItemsList(existingShoppingListId: String, existingEstimateId: String) {
        
        view.makeToastActivity(.center)
        
        let parameters: [String:Any] = ["estimate_id" : existingEstimateId, "shopping_list_id" : existingShoppingListId]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.get_search_supplies_items, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (searchSupplies: SearchSupplyItem!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResonse = searchSupplies {
                    
                    if returnedResonse.success == "true" {
                        //Success
                        
                        if let returnedData = returnedResonse.data {
                            
                            returnedData.forEach { (supplyItem) in
                                self.shoppingSuppliesListArr.append(AddedSupplyItem(id: supplyItem.id, itemName: supplyItem.name, supplierName: supplyItem.supplierName, itemPrice: supplyItem.price, itemImage: supplyItem.image, itemCount: supplyItem.quantity ?? 1))
                            }
                            //print(self.searchSupplyItemsArr)
                            
                        }
                        else {
                            self.view.makeToast(returnedResonse.message, duration: 2.0, position: .bottom)
                        }
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.shoppingSuppliesTableView.reloadData()
                            self.updateViewConstraints()
                            self.view.layoutIfNeeded()
                            self.grandTotalPriceLbl.text = "$ \(self.shoppingSuppliesListArr.reduce(0) {$0 + $1.computedPrice})"
                            self.emptyMessageStack.isHidden = self.shoppingSuppliesListArr.isEmpty ? false : true
                            
                        }
                        
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResonse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //Todo: Create or Edit Shopping List
    func createEditShoppingList(existingShoppingListId: String, existingEstimateId: String, existingSuppliesArr: [[String:Any]]) {
        
        view.makeToastActivity(.center)
        
        let parameters: [String:Any] = ["estimate_id" : existingEstimateId, "shopping_list_id" : existingShoppingListId, "supplies" : existingSuppliesArr]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.create_edit_shopping_list, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (createEditShoppingList: CreateShoppingList!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                self.view.hideToastActivity()
                self.view.makeToast(error, duration: 2.0, position: .bottom)
            }
            else {
                
                if let returnedResponse = createEditShoppingList {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        
                        if let returnedData = returnedResponse.data {
                            print("Save PDF Here")
                            self.generatedPDFLink = returnedData.pdf
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            self.shoppingSuppliesListArr.removeAll()
                            
                            //This will call the newly created shopping list using the created id.
                            if let returnedData = returnedResponse.data {
                                self.existingShoppingListID = "\(returnedData.id)"
                                self.getSearchSupplyItemsList(existingShoppingListId: self.existingShoppingListID ?? "", existingEstimateId: self.estimateID ?? "")
                            }
                            
                            
                        }
                        else {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                        
                    }
                    else {
                        //Failure
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
