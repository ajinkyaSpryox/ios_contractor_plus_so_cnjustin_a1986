//
//  CreateNewShoppingListVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 11/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class CreateNewShoppingListVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var selectEstimateTextField: UITextField!
    
    //Variables
    var pickerView = UIPickerView()
    var estimatePickerDataArr = [Estimate]()
    var selectedEstimateId = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        initialUISetup()
        
    }
    
    private func initialUISetup() {
        title = "CREATE NEW SHOPPING LIST"
        selectEstimateTextField.delegate = self
        serviceCalls()
    }
    
    private func serviceCalls() {
        getAllEstimateList()
    }

}

//MARK: IB-ACTIONS IMPLEMENTATION
extension CreateNewShoppingListVC {
    
    @IBAction func generateShoppingListBtnTapped(_ sender: UIButton) {
        guard let shoppingSuppliesListVc = storyboard?.instantiateViewController(withIdentifier: "ShoppingSuppliesListVC") as? ShoppingSuppliesListVC else {return}
        
        if selectedEstimateId > 0 {
            shoppingSuppliesListVc.estimateID = "\(selectedEstimateId)"
        }
        
        navigationController?.pushViewController(shoppingSuppliesListVc, animated: true)
    }
    
}

//MARK: UITEXT FIELD DELEGATE IMPLEMENTATION
extension CreateNewShoppingListVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == selectEstimateTextField {
            estimatePickerDataArr.count == 0 ? view.makeToast("No Estimate Found", duration: 2.0, position: .center) : addPickerToEstimateTextField()
            
        }
        
    }
    
    func addPickerToEstimateTextField() {
        let estimatePicker = UIPickerView()
        selectEstimateTextField.inputView = estimatePicker
        estimatePicker.delegate = self
        estimatePicker.dataSource = self
    }
    
}

extension CreateNewShoppingListVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return estimatePickerDataArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return estimatePickerDataArr[row].displayNameInList
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectEstimateTextField.text = estimatePickerDataArr[row].displayNameInList
        self.selectedEstimateId = estimatePickerDataArr[row].id
        
    }
    
    
    
}

//MARK: API SERVICE IMPLEMENTATIONS
extension CreateNewShoppingListVC {
    
    func getAllEstimateList() {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters = ["page_number": 0]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_estimate, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (listEstimates: ListEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = listEstimates {
                    
                    if returnedResponse.success == "true" {
                        //success
                        
                        if let data = returnedResponse.data {
                            
                            data.estimatesDetailData.forEach { (estimate) in
                                self.estimatePickerDataArr = estimate.estimates
                            }
                            
                        }
                         //print(self.estimatePickerDataArr)
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                }
            }
            
            
        }
        
    }
    
    
    
}
