//
//  AddShoppingSuppliesVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 12/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class AddShoppingSuppliesVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var emptyMessageStack: UIStackView!
    @IBOutlet weak var searchSuppliesTextField: UITextField!
    @IBOutlet weak var searchSuppliesTableView: UITableView!
    @IBOutlet weak var searchSuppliesTableHeight: NSLayoutConstraint!
    @IBOutlet weak var selectedSuppliesDisplayTableView: UITableView!
    @IBOutlet weak var selectedSuppliesDisplayTableHeight: NSLayoutConstraint!
    
    //Variables
    var searchSupplyItemsArr = [SearchSupplyItemData]()
    var addedSupplyItemsArr = [AddedSupplyItem]()
    
    //Searching logic Variables
    var searchSupplyItemsFilterArr = [SearchSupplyItemData]()
    var isSearching = false
    
    var finalizeAddedSupplies: ((_ addedSupplyItemsArr: [AddedSupplyItem]) -> ())?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchSuppliesTableView.isHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "ADD SUPPLIES"
        searchSuppliesTextField.delegate = self
        searchSuppliesTableView.delegate = self
        searchSuppliesTableView.dataSource = self
        selectedSuppliesDisplayTableView.delegate = self
        selectedSuppliesDisplayTableView.dataSource = self
        searchSuppliesTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: .editingChanged)
        searchSuppliesTableView.register(UINib(nibName: "SearchSuppliesItemTblCell", bundle: nil), forCellReuseIdentifier: "SearchSuppliesItemTblCell")
        selectedSuppliesDisplayTableView.register(UINib(nibName: "SupplyDisplayTblCell", bundle: nil), forCellReuseIdentifier: "SupplyDisplayTblCell")
        serviceCall()
    }
    
    private func serviceCall() {
        getSearchSupplyItemsList()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        searchSuppliesTableHeight.constant = searchSuppliesTableView.contentSize.height
        selectedSuppliesDisplayTableHeight.constant = selectedSuppliesDisplayTableView.contentSize.height
    }

}

//MARK: IB-ACTIONS IMPLEMENTATION
extension AddShoppingSuppliesVC {
    
    @IBAction func addSuppliesBtnTapped(_ sender: UIButton) {
        finalizeAddedSupplies?(addedSupplyItemsArr)
        navigationController?.popViewController(animated: true)
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension AddShoppingSuppliesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        case selectedSuppliesDisplayTableView:
            return addedSupplyItemsArr.count
        default:
            return isSearching ? searchSupplyItemsFilterArr.count : searchSupplyItemsArr.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        
        case selectedSuppliesDisplayTableView:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SupplyDisplayTblCell") as? SupplyDisplayTblCell else {return UITableViewCell()}
            cell.configureCell(addedItem: addedSupplyItemsArr[indexPath.row], delegate: self, indexpath: indexPath)
            return cell
            
        default:
            
            if isSearching {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchSuppliesItemTblCell") as? SearchSuppliesItemTblCell else {return UITableViewCell()}
                cell.configureCell(searchSupplyItem: searchSupplyItemsFilterArr[indexPath.row])
                return cell
            }
            else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchSuppliesItemTblCell") as? SearchSuppliesItemTblCell else {return UITableViewCell()}
                cell.configureCell(searchSupplyItem: searchSupplyItemsArr[indexPath.row])
                return cell
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch tableView {
            
        case searchSuppliesTableView:
            
            let selectedItem = searchSupplyItemsFilterArr[indexPath.row]
            
            //Checks for alreay existing items
            if addedSupplyItemsArr.contains(where: {$0.id == selectedItem.id}) {
                searchSuppliesTextField.resignFirstResponder()
                view.makeToast("You already have this item in List", duration: 1.0, position: .bottom)
            }
            else {
                
                //Adds new Items to the List
                addedSupplyItemsArr.append(AddedSupplyItem(id: selectedItem.id, itemName: selectedItem.name, supplierName: selectedItem.supplierName, itemPrice: selectedItem.price, itemImage: selectedItem.image, itemCount: 1))
                
                //searchSupplyItemFilterArr.removeAll()
                searchSuppliesTextField.resignFirstResponder()
                searchSuppliesTextField.text = ""
                searchSuppliesTableView.isHidden = true
                searchSuppliesTableView.reloadData()
                selectedSuppliesDisplayTableView.reloadData()
                updateViewConstraints()
            }
            
            emptyMessageStack.isHidden = addedSupplyItemsArr.isEmpty ? false : true
            
        default:
            return
        }
        
    }
    
}

//MARK: SupplyDisplay TABLE CELL DELEGATE IMPLEMENTATION
extension AddShoppingSuppliesVC: SupplyDisplayTblCellDelegate {
    
    func didTapAddItemBtn(addedItem: AddedSupplyItem, at index: IndexPath) {
        addedItem.itemCount += 1
    }
    
    func didTapRemoveItemBtn(addedItem: AddedSupplyItem, at index: IndexPath) {
        
        if addedItem.itemCount > 1 {
            addedItem.itemCount -= 1
        }
        else {
            view.makeToast("Already at the lowest quantity", duration: 1.0, position: .bottom)
        }
        
    }
    
    func didTapDeleteItemBtn(addedItem: AddedSupplyItem, at index: IndexPath) {
        
        //Todo: Calls Common alert and then continues delete functionality inside closure
        
        guard let commonAlertvc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonAlertVC") as? CommonAlertVC else {return}
        
        commonAlertvc.onOkBtnTap = { [weak self] (success: Bool) -> () in
            guard let self = self else {return}
            
            print("This will remove item completely")
            self.addedSupplyItemsArr.remove(at: index.row)
            self.selectedSuppliesDisplayTableView.reloadData()
            self.updateViewConstraints()
            self.emptyMessageStack.isHidden = self.addedSupplyItemsArr.isEmpty ? false : true
            
        }
        commonAlertvc.alertTitle = "Delete Supply Item"
        commonAlertvc.alertMessage = "Are you sure about deleting this Supply Item?"
        commonAlertvc.actionButtonTitle = "YES"
        commonAlertvc.modalPresentationStyle = .overCurrentContext
        commonAlertvc.modalTransitionStyle = .crossDissolve
        self.present(commonAlertvc, animated: true, completion: nil)
        
    }
    
}


//MARK: TEXT FIELD DELEGATE IMPLEMENTATION
extension AddShoppingSuppliesVC: UITextFieldDelegate {
    
    @objc
    func textFieldEditingDidChange(_ textField: UITextField) {
        
        switch textField {
        
        case searchSuppliesTextField:
            guard let text = textField.text else {return}
            searchSuppliesTableView.isHidden = text.isEmpty ? true : false
            
            if !text.isEmpty {
                isSearching = true
                searchSupplyItemsFilterArr = searchSupplyItemsArr.filter({$0.name.lowercased().contains(text.lowercased())})
                searchSuppliesTableView.reloadData()
                updateViewConstraints()
                emptyMessageStack.isHidden = true
            }
            else {
                isSearching = false
                searchSuppliesTableView.reloadData()
                updateViewConstraints()
                searchSuppliesTextField.resignFirstResponder()
            }
            
        default:
            return
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textField.resignFirstResponder()
        self.emptyMessageStack.isHidden = self.addedSupplyItemsArr.isEmpty ? false : true
        return true
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension AddShoppingSuppliesVC {
    
    func getSearchSupplyItemsList() {
        
        view.makeToastActivity(.center)
        
        let parameters = [String:Any]()
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.get_search_supplies_items, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (searchSupplies: SearchSupplyItem!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResonse = searchSupplies {
                    
                    if returnedResonse.success == "true" {
                        //Success
                        
                        if let returnedData = returnedResonse.data {
                           self.searchSupplyItemsArr = returnedData
                        }
                        else {
                           self.view.makeToast(returnedResonse.message, duration: 2.0, position: .bottom)
                        }
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.searchSuppliesTableView.reloadData()
                            
                        }
                        //print(self.searchSupplyItemsArr)
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResonse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
}
