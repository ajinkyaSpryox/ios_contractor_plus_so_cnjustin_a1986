//
//  ListShoppingListsVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 11/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ListShoppingListsVC: UIViewController {
    
    @IBOutlet weak var shoppingListTableView: UITableView!
    @IBOutlet weak var emptyStackView: UIStackView!
    
    //Variables
    var shoppingListArr = [ShoppingList]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        serviceCalls()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        shoppingListArr.removeAll()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "SHOPPING LIST"
        shoppingListTableView.delegate = self
        shoppingListTableView.dataSource = self
        shoppingListTableView.register(UINib(nibName: "ShoppingListDetailsTblCell", bundle: nil), forCellReuseIdentifier: "ShoppingListDetailsTblCell")
    }
    
    private func serviceCalls() {
        listShoppingLists()
    }

}

//MARK: IB-ACTIONS IMPLEMENTATION
extension ListShoppingListsVC {
    
    @IBAction func createNewShoppingListBtnTapped(_sender: UIButton) {
        guard let createNewShoppingListVc = storyboard?.instantiateViewController(withIdentifier: "CreateNewShoppingListVC") as? CreateNewShoppingListVC else {return}
        navigationController?.pushViewController(createNewShoppingListVc, animated: true)
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListShoppingListsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shoppingListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingListDetailsTblCell") as? ShoppingListDetailsTblCell else {return UITableViewCell()}
        cell.configureCell(shoppingListDetails: shoppingListArr[indexPath.row], delegate: self, indexpath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: SHOPPING LIST DETAILS TABLE CELL DELEGATE IMPLEMENTATION
extension ListShoppingListsVC: ShoppingListDetailsTblCellDelegate {
    
    func didTapEditShoppingListBtn(shoppingList: ShoppingList, at indexpath: IndexPath) {
        //Go to Shopping List supplies list view controller
        guard let shoppingSuppliesListVc = storyboard?.instantiateViewController(withIdentifier: "ShoppingSuppliesListVC") as? ShoppingSuppliesListVC else {return}
        shoppingSuppliesListVc.existingShoppingListID = "\(shoppingList.id)"
        if let estimateId = shoppingList.estimateID {
            shoppingSuppliesListVc.estimateID = "\(estimateId)"
        }
        navigationController?.pushViewController(shoppingSuppliesListVc, animated: true)
    }
    
    func didTapDeleteShoppingListBtn(shoppingList: ShoppingList, at indexpath: IndexPath) {
        shoppingListArr.remove(at: indexpath.row)
        shoppingListTableView.deleteRows(at: [indexpath], with: .fade)
        deleteShoppingListWith(shoppingListId: shoppingList.id)
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension ListShoppingListsVC {
    
    func listShoppingLists() {
        
        let parameters = [String:Any]()
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        view.makeToastActivity(.center)
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_shopping_lists, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (listShoppingLists: ListShoppingLists!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                self.view.hideToastActivity()
                self.view.makeToast(error, duration: 2.0, position: .bottom)
                
            }
            else {
                
                if let returnedResponse = listShoppingLists {
                    
                    if returnedResponse.success == "true" {
                        
                        if let returnedData = returnedResponse.data {
                            self.shoppingListArr = returnedData.shoppingList
                        }
                        self.view.hideToastActivity()
                        self.shoppingListTableView.reloadData()
                        self.emptyStackView.isHidden = self.shoppingListArr.isEmpty ? false : true
                        
                    }
                    else {
                        //Failure
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //Todo: Delete Shopping List Service
    func deleteShoppingListWith(shoppingListId: Int) {
        
        let parameters: [String:Any] = ["id" : shoppingListId]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.delete_shopping_list, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (shoppingList: DeleteEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            self.view.makeToastActivity(.center)
            
            if let error = errorMessage {
                self.view.hideToastActivity()
                self.view.makeToast(error, duration: 2.0, position: .bottom)
            }
            else {
                if let returnedResponse = shoppingList {
                    self.view.hideToastActivity()
                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    self.shoppingListTableView.reloadData()
                    self.emptyStackView.isHidden = self.shoppingListArr.isEmpty ? false : true
                }
            }
            
        }
        
    }
    
    
}
