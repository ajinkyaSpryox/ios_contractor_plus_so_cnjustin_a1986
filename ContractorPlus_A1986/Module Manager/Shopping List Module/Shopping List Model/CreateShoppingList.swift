//
//  CreateShoppingList.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 13/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - CreateShoppingList
struct CreateShoppingList: Codable {
    let success, message: String
    let data: CreateShoppingListData?
}

// MARK: - DataClass
struct CreateShoppingListData: Codable {
    let id: Int
    let pdf: String
}

