//
//  ListShoppingLists.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 11/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListShoppingLists
struct ListShoppingLists: Codable {
    let success, message: String
    let data: ListShoppingListsData?
}

// MARK: - DataClass
struct ListShoppingListsData: Codable {
    let shoppingList: [ShoppingList]

    enum CodingKeys: String, CodingKey {
        case shoppingList = "shopping_list"
    }
}

// MARK: - ShoppingList
struct ShoppingList: Codable {
    let id: Int
    let shoppingListNo: String
    let estimateID: Int?
    let estimateNo, date, clientName: String
    let totalItems: Int
    

    enum CodingKeys: String, CodingKey {
        case id
        case shoppingListNo = "shopping_list_no"
        case estimateID = "estimate_id"
        case estimateNo = "estimate_no"
        case date
        case clientName = "client_name"
        case totalItems = "total_items"
    }
}
