//
//  ListScheduleJob.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 26/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListScheduleJob
struct ListScheduleJob: Codable {
    let success, message: String
    let data: ListScheduleJobData?
}

// MARK: - DataClass
struct ListScheduleJobData: Codable {
    let jobList: [JobList]
    let pageCount: Int

    enum CodingKeys: String, CodingKey {
        case jobList = "job_list"
        case pageCount = "page_count"
    }
}

// MARK: - JobList
struct JobList: Codable {
    let date: String
    var jobs: [Job]
}

// MARK: - Job
struct Job: Codable {
    let id: Int
    let invoiceNo, jobNo, address, startDate: String
    let endDate: String
    let assignedTeamMember: [AssignedTeamMember]
    let postInspector: String

    enum CodingKeys: String, CodingKey {
        case id
        case invoiceNo = "invoice_no"
        case jobNo = "job_no"
        case address
        case startDate = "start_date"
        case endDate = "end_date"
        case assignedTeamMember = "assigned_team_member"
        case postInspector = "post_inspector"
    }
}



// MARK: - AssignedTeamMember
struct AssignedTeamMember: Codable {
    let id: Int
    let name: String
    let picture: String?
    let isAdmin: String?
    var isSelected = false
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case picture
        case isAdmin = "is_admin"
    }
}
