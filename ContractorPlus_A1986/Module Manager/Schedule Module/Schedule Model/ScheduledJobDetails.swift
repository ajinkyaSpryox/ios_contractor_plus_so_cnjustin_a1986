//
//  ScheduledJobDetails.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 28/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ScheduledJobDetails
struct ScheduledJobDetails: Codable {
    let success, message: String
    let data: ScheduledJobDetailsJobData
}

// MARK: - DataClass
struct ScheduledJobDetailsJobData: Codable {
    let id, invoiceID: Int
    let invoiceNo, jobNo, fromDate, toDate: String
    let postInspectorDetails: PostInspectorDetails
    let assignedToTeamMembers: [AssignedTeamMember]

    enum CodingKeys: String, CodingKey {
        case id
        case invoiceID = "invoice_id"
        case invoiceNo = "invoice_no"
        case jobNo = "job_no"
        case fromDate = "from_date"
        case toDate = "to_date"
        case postInspectorDetails = "post_inspector_details"
        case assignedToTeamMembers = "assigned_to_team_members"
    }
}

// MARK: - PostInspectorDetails
struct PostInspectorDetails: Codable {
    let id: Int
    let name: String
}
