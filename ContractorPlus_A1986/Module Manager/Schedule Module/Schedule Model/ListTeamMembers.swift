//
//  ListTeamMembers.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 27/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListTeamMember
struct ListTeamMember: Codable {
    let success, message: String
    let data: [AssignedTeamMember]?
}
