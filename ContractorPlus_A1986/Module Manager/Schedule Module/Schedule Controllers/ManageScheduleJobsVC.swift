//
//  ManageScheduleJobsVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 26/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ManageScheduleJobsVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var scheduleJobTblView: UITableView!
    @IBOutlet weak var emptyMessageStack: UIStackView!
    @IBOutlet weak var fromDateStackView: UIStackView!
    @IBOutlet weak var toDateStackView: UIStackView!
    
    //From Date Outlets
    @IBOutlet weak var fromDateLbl: UILabel!
    @IBOutlet weak var fromMonthYearLbl: UILabel!
    @IBOutlet weak var fromWeekDayLbl: UILabel!
    
    //To Date Outlets
    @IBOutlet weak var toDateLbl: UILabel!
    @IBOutlet weak var toMonthYearLbl: UILabel!
    @IBOutlet weak var toWeekDayLbl: UILabel!
    
    //Variables
    var scheduledJobsDataArr = [JobList]()
    let dateFormatter = DateFormatter()
    var selectedFromDateString = ""
    var selectedToDateString = ""
    
    //Pagination Variables
    var currentPageNumber = 1
    var maxPageNumber = 0
    var isLoadingMoreInvoices = false
    var cellHeights = [IndexPath: CGFloat]()
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        serviceCalls()
        setupInitialUI()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        currentPageNumber = 1
        scheduledJobsDataArr.removeAll()
    }
    private func setupInitialUI() {
        title = "Scheduled Jobs"
        
        scheduleJobTblView.register(UINib(nibName: "ManageScheduleListHeadertblCell", bundle: nil), forCellReuseIdentifier: "ManageScheduleListHeadertblCell")
        scheduleJobTblView.register(UINib(nibName: "ManageScheduleJobTblCell", bundle: nil), forCellReuseIdentifier: "ManageScheduleJobTblCell")
        scheduleJobTblView.delegate = self
        scheduleJobTblView.dataSource = self
        
        let fromDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectFromDate(_:)))
        fromDateStackView.addGestureRecognizer(fromDateTapGesture)
        
        let toDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectToDate(_:)))
        toDateStackView.addGestureRecognizer(toDateTapGesture)
        
        setCurrentDateOnFromDate()
        setTomorrowDateOnToDate()
    }
    
    private func serviceCalls() {
        getScheduledJobsList(pageNumber: currentPageNumber, fromDate: "", toDate: "")
    }
    
}

//MARK: IB-ACTIONS IMPLEMENTATION
extension ManageScheduleJobsVC {
    
    @IBAction func addJobBtnTapped(_ sender: UIButton) {
        guard let addJobvc = storyboard?.instantiateViewController(withIdentifier: "AddJobVC") as? AddJobVC else {return}
        navigationController?.pushViewController(addJobvc, animated: true)
    }
    
}

//MARK: TABLE VIEW DELEGATE AND DATASOURCE IMPLEMENTATION
extension ManageScheduleJobsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return scheduledJobsDataArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scheduledJobsDataArr[section].jobs.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ManageScheduleListHeadertblCell") as? ManageScheduleListHeadertblCell else {return UIView()}
        cell.configureCell(jobDate: scheduledJobsDataArr[section])
        return cell.contentView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ManageScheduleJobTblCell") as? ManageScheduleJobTblCell else {return UITableViewCell()}
        cell.configureCell(jobDetails: scheduledJobsDataArr[indexPath.section].jobs[indexPath.row], delegate: self, indexpath: indexPath)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if currentPageNumber < maxPageNumber && !isLoadingMoreInvoices {
            currentPageNumber += 1
            getScheduledJobsList(pageNumber: currentPageNumber, fromDate: selectedFromDateString, toDate: selectedToDateString)
        }
        
    }
    
}

//MARK: ManageScheduleJobTblCell Delegate Implementation
extension ManageScheduleJobsVC: ManageScheduleJobTblCellDelegate {
    
    func didTapDeleteScheduleJobBtn(jobDetails: Job, at indexpath: IndexPath) {
        
        //Todo: Calls Common alert and then continues delete functionality inside closure
        
        guard let commonAlertvc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonAlertVC") as? CommonAlertVC else {return}
        
        commonAlertvc.onOkBtnTap = { [weak self] (success: Bool) -> () in
            
            guard let self = self else {return}
            
            self.scheduledJobsDataArr[indexpath.section].jobs.remove(at: indexpath.row)
            self.scheduleJobTblView.deleteRows(at: [indexpath], with: .fade)
            self.deleteScheduledJob(jobId: jobDetails.id)
            
            if indexpath.section == 0 && indexpath.row == 0 {
                self.scheduledJobsDataArr.removeAll()
                //self.scheduledJobsDataArr.remove(at: indexpath.section)
                self.scheduleJobTblView.reloadData()
            }
            
        }
        commonAlertvc.alertTitle = "Delete Scheduled Job"
        commonAlertvc.alertMessage = "Are you sure about deleting this job?"
        commonAlertvc.actionButtonTitle = "YES"
        commonAlertvc.modalPresentationStyle = .overCurrentContext
        commonAlertvc.modalTransitionStyle = .crossDissolve
        self.present(commonAlertvc, animated: true, completion: nil)
        
    }
    
    func didTapEditScheduleJobBtn(jobDetails: Job, at indexpath: IndexPath) {
        guard let addJobVc = storyboard?.instantiateViewController(withIdentifier: "AddJobVC") as? AddJobVC else {return}
        addJobVc.existingJobId = jobDetails.id
        navigationController?.pushViewController(addJobVc, animated: true)
        
    }
    
    func didTapViewScheduleJobBtn(jobDetails: Job, at indexpath: IndexPath) {
        
        guard let timeClockVc = STORYBOARDNAMES.TIME_CLOCK_STORYBOARD.instantiateViewController(withIdentifier: "TimeClockInOutVC") as? TimeClockInOutVC else {return}
        timeClockVc.jobDetails = jobDetails
        navigationController?.pushViewController(timeClockVc, animated: true)
    }
    
}


//MARK: ALL DATE SELECTION LOGIC HERE
extension ManageScheduleJobsVC {
    
    @objc
    func selectFromDate(_ sender: UITapGestureRecognizer) {
        //print("From Date To Be Set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = true
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    @objc
    func selectToDate(_ sender: UITapGestureRecognizer) {
        //print("To Date to be set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = false
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    func setCurrentDateOnFromDate() {
        
        let todaysDate = Date()
        
        dateFormatter.dateFormat = "d"
        let currentDate = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let currentMonthYear = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "EEEE"
        let currentWeekday = dateFormatter.string(from: todaysDate)
        
        fromDateLbl.text = currentDate
        fromMonthYearLbl.text = currentMonthYear
        fromWeekDayLbl.text = currentWeekday
        dateFormatter.dateFormat = formattedDateString
        selectedFromDateString = dateFormatter.string(from: todaysDate)
                
    }
    
    func setTomorrowDateOnToDate() {
        
        let nextDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        
        dateFormatter.dateFormat = "d"
        let tomorrowDate = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let tomorrowMonthYear = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "EEEE"
        let tomorrowWeekday = dateFormatter.string(from: nextDate!)
        
        toDateLbl.text = tomorrowDate
        toMonthYearLbl.text = tomorrowMonthYear
        toWeekDayLbl.text = tomorrowWeekday
        dateFormatter.dateFormat = formattedDateString
        selectedToDateString = dateFormatter.string(from: nextDate!)
                
    }
    
    
}

//MARK: COMMON DATE PICKER VIEW CONTROLLER DELEGATE IMPLEMENTATION
extension ManageScheduleJobsVC: CommonDatePickerVCDelegate {
    
    func getSelectedDateFromPicker(date: String, monthYear: String, weekDay: String, isFromDate: Bool, originalSelectedDate: String) {
        
        if isFromDate {
            
            if originalSelectedDate == "" {
                setCurrentDateOnFromDate()
            }
            else {
                fromDateLbl.text = date
                fromMonthYearLbl.text = monthYear
                fromWeekDayLbl.text = weekDay
                selectedFromDateString = originalSelectedDate
            }
            
        }
        else {
            
            if originalSelectedDate == "" {
                setTomorrowDateOnToDate()
            }
            else {
                toDateLbl.text = date
                toMonthYearLbl.text = monthYear
                toWeekDayLbl.text = weekDay
                selectedToDateString = originalSelectedDate
            }
            
        }
        
        if selectedFromDateString != "" || selectedToDateString != "" { //Service call after date filteration is complete
            currentPageNumber = 1
            isLoadingMoreInvoices = false
            scheduledJobsDataArr.removeAll()
            getScheduledJobsList(pageNumber: currentPageNumber, fromDate: selectedFromDateString, toDate: selectedToDateString)
            
        }
        
    }
    
}


//MARK: API SERVICES IMPLEMENTATION
extension ManageScheduleJobsVC {
    
    //MARK: GET ESTIMATE LIST SERVICE
    func getScheduledJobsList(pageNumber: Int, fromDate: String, toDate: String) {
        
        self.view.makeToastActivity(.bottom)
        
        isLoadingMoreInvoices = true
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["page_number": pageNumber, "from_date": fromDate, "to_date": toDate]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_scheduled_jobs, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (listScheduleJob: ListScheduleJob!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = listScheduleJob {
                    
                    if returnedResponse.success == "true" {
                        //success
                        
                        if let data = returnedResponse.data {
                            
                            self.maxPageNumber = data.pageCount
                            
                            returnedResponse.data?.jobList.forEach({ (detail) in
                                self.scheduledJobsDataArr.append(detail)
                            })
                            
                            DispatchQueue.main.async {
                                self.emptyMessageStack.isHidden = self.scheduledJobsDataArr.isEmpty ? false : true
                                self.view.hideToastActivity()
                                self.scheduleJobTblView.reloadData()
                            }
                            
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            print(returnedResponse.message)
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            self.scheduleJobTblView.reloadData()
                            self.emptyMessageStack.isHidden = self.scheduledJobsDataArr.isEmpty ? false : true
                            
                        }
                    }
                }
            }
            
            self.isLoadingMoreInvoices = false
        }
        
    }
    
    // Delete Schedule Job Service
    func deleteScheduledJob(jobId: Int) {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["job_id": jobId]
        
        GenericWebservice.instance.getServiceData(url: Webservices.delete_scheduled_job, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (deletedScheduleJob: DeleteEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = deletedScheduleJob {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.scheduleJobTblView.reloadData()
                            self.emptyMessageStack.isHidden = self.scheduledJobsDataArr.isEmpty ? false : true
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            self.emptyMessageStack.isHidden = self.scheduledJobsDataArr.isEmpty ? false : true
                            self.view.hideToastActivity()
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
