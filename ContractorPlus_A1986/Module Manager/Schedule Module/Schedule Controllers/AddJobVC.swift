//
//  AddJobVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 27/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class AddJobVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var listTeamMemberTblView: UITableView!
    @IBOutlet weak var listTeamMemberTblHeight: NSLayoutConstraint!
    @IBOutlet weak var selectedTeamMemberTblView: UITableView!
    @IBOutlet weak var selectedTeamMemberTblHeight: NSLayoutConstraint!
    @IBOutlet weak var selectTeamMemberView: CustomView!
    @IBOutlet weak var addJobBtn: UIButton!
    
    @IBOutlet weak var invoiceTextField: UITextField!
    @IBOutlet weak var postInspectorTextField: UITextField!
    
    @IBOutlet weak var startDateStackView: UIStackView!
    @IBOutlet weak var endDateStackView: UIStackView!
    
    //From Date Outlets
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var startMonthYearLbl: UILabel!
    @IBOutlet weak var startWeekDayLbl: UILabel!
    
    //To Date Outlets
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var endMonthYearLbl: UILabel!
    @IBOutlet weak var endWeekDayLbl: UILabel!
    
    //Variables
    var listTeamMembersArr = [AssignedTeamMember]()
    var selectedTeamMembersArr = [AssignedTeamMember]()
    var pickerView = UIPickerView()
    var invoicesListArray = [Invoice]()
    
    var existingJobId: Int? //This is for Editing
    var scheduledJobDetails: ScheduledJobDetailsJobData?
    
    let dateFormatter = DateFormatter()
    var selectedStartDateString = ""
    var selectedEndDateString = ""
    
    var selectedInvoiceId: Int!
    var selectedPostInspectorId: Int!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listTeamMemberTblView.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceCalls()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "ADD JOB"
        
        setCurrentDateOnFromDate()
        setTomorrowDateOnToDate()
        
        listTeamMemberTblView.register(UINib(nibName: "ListTeamMemberTblCell", bundle: nil), forCellReuseIdentifier: "ListTeamMemberTblCell")
        selectedTeamMemberTblView.register(UINib(nibName: "SelectedTeamMemberTblCell", bundle: nil), forCellReuseIdentifier: "SelectedTeamMemberTblCell")
        listTeamMemberTblView.delegate = self
        listTeamMemberTblView.dataSource = self
        selectedTeamMemberTblView.delegate = self
        selectedTeamMemberTblView.dataSource = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(selectTeamMemberViewTapped(_:)))
        selectTeamMemberView.addGestureRecognizer(tapGesture)
        
        invoiceTextField.delegate = self
        postInspectorTextField.delegate = self
        
        let startDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectFromDate(_:)))
        startDateStackView.addGestureRecognizer(startDateTapGesture)
        
        let endDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectToDate(_:)))
        endDateStackView.addGestureRecognizer(endDateTapGesture)
        
    }
    
    private func serviceCalls() {
        listTeamMembers()
        getInvoiceListFor(pageNumber: 0)
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        listTeamMemberTblHeight.constant = listTeamMemberTblView.contentSize.height
        selectedTeamMemberTblHeight.constant = selectedTeamMemberTblView.contentSize.height
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
}

//MARK: IB-ACTIONS IMPELMENTATION
extension AddJobVC {
    
    @objc
    func selectTeamMemberViewTapped(_ sender: UITapGestureRecognizer) {
        listTeamMemberTblView.isHidden = false
    }
    
    @IBAction func addJobBtnTapped(_ sender: UIButton) {
        
        if let existingJobId = self.existingJobId {
            scheduleEditJob(jobId: "\(existingJobId)") //To Edit Existing Job
        }
        else {
            scheduleEditJob(jobId: "")
        }
    }
    
}

extension AddJobVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
            
        case listTeamMemberTblView:
            return listTeamMembersArr.count
        case selectedTeamMemberTblView:
            return selectedTeamMembersArr.count
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
            
        case listTeamMemberTblView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListTeamMemberTblCell") as? ListTeamMemberTblCell else {return UITableViewCell()}
            cell.configureCell(teamMemberDetail: listTeamMembersArr[indexPath.row])
            return cell
            
        case selectedTeamMemberTblView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedTeamMemberTblCell") as? SelectedTeamMemberTblCell else {return UITableViewCell()}
            cell.configureCell(selectedTeamMember: selectedTeamMembersArr[indexPath.row], delegate: self, indexpath: indexPath)
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch tableView {
            
        case listTeamMemberTblView:
            listTeamMembersArr[indexPath.row].isSelected = !listTeamMembersArr[indexPath.row].isSelected
            tableView.reloadData()
            
            if !selectedTeamMembersArr.contains(where: {$0.id == listTeamMembersArr[indexPath.row].id}) {
                if listTeamMembersArr[indexPath.row].isSelected == true {
                    selectedTeamMembersArr.append(listTeamMembersArr[indexPath.row])
                }
            }
            else {
                
                if let index = selectedTeamMembersArr.lastIndex(where: {$0.id == listTeamMembersArr[indexPath.row].id}) {
                    selectedTeamMembersArr.remove(at: index)
                }
            }
            
            selectedTeamMemberTblView.reloadData()
            updateViewConstraints()
            //print(selectedTeamMembersArr)
            
        default:
            return
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch tableView {
        case listTeamMemberTblView:
            return UITableView.automaticDimension
        case selectedTeamMemberTblView:
            return UITableView.automaticDimension
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case listTeamMemberTblView:
            return 90
        case selectedTeamMemberTblView:
            return 80
        default:
            return 0
        }
    }
    
}

//MARK: SELECTED TEAM MEMBER TABKLE CELL DELEGATE
extension AddJobVC: SelectedTeamMemberTblCellDelegate {
    
    func didTapRemoveMemberBtn(selectedTeamMember: AssignedTeamMember, at indexpath: IndexPath) {
        
        if let index = listTeamMembersArr.lastIndex(where: {$0.id == selectedTeamMember.id}) {
            listTeamMembersArr[index].isSelected = false
            selectedTeamMembersArr.remove(at: indexpath.row)
            selectedTeamMemberTblView.deleteRows(at: [indexpath], with: .fade)
            listTeamMemberTblView.reloadData()
            selectedTeamMemberTblView.reloadData()
            updateViewConstraints()
            //print(selectedTeamMembersArr)
        }
        
    }
    
}

//MARK: TEXT FIELD DELEGATE IMPLEMENTATION
extension AddJobVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField {
        case invoiceTextField:
            invoicesListArray.count == 0 ? view.makeToast("No Invoices Found", duration: 2.0, position: .center) : addPickerToInvoiceTextField()
            
        case postInspectorTextField:
            listTeamMembersArr.count == 0 ? view.makeToast("No Post-Inspectors Found", duration: 2.0, position: .center) : addPickerToPostInspectorTextField()
            
        default:
            return
        }
        
    }
    
    func addPickerToInvoiceTextField() {
        pickerView.tag = 1
        invoiceTextField.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func addPickerToPostInspectorTextField() {
        pickerView.tag = 2
        postInspectorTextField.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
}

//MARK: PICKER VIEW DELEAGET & DATA SOURCE IMPLEMENTATION
extension AddJobVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
            
        case 1:
            return invoicesListArray.count
            
        case 2:
            return listTeamMembersArr.count
            
        default:
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch pickerView.tag {
            
        case 1:
            return invoicesListArray[row].displayNameInList
            
        case 2:
            return listTeamMembersArr[row].name
            
        default:
            return ""
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
            
        case 1:
            invoiceTextField.text = invoicesListArray[row].displayNameInList
            selectedInvoiceId = invoicesListArray[row].id
            
        case 2:
            postInspectorTextField.text = listTeamMembersArr[row].name
            selectedPostInspectorId = listTeamMembersArr[row].id
            
        default:
            return
        }
        
    }
    
}

//MARK: ALL DATE SELECTION LOGIC HERE
extension AddJobVC {
    
    @objc
    func selectFromDate(_ sender: UITapGestureRecognizer) {
        //print("From Date To Be Set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = true
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    @objc
    func selectToDate(_ sender: UITapGestureRecognizer) {
        //print("To Date to be set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = false
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    func setCurrentDateOnFromDate() {
        
        let todaysDate = Date()
        
        dateFormatter.dateFormat = "d"
        let currentDate = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let currentMonthYear = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "EEEE"
        let currentWeekday = dateFormatter.string(from: todaysDate)
        
        startDateLbl.text = currentDate
        startMonthYearLbl.text = currentMonthYear
        startWeekDayLbl.text = currentWeekday
        
    }
    
    func setTomorrowDateOnToDate() {
        
        let nextDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        
        dateFormatter.dateFormat = "d"
        let tomorrowDate = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let tomorrowMonthYear = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "EEEE"
        let tomorrowWeekday = dateFormatter.string(from: nextDate!)
        
        endDateLbl.text = tomorrowDate
        endMonthYearLbl.text = tomorrowMonthYear
        endWeekDayLbl.text = tomorrowWeekday
        
    }
    
    
}

//MARK: COMMON DATE PICKER VIEW CONTROLLER DELEGATE IMPLEMENTATION
extension AddJobVC: CommonDatePickerVCDelegate {
    
    func getSelectedDateFromPicker(date: String, monthYear: String, weekDay: String, isFromDate: Bool, originalSelectedDate: String) {
        
        if isFromDate {
            startDateLbl.text = date
            startMonthYearLbl.text = monthYear
            startWeekDayLbl.text = weekDay
            selectedStartDateString = originalSelectedDate
            
        }
        else {
            endDateLbl.text = date
            endMonthYearLbl.text = monthYear
            endWeekDayLbl.text = weekDay
            selectedEndDateString = originalSelectedDate
        }
        
    }
    
}


//MARK: API SERVICES IMPLEMENTATION
extension AddJobVC {
    
    //Get All Team Members List Service
    func listTeamMembers() {
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters = [String:Any]()
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_team_members, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (listTeamMembers: ListTeamMember!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = listTeamMembers {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        
                        if let returnedData = returnedResponse.data {
                            self.listTeamMembersArr = returnedData
                            //print(self.listTeamMembersArr)
                            
                            if let existingJobId = self.existingJobId {
                                self.getExistingJobDetails(jobId: existingJobId)
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            
                            self.listTeamMemberTblView.reloadData()
                            self.updateViewConstraints()
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            //self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            debugPrint(returnedResponse.message)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //Get All Invoices
    func getInvoiceListFor(pageNumber: Int) {

        let parameters = ["page_number": pageNumber]

        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]

        GenericWebservice.instance.getServiceData(url: Webservices.list_invoice, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [weak self] (invoices: ListInvoice!, errorMessage) in

            guard let self = self else {return}

            if let error = errorMessage {

                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }

            }
            else {

                if let returnedResponse = invoices {

                    if returnedResponse.success == "true" {
                        //Success

                        if let data = returnedResponse.data {

                            data.invoiceList.forEach { (invoice) in
                                self.invoicesListArray = invoice.invoices
                            }

                        }
                        //print(self.invoicesListArray)

                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            //self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            debugPrint(returnedResponse.message)
                        }

                    }

                }

            }

        }

    }
    
    //Create Schedule Job
    func scheduleEditJob(jobId: String) {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["invoice_id": selectedInvoiceId, "from_date": selectedStartDateString, "to_date": selectedEndDateString, "assigned_to_team_members": selectedTeamMembersArr.map({$0.id}), "post_inspector_id": (selectedPostInspectorId != nil ? selectedPostInspectorId : ""), "job_id": jobId]
        
        view.makeToastActivity(.center)
        
        GenericWebservice.instance.getServiceData(url: Webservices.schedule_job, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (scheduleJobResponse: DeleteEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if scheduleJobResponse.success == "true" {
                    //Success
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(scheduleJobResponse.message, duration: 1.5, position: .bottom)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
                else {
                    //Failure
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(scheduleJobResponse.message, duration: 2.0, position: .bottom)
                    }
                }
                
            }
            
        }
        
    }
    
    //Get Existing Job Details
    func getExistingJobDetails(jobId: Int) {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["job_id": jobId]
        
        GenericWebservice.instance.getServiceData(url: Webservices.scheduled_job_details, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (scheduledJobDetails: ScheduledJobDetails!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = scheduledJobDetails {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        self.scheduledJobDetails = returnedResponse.data
                        
                        //MARK: Here manage list team members array & selected team members array on Edit
                        self.scheduledJobDetails?.assignedToTeamMembers.forEach({ (member) in
                            if let index = self.listTeamMembersArr.lastIndex(where: {$0.id == member.id}) {
                                self.listTeamMembersArr[index].isSelected = true
                                self.selectedTeamMembersArr.append(self.listTeamMembersArr[index])
                            }
                        })
                        
                        DispatchQueue.main.async {
                            self.listTeamMemberTblView.isHidden = false
                            self.listTeamMemberTblView.reloadData()
                            self.selectedTeamMemberTblView.reloadData()
                            self.updateViewConstraints()
                            self.addJobBtn.setTitle("Edit Job", for: .normal)
                            self.invoiceTextField.text = self.scheduledJobDetails?.invoiceNo
                            self.selectedInvoiceId = self.scheduledJobDetails?.invoiceID
                            self.selectedStartDateString = self.scheduledJobDetails?.fromDate ?? ""
                            self.selectedEndDateString = self.scheduledJobDetails?.toDate ?? ""
                            self.postInspectorTextField.text = self.scheduledJobDetails?.postInspectorDetails.name
                            self.selectedPostInspectorId = self.scheduledJobDetails?.postInspectorDetails.id
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
