//
//  ManageScheduleListHeadertblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 27/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class ManageScheduleListHeadertblCell: UITableViewCell {
    
    @IBOutlet weak var manageScheduleListDateLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(jobDate: JobList) {
        manageScheduleListDateLbl.text = jobDate.date
    }
    
}
