//
//  SelectedTeamMemberTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 27/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

protocol SelectedTeamMemberTblCellDelegate: class {
    func didTapRemoveMemberBtn(selectedTeamMember: AssignedTeamMember, at indexpath: IndexPath)
}

class SelectedTeamMemberTblCell: UITableViewCell {
    
    @IBOutlet weak var selectedTeamMemberImage: UIImageView!
    @IBOutlet weak var selectedTeamMemberNameLbl: UILabel!
    
    weak var delegate: SelectedTeamMemberTblCellDelegate?
    var selectedTeamMember: AssignedTeamMember!
    var selectedIndexPath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(selectedTeamMember: AssignedTeamMember, delegate: SelectedTeamMemberTblCellDelegate, indexpath: IndexPath) {
        
        self.delegate = delegate
        self.selectedTeamMember = selectedTeamMember
        self.selectedIndexPath = indexpath
        
        guard let teamMemberImageUrl = URL(string: selectedTeamMember.picture ?? "Team-member") else {return}
               selectedTeamMemberImage.kf.indicatorType = .activity
               selectedTeamMemberImage.kf.setImage(with: teamMemberImageUrl, placeholder: UIImage(named: "Team-member"))
        
        selectedTeamMemberNameLbl.text = selectedTeamMember.name
    }
    
    @IBAction func removeTeamMemberBtnTapped(_ sender: UIButton) {
        delegate?.didTapRemoveMemberBtn(selectedTeamMember: selectedTeamMember, at: selectedIndexPath)
    }
    
}
