//
//  ListTeamMemberTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 27/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

class ListTeamMemberTblCell: UITableViewCell {
    
    @IBOutlet weak var teamMemberImage: UIImageView!
    @IBOutlet weak var teamMemberNameLbl: UILabel!
    @IBOutlet weak var teamMemberSelectedStatusImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(teamMemberDetail: AssignedTeamMember) {
        
        guard let teamMemberImageUrl = URL(string: teamMemberDetail.picture ?? "Team-member") else {return}
        teamMemberImage.kf.indicatorType = .activity
        teamMemberImage.kf.setImage(with: teamMemberImageUrl, placeholder: UIImage(named: "Team-member"))
        
        teamMemberNameLbl.text = teamMemberDetail.name
        
        if teamMemberDetail.isSelected {
            teamMemberSelectedStatusImage.image = UIImage(named: "ic_checkboxselected")
        }
        else {
            teamMemberSelectedStatusImage.image = UIImage(named: "ic_checkbox")
        }
    }
    
}
