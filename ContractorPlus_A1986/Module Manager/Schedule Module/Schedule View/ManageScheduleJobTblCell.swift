//
//  ManageScheduleJobTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 27/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

protocol ManageScheduleJobTblCellDelegate: class {
    func didTapDeleteScheduleJobBtn(jobDetails: Job, at indexpath: IndexPath)
    func didTapEditScheduleJobBtn(jobDetails: Job, at indexpath: IndexPath)
    func didTapViewScheduleJobBtn(jobDetails: Job, at indexpath: IndexPath)
}

class ManageScheduleJobTblCell: UITableViewCell {
    
    @IBOutlet weak var bottomOptionsView: UIView!
    @IBOutlet weak var postInspectorNameLbl: UILabel!
    @IBOutlet weak var invoiceIdLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var assignedTeamMembersLbl: UILabel!
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var endDateLbl: UILabel!
    
    weak var delegate: ManageScheduleJobTblCellDelegate?
    var selectedJobDetails: Job!
    var selectedIndexpath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func configureCell(jobDetails: Job, delegate: ManageScheduleJobTblCellDelegate, indexpath: IndexPath) {
        self.delegate = delegate
        self.selectedJobDetails = jobDetails
        self.selectedIndexpath = indexpath
        
        postInspectorNameLbl.text = jobDetails.postInspector
        invoiceIdLbl.text = jobDetails.invoiceNo
        addressLbl.text = jobDetails.address
        startDateLbl.text = jobDetails.startDate
        endDateLbl.text = jobDetails.endDate
        
        let teamMembersNamesArr = jobDetails.assignedTeamMember.map({$0.name})
        assignedTeamMembersLbl.text = teamMembersNamesArr.joined(separator: ",")
        
        
    }
    
    @IBAction func deleteScheduleJobBtnTapped(_ sender: UIButton) {
        delegate?.didTapDeleteScheduleJobBtn(jobDetails: selectedJobDetails, at: selectedIndexpath)
    }
    
    @IBAction func editScheduleTaskBtnTapped(_ sender: UIButton) {
        delegate?.didTapEditScheduleJobBtn(jobDetails: selectedJobDetails, at: selectedIndexpath)
    }
    
    @IBAction func viewScheduleTaskBtnTapped(_ sender: UIButton) {
        delegate?.didTapViewScheduleJobBtn(jobDetails: selectedJobDetails, at: selectedIndexpath)
    }
    
}
