//
//  ListSpecialityItems.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 09/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListSpecialityItems
struct ListSpecialityItems: Codable {
    let success, message: String
    let data: ListSpecialityItemsData
}

// MARK: - DataClass
struct ListSpecialityItemsData: Codable {
    let items: [SpecialityItemDetail]
}
