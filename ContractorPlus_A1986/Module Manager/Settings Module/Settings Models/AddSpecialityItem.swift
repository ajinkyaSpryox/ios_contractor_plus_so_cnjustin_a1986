//
//  AddSpecialityItem.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 06/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - AddSpecialityItem
struct AddSpecialityItem: Codable {
    let success, message: String
    let data: AddSpecialityItemData
}

// MARK: - DataClass
struct AddSpecialityItemData: Codable {
    let item: SpecialityItemDetail
}

// MARK: - Item
struct SpecialityItemDetail: Codable {
    let id: Int
    let productName, supplierName: String
    let retailPrice: Double
    let productImage: String

    enum CodingKeys: String, CodingKey {
        case id
        case productName = "product_name"
        case retailPrice = "retail_price"
        case supplierName = "supplier_name"
        case productImage = "product_image"
    }
}
