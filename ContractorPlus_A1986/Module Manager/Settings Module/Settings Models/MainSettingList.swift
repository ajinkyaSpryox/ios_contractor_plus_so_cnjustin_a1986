//
//  MainSettingList.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 14/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

struct MainSettingList {
    let displayIcon: String
    let title: String
    
}

var mainSettingListArr = [
    
    MainSettingList(displayIcon: "Estimates", title: "Estimates"),
    MainSettingList(displayIcon: "Invoices", title: "Invoices"),
    MainSettingList(displayIcon: "Payments", title: "Payments"),
    MainSettingList(displayIcon: "Branding", title: "Branding"),
    MainSettingList(displayIcon: "Quickbooks", title: "Quickbooks"),
    MainSettingList(displayIcon: "Supplies", title: "Supplies"),
    MainSettingList(displayIcon: "My-Account", title: "My Account"),
    MainSettingList(displayIcon: "Time-Clock", title: "Time Clock"),
    MainSettingList(displayIcon: "Mileage-Log", title: "Mileage Log"),
    MainSettingList(displayIcon: "About", title: "Shopping Settings"),
    MainSettingList(displayIcon: "help", title: "Help"),
    MainSettingList(displayIcon: "About", title: "About")
   
    
]
