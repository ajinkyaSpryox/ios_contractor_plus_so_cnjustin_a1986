//
//  SupplyStoresList.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 06/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - SupplyStoresList
struct SupplyStoresList: Codable {
    let success, message: String
    let data: [SupplyStoresListData]
}

// MARK: - SupplyStoresListData
class SupplyStoresListData: Codable {
    let id: Int
    let name: String
    let image: String
    var isChecked: String

    enum CodingKeys: String, CodingKey {
        case id, name, image
        case isChecked = "is_checked"
    }
}
