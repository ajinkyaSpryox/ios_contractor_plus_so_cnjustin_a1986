//
//  MileageSettingsVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 05/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class MileageSettingsVC: UIViewController {
    
    @IBOutlet weak var trackRoutesSwitch: UISwitch!
    @IBOutlet weak var trackDriveSpeedSwitch: UISwitch!
    @IBOutlet weak var trackDrivingTimeSwitch: UISwitch!
    
    //Variables
    let userDefaults = UserDefaults.standard
    var trackRoutes: Bool!
    var trackDrivingTime: Bool!
    var trackDriveSpeed: Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "MILEAGE LOG SETTINGS"
        addSaveButtonToNavigationBar()
        
        trackRoutes = userDefaults.bool(forKey: MileageSettingsSavedDefaults.trackRoutes)
        trackDriveSpeed = userDefaults.bool(forKey: MileageSettingsSavedDefaults.trackDrivingSpeed)
        trackDrivingTime = userDefaults.bool(forKey: MileageSettingsSavedDefaults.trackDrivingTime)
        
        trackRoutesSwitch.isOn = trackRoutes
        trackDriveSpeedSwitch.isOn = trackDriveSpeed
        trackDrivingTimeSwitch.isOn = trackDrivingTime
        
    }
    
    private func addSaveButtonToNavigationBar() {
        let saveNavigationBarButton = UIButton(type: .custom)
        saveNavigationBarButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        saveNavigationBarButton.addTarget(self, action: #selector(saveBtnTapped(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: saveNavigationBarButton)
        self.navigationItem.rightBarButtonItems = [item1]
    }

}

//MARK: IB-ACTIONS
extension MileageSettingsVC {
    
    @IBAction func trackRouteSwitchValueChanged(_ sender: UISwitch) {
        trackRoutes = sender.isOn ? true : false
    }
    
    @IBAction func trackDrivingTimeSwitchValueChanged(_ sender: UISwitch) {
        trackDrivingTime = sender.isOn ? true : false
    }
    
    @IBAction func trackDrivingSpeedSwitchValueChanged(_ sender: UISwitch) {
        trackDriveSpeed = sender.isOn ? true : false
    }
    
    @objc
    func saveBtnTapped(_ sender: UIBarButtonItem) {
        addUpdateMileageLogSettings()
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension MileageSettingsVC {
    
    func addUpdateMileageLogSettings() {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String: Any] = ["track_routes":  trackRoutes, "track_drive_time": trackDrivingTime, "track_driving_speed": trackDriveSpeed]
        
        view.makeToastActivity(.center)
        
        GenericWebservice.instance.getServiceData(url: Webservices.add_update_mileage_log_settings, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) {[weak self] (milageSetting: DeleteEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = milageSetting {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        
                        UserDefaults.standard.set(self.trackRoutes, forKey: MileageSettingsSavedDefaults.trackRoutes)
                        UserDefaults.standard.set(self.trackDriveSpeed, forKey: MileageSettingsSavedDefaults.trackDrivingSpeed)
                        UserDefaults.standard.set(self.trackDrivingTime, forKey: MileageSettingsSavedDefaults.trackDrivingTime)
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
