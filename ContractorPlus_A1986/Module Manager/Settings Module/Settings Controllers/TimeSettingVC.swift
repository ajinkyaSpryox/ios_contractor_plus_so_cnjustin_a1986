//
//  TimeSettingVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 09/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class TimeSettingVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var distanceTextField: UITextField!
    @IBOutlet weak var acceptDeclineSwitch: UISwitch!
    
    //Variables
    let userDefaults = UserDefaults.standard
    var requireEmployeeClockin: Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "TIME CLOCK SETTINGS"
        distanceTextField.delegate = self
        distanceTextField.underlined()
        
        requireEmployeeClockin = userDefaults.bool(forKey: TimeClockSettingsSavedDefaults.requiredEmployeeClockInOut)
        acceptDeclineSwitch.isOn = requireEmployeeClockin
        distanceTextField.text = "\(userDefaults.integer(forKey: TimeClockSettingsSavedDefaults.acceptableDistance))"
    }

}

//MARK: IB-ACTIONS
extension TimeSettingVC {
    
    @IBAction func acceptDeclineSwitchValueChanged(_ sender: UISwitch) {
        requireEmployeeClockin = sender.isOn
    }
    
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        
        guard let distanceValue = distanceTextField.text else {return}
        
        if distanceValue != "" {
            addUpdateTimeClockSettings(acceptableDistance: distanceValue, employeeClockIn: requireEmployeeClockin)
        }
        else {
            view.makeToast("Distance value is required", duration: 2.0, position: .bottom)
        }
        
    }
    
}

//MARK: TEXT FIELD DELEGATE IMPLEMENTATION
extension TimeSettingVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true
    }
}

//MARK: API SERVICE IMPLEMENTATION
extension TimeSettingVC {
    
    func addUpdateTimeClockSettings(acceptableDistance: String, employeeClockIn: Bool) {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["acceptable_distance":acceptableDistance, "require_employee_clockin_clockout":employeeClockIn]
        
        view.makeToastActivity(.center)
        
        GenericWebservice.instance.getServiceData(url: Webservices.add_update_timeclock_settings, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (timeSetting: DeleteEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                self.view.hideToastActivity()
                self.view.makeToast(error, duration: 2.0, position: .center)
            }
            else {
                
                if let returnedResponse = timeSetting {
                    
                    if returnedResponse.success == "true" {
                        
                        UserDefaults.standard.set(employeeClockIn, forKey: TimeClockSettingsSavedDefaults.requiredEmployeeClockInOut)
                        
                        UserDefaults.standard.set(acceptableDistance, forKey: TimeClockSettingsSavedDefaults.acceptableDistance)
                    }
                    
                    self.view.hideToastActivity()
                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                }
                
            }
            
        }
        
    }
    
}
