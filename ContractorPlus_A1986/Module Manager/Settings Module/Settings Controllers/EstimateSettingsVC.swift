//
//  EstimateSettingsVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 05/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class EstimateSettingsVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var laborRateTextField: UITextField!
    @IBOutlet weak var laborTotalPerTaskSwitch: UISwitch!
    @IBOutlet weak var timePerTaskSwitch: UISwitch!
    @IBOutlet weak var supplyTextField: UITextField!
    @IBOutlet weak var supplyMarkupSegmentController: UISegmentedControl!
    @IBOutlet weak var supplyUnitLbl: UILabel!
    @IBOutlet weak var individualSupplyPriceSwitch: UISwitch!
    @IBOutlet weak var supplyTotalPerTaskSwitch: UISwitch!
    @IBOutlet weak var grandTotalPerTaskSwitch: UISwitch!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    
    //Variables
    var selectedSupplySegmentUnit = "percent"
    var laborRate = 0
    var supplyMarkupRate = 0
    var clientApprovalRadioValue = 0
    
    let userDefaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "ESTIMATE SETTINGS"
        laborRateTextField.underlined()
        supplyTextField.underlined()
        
        supplyMarkupSegmentController.selectedSegmentIndex = 0
        supplyUnitLbl.text = "%"
        
        setupInitialRadioButtons()
        
        addSaveButtonToNavigationBar()
        
        setInitialSettingsFromUserDefault()
        
    }
    
    private func setInitialSettingsFromUserDefault() {
        laborRateTextField.text = userDefaults.string(forKey: EstimateSettingsSavedDefaults.defaultLabourRate)
        supplyTextField.text = userDefaults.string(forKey: EstimateSettingsSavedDefaults.supplyMarkupRate)
        
        if let supplyMarkupUnit = userDefaults.string(forKey: EstimateSettingsSavedDefaults.supplyMarkupUnit) {
            selectedSupplySegmentUnit = supplyMarkupUnit
            supplyMarkupSegmentController.selectedSegmentIndex = selectedSupplySegmentUnit == "percent" ? 0 : 1
            supplyUnitLbl.text = supplyMarkupSegmentController.selectedSegmentIndex == 0 ? "%" : "$"
        }
        
    }
    
    private func setupInitialRadioButtons() {
        noBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        yesBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        clientApprovalRadioValue = noBtn.tag
    }
    
    private func addSaveButtonToNavigationBar() {
        let saveNavigationBarButton = UIButton(type: .custom)
        saveNavigationBarButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        saveNavigationBarButton.addTarget(self, action: #selector(saveBtnTapped(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: saveNavigationBarButton)
        self.navigationItem.rightBarButtonItems = [item1]
    }

}

//MARK: IB-ACTIONS
extension EstimateSettingsVC {
    
    @IBAction func radioBtnsTapped(_ sender: UIButton) {
        //Yes = 1 & No = 2
        if sender.tag == 1 {
            yesBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            noBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
        else {
            noBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            yesBtn.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
        
        clientApprovalRadioValue = sender.tag
        
    }
    
    @IBAction func customizeEmailBtnTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func laborTotalPerTaskSwitchValueChanged(_ sender: UISwitch) {
       
    }
    
    @IBAction func timePerTaskSwitchValueChanged(_ sender: UISwitch) {
        
    }
    
    @IBAction func individualSupplyPriceSwitchValueChanged(_ sender: UISwitch) {
        
    }
    
    @IBAction func supplyTotalPerTaskSwitchValueChanged(_ sender: UISwitch) {
        
    }
    
    @IBAction func grandTotalPerTaskSwitchValueChanged(_ sender: UISwitch) {
        
    }
    
    @IBAction func supplyMarkupSegmentValueChanged(_ sender: UISegmentedControl) {
        selectedSupplySegmentUnit =  sender.selectedSegmentIndex == 0 ? "percent" : "value"
        supplyUnitLbl.text = sender.selectedSegmentIndex == 0 ? "%" : "$"
    }
    
    @objc
    func saveBtnTapped(_ sender: UIBarButtonItem) {
        
        guard let laborTotal = laborRateTextField.text, let supplyMarkupRate = supplyTextField.text else {return}
        
        if laborTotal != "" && supplyMarkupRate != "" {
            self.laborRate = Int(laborTotal) ?? 0
            self.supplyMarkupRate = Int(supplyMarkupRate) ?? 0
            addUpdateEstimateSettings()
            
        }
        else {
            view.makeToast("Supply and Labor Rates are required.", duration: 2.0, position: .bottom)
        }
        
    }
    
    
}

//MARK: API SERVICES IMPLEMENTATION
extension EstimateSettingsVC {
    
    func addUpdateEstimateSettings() {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String: Any] = ["default_labour_rate":  laborRate, "supply_markup": supplyMarkupRate, "supply_markup_unit": selectedSupplySegmentUnit]
        
        view.makeToastActivity(.center)
        
        GenericWebservice.instance.getServiceData(url: Webservices.add_update_estimate_settings, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (estimateSettings: DeleteEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = estimateSettings {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        
                        UserDefaults.standard.set(self.laborRate, forKey: EstimateSettingsSavedDefaults.defaultLabourRate)
                        UserDefaults.standard.set(self.supplyMarkupRate, forKey: EstimateSettingsSavedDefaults.supplyMarkupRate)
                        UserDefaults.standard.set(self.selectedSupplySegmentUnit, forKey: EstimateSettingsSavedDefaults.supplyMarkupUnit)
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
