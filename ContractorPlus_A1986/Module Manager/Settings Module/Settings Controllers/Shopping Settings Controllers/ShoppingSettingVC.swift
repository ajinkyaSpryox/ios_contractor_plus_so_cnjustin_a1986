//
//  ShoppingSettingVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 06/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ShoppingSettingVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var supplyStoreTableView: UITableView!
    @IBOutlet weak var supplyStoreTableHeight: NSLayoutConstraint!
    
    //Variables
    var supplyStoresListArr = [SupplyStoresListData]()
    var selectedSupplyStoresIds = [Int]()

    override func viewDidLoad() {
        super.viewDidLoad()
        initialUiSetup()
    }
    
    private func initialUiSetup() {
        title = "SHOPPING SETTINGS"
        supplyStoreTableView.register(UINib(nibName: "ShoppingSupplyStoresListTblCell", bundle: nil), forCellReuseIdentifier: "ShoppingSupplyStoresListTblCell")
        supplyStoreTableView.delegate = self
        supplyStoreTableView.dataSource = self
        serviceCalls()
    }
    
    private func serviceCalls() {
        getSupplyStoresList()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    override func updateViewConstraints() {
        supplyStoreTableHeight.constant = supplyStoreTableView.contentSize.height
        super.updateViewConstraints()
    }

}

//MARK: IB-ACTIONS
extension ShoppingSettingVC {
    
    @IBAction func updateBtnTapped(_ sender: UIButton) {
        addUpdateShoppingSetting()
    }
    
    @IBAction func addItemBtnTapped(_ sender: UIButton) {
        guard let addItemVc = storyboard?.instantiateViewController(withIdentifier: "AddSpecialityItemVC") as? AddSpecialityItemVC else {return}
        navigationController?.pushViewController(addItemVc, animated: true)
    }
    
    @IBAction func itemsListBtnTapped(_ sender: UIButton) {
        guard let itemsListVc = storyboard?.instantiateViewController(withIdentifier: "ListSpecialityItemsVC") as? ListSpecialityItemsVC else {return}
        navigationController?.pushViewController(itemsListVc, animated: true)
    }
    
}

//MARK: TABLE VIEW DELEGATE AND DATA SOURCE IMPLEMENTATION
extension ShoppingSettingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return supplyStoresListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingSupplyStoresListTblCell") as? ShoppingSupplyStoresListTblCell else {return UITableViewCell()}
        cell.congigureCell(supplyStoreData: supplyStoresListArr[indexPath.row], delegate: self, indexpath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

//MARK: SHOPPING SUPPLY STORES TABLE CELL DELEGATE IMPLEMENTATION
extension ShoppingSettingVC: ShoppingSupplyStoresListTblCellDelegate {
    
    func didTapCheckMarkBtn(supplyStoreData: SupplyStoresListData, at indexpath: IndexPath) {
        
        if supplyStoreData.isChecked == "true" {
            supplyStoreData.isChecked = "false"
            if let selectedIndex = selectedSupplyStoresIds.lastIndex(of: supplyStoreData.id) {
                selectedSupplyStoresIds.remove(at: selectedIndex)
            }
        }
        else {
            supplyStoreData.isChecked = "true"
            selectedSupplyStoresIds.append(supplyStoreData.id)
        }
        
        supplyStoreTableView.reloadData()
        
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension ShoppingSettingVC {
    
    //Populate Stores List
    func getSupplyStoresList() {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = [String:Any]()
        
        view.makeToastActivity(.center)
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_supply_stores, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (supplyStoresList: SupplyStoresList!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = supplyStoresList {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        self.supplyStoresListArr = returnedResponse.data
                        //print(self.supplyStoresListArr)
                        self.supplyStoresListArr.forEach { (store) in
                            if store.isChecked == "true" {
                                self.selectedSupplyStoresIds.append(store.id)
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.supplyStoreTableView.reloadData()
                            self.updateViewConstraints()
                            self.supplyStoreTableView.isHidden = self.supplyStoresListArr.isEmpty ? true : false
                            
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func addUpdateShoppingSetting() {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["where_i_shop_ids" : selectedSupplyStoresIds]
        
        view.makeToastActivity(.center)
        
        GenericWebservice.instance.getServiceData(url: Webservices.add_shopping_settings, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (shoppingSettings: DeleteEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(shoppingSettings.message, duration: 2.0, position: .bottom)
                }
            }
            
        }
        
    }
    
}

