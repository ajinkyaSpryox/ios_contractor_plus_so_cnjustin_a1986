//
//  AddSpecialityItemVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 06/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class AddSpecialityItemVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameTextField: UITextField!
    @IBOutlet weak var productRetailPricetextField: UITextField!
    @IBOutlet weak var supplerNameTextField: UITextField!
    
    //Variables
    var selectedImage: UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "Add Item"
        productNameTextField.delegate = self
        productRetailPricetextField.delegate = self
        supplerNameTextField.delegate = self
    }

}

//MARK: UI TEXT FIELD DELEGATE IMPLEMENTATION
extension AddSpecialityItemVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        view.endEditing(true)
        return true
    }
    
}

//MARK: IB-ACTIONS
extension AddSpecialityItemVC {
    
    @IBAction func uploadBtnTapped(_ sender: UIButton) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }
            else {
                self.view.makeToast("Camera Not Available", duration: 2.0, position: .bottom)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    @IBAction func addItemBtnTapped(_ sender: UIButton) {
        
        guard let productName = productNameTextField.text, let retailPrice = productRetailPricetextField.text, let supplierName = supplerNameTextField.text else {return}
        
        if productName != "" && retailPrice != "" {
            
            if selectedImage != nil {
               addEditSpecialityItem(image: selectedImage, productName: productName, retailPrice: retailPrice, supplierName: supplierName)
            }
            else {
                view.makeToast("Product image is required.", duration: 2.0, position: .bottom)
            }
            
        }
        else {
            view.makeToast("Product Name & Retail Price is required.", duration: 2.0, position: .bottom)
        }
        
        
    }
    
}

//MARK: IMAGE PICKER CONTROLLER DELEGATE IMPLEMENTATION
extension AddSpecialityItemVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        productImageView.image = image
        
        selectedImage = image
        
        //upload(image: image)
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension AddSpecialityItemVC {
    
    //TODO: UPLOAD SELECTED AREA IMAGE TO SERVER
    func addEditSpecialityItem(image: UIImage, productName: String, retailPrice: String, supplierName: String) {
        
        let headers = [
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        guard let imageData = image.jpegData(compressionQuality: 0.5) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        
        
        let parameters: Parameters = [
            
            "product_image" : "file.jpeg",
            "product_name" : productName,
            "retail_price" : retailPrice,
            "supplier_name" : supplierName
        ]
        
        print(parameters)
        
        Alamofire.upload(multipartFormData: {multipartData in
            for(key,value) in parameters {
                multipartData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            
            multipartData.append(imageData, withName: "product_image", fileName: "file.jpeg", mimeType: "image/jpeg")
        },
                         //usingThreshold: UInt64.init(),
            to: Webservices.add_edit_speciality_item,
            method: .post,
            headers: headers,
            encodingCompletion: {result in
                
                switch result {
                case .success(let upload, _, _):
                    
                    self.view.makeToastActivity(.center)
                    
                    upload.uploadProgress(closure: { (progress) in
                        //print("Upload Progress: \(progress.fractionCompleted)")
                        //let progressPercent = Int(progress.fractionCompleted * 100)
                        //print(progressPercent)
                    })
                    
                    upload.responseJSON { response in
                        //print(response.result.value)
                        
                        guard let data = response.data else {return}
                        let decoder = JSONDecoder()
                        
                        do{
                            let returnedResponse = try decoder.decode(AddSpecialityItem.self, from: data)
                            
                            if returnedResponse.success == "true" {
                                
                                DispatchQueue.main.async {
                                    //self.beforePhotosCollectionView.reloadData()
                                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                                    self.view.hideToastActivity()
                                }
                                
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.view.hideToastActivity()
                                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                                }
                            }
                            //print(self.areaImageUploadedImagesArr)
                        }
                        catch let jsonError {
                            //print(jsonError.localizedDescription)
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.view.makeToast(jsonError.localizedDescription, duration: 2.0, position: .bottom)
                            }
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    self.view.hideToastActivity()
                    self.view.makeToast(encodingError.localizedDescription, duration: 2.0, position: .bottom)
                }
                
        })
        
    }
    
    
}
