//
//  ListSpecialityItemsVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 06/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ListSpecialityItemsVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var listSpecialityItemsTableView: UITableView!
    
    //Variables
    var specialityItemsListArr = [SpecialityItemDetail]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "ITEMS LIST"
        listSpecialityItemsTableView.register(UINib(nibName: "ListSpecialityItemsTblCell", bundle: nil), forCellReuseIdentifier: "ListSpecialityItemsTblCell")
        listSpecialityItemsTableView.delegate = self
        listSpecialityItemsTableView.dataSource = self
        listSpecialityItems()
    }

}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListSpecialityItemsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return specialityItemsListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListSpecialityItemsTblCell") as? ListSpecialityItemsTblCell else {return UITableViewCell()}
        cell.configureCell(itemDetails: specialityItemsListArr[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension ListSpecialityItemsVC {
    
    func listSpecialityItems() {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = [String:Any]()
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_speciality_items, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (specialityItems: ListSpecialityItems!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = specialityItems {
                    
                    if returnedResponse.success == "true" {
                        self.specialityItemsListArr = returnedResponse.data.items
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.listSpecialityItemsTableView.reloadData()
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
