//
//  MainSettingsListVC.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 14/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Toast_Swift

class MainSettingsListVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var settingsTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        self.title = "SETTINGS"
        
        settingsTableView.delegate = self
        settingsTableView.dataSource = self
        settingsTableView.register(UINib(nibName: "MainSettingTblCell", bundle: nil), forCellReuseIdentifier: "MainSettingTblCell")
    }

}

extension MainSettingsListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainSettingListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainSettingTblCell") as? MainSettingTblCell else {return UITableViewCell()}
        cell.configureCell(settingsData: mainSettingListArr[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        switch indexPath.row {
        case 0:
            //Go to estimate settings
            guard let estimateSettingsVc = storyboard?.instantiateViewController(withIdentifier: "EstimateSettingsVC") as? EstimateSettingsVC else {return}
            navigationController?.pushViewController(estimateSettingsVc, animated: true)
            
        case 3:
            //Go to branding settings
            guard let brandingSettingsVc = storyboard?.instantiateViewController(withIdentifier: "BrandingSettingsVC") as? BrandingSettingsVC else {return}
            navigationController?.pushViewController(brandingSettingsVc, animated: true)
            
        case 8:
            //Go to mileage settings
            guard let mileageSettingsVc = storyboard?.instantiateViewController(withIdentifier: "MileageSettingsVC") as? MileageSettingsVC else {return}
            navigationController?.pushViewController(mileageSettingsVc, animated: true)
            
        case 7:
            //Go to time clock settings
            guard let timeClockSettingVc = storyboard?.instantiateViewController(withIdentifier: "TimeSettingVC") as? TimeSettingVC else {return}
            navigationController?.pushViewController(timeClockSettingVc, animated: true)
            
        case 9:
            //Go to shopping settings
            guard let shoppingSettingsVc = storyboard?.instantiateViewController(withIdentifier: "ShoppingSettingVC") as? ShoppingSettingVC else {return}
            navigationController?.pushViewController(shoppingSettingsVc, animated: true)
            
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
}
