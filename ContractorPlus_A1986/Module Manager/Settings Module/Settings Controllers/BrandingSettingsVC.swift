//
//  BrandingSettingsVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 05/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class BrandingSettingsVC: UIViewController {
    
    // Radio Buttons Outlets
    @IBOutlet weak var nothingToDisplayRadioBtn: UIButton!
    @IBOutlet weak var bothToDisplayRadioBtn: UIButton!
    @IBOutlet weak var logoToDisplayRadioBtn: UIButton!
    @IBOutlet weak var businessInfoToDisplayRadioBtn: UIButton!
    
    //Radio Button Selection
    var isNothingToDisplayRadioBtnSelected = false
    var isBothToDisplayRadioBtnSelected = false
    var isLogoToDisplayRadioBtnSelected = false
    var isBusinessToDisplayRadioBtnSelected = false
    
    //Powered By Radio Button Selection Variables
    var isShowCoBrandRadioBtnSelected = false
    var isRemoveCoBrandRadioBtnSelected = false
    
    //CheckBoxes Outlets
    @IBOutlet weak var estimateCheckBox: UIButton!
    @IBOutlet weak var invoicesCheckBox: UIButton!
    @IBOutlet weak var postInspectionCheckBox: UIButton!
    @IBOutlet weak var emailSignatureCheckBox: UIButton!
    
    //Check Box Selection Variables
    var isEstimatedCheckBoxSelected = false
    var isInvoiceCheckBoxSelected = false
    var isPostInspectionCheckBoxSelected = false
    var isEmailSignatureCheckBoxSelected = false
    
    //Logo Image Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    
    //PickerViews
    var pickerView = UIPickerView()
    var countriesListArr = [ListCountriesData]()
    var statesListArr = [ListStatesData]()
    var parameters: Parameters = [String:Any]()
    
    //Final Values to pass
    var whatToDisplayRadioValue = "both"
    var whereToDisplayArr = [String]()
    var coBrandRadioValue = true
    
    //TextField Outlets
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipTextFiled: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    
    //Radio Buttons for Remove Powered
    @IBOutlet weak var showCoBrandRadioBtn: UIButton!
    @IBOutlet weak var removeCoBrandRadioBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "BRANDING SETTINGS"
        addSaveButtonToNavigationBar()
        bothToDisplayRadioBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        showCoBrandRadioBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        underlineTextField()
        countryTextField.delegate = self
        stateTextField.delegate = self
        serviceCalls()
    }
    
    private func serviceCalls() {
        getCountriesList()
    }
    
    private func addSaveButtonToNavigationBar() {
        let saveNavigationBarButton = UIButton(type: .custom)
        saveNavigationBarButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        saveNavigationBarButton.addTarget(self, action: #selector(saveBtnTapped(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: saveNavigationBarButton)
        self.navigationItem.rightBarButtonItems = [item1]
    }
    
    private func underlineTextField() {
        companyNameTextField.underlined()
        emailTextField.underlined()
        phoneTextField.underlined()
        countryTextField.underlined()
        stateTextField.underlined()
        cityTextField.underlined()
        zipTextFiled.underlined()
        addressTextField.underlined()
    }

}

//MARK: IB-Actions
extension BrandingSettingsVC {
    
    //Radio Button Actions
    @IBAction func radioBtnTapped(sender: UIButton) {
        //Tags: 1 - Nothing, 2- Both, 3- Logo, 4- Business
        
        switch sender.tag {
        case 1:
            
            isNothingToDisplayRadioBtnSelected = true
            isBothToDisplayRadioBtnSelected = false
            isLogoToDisplayRadioBtnSelected = false
            isBusinessToDisplayRadioBtnSelected = false
            
            nothingToDisplayRadioBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            bothToDisplayRadioBtn.backgroundColor = .systemGray
            logoToDisplayRadioBtn.backgroundColor = .systemGray
            businessInfoToDisplayRadioBtn.backgroundColor = .systemGray
            
        case 2:
            
            isBothToDisplayRadioBtnSelected = true
            isNothingToDisplayRadioBtnSelected = false
            isLogoToDisplayRadioBtnSelected = false
            isBusinessToDisplayRadioBtnSelected = false
            
            bothToDisplayRadioBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            nothingToDisplayRadioBtn.backgroundColor = .systemGray
            logoToDisplayRadioBtn.backgroundColor = .systemGray
            businessInfoToDisplayRadioBtn.backgroundColor = .systemGray
            
        case 3:
            
            isLogoToDisplayRadioBtnSelected = true
            isBothToDisplayRadioBtnSelected = false
            isNothingToDisplayRadioBtnSelected = false
            isBusinessToDisplayRadioBtnSelected = false
            
            logoToDisplayRadioBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            bothToDisplayRadioBtn.backgroundColor = .systemGray
            nothingToDisplayRadioBtn.backgroundColor = .systemGray
            businessInfoToDisplayRadioBtn.backgroundColor = .systemGray
            
        case 4:
            
            isBusinessToDisplayRadioBtnSelected = true
            isLogoToDisplayRadioBtnSelected = false
            isBothToDisplayRadioBtnSelected = false
            isNothingToDisplayRadioBtnSelected = false
            
            businessInfoToDisplayRadioBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            logoToDisplayRadioBtn.backgroundColor = .systemGray
                bothToDisplayRadioBtn.backgroundColor = .systemGray
                    nothingToDisplayRadioBtn.backgroundColor = .systemGray
            
        default:
            return
        }
        
        switch sender.tag {
        case 1:
            whatToDisplayRadioValue = "nothing"
        case 2:
            whatToDisplayRadioValue = "both"
        case 3:
            whatToDisplayRadioValue = "logo"
        case 4:
            whatToDisplayRadioValue = "business_info"
        default:
            return
        }
        
    }
    
    //Powered by Radio Buttons Action
    @IBAction func poweredByRadioBtnsTapped(_ sender: UIButton) {
        //Tag 1 to show and 2 to Remove
        switch sender.tag {
        case 1:
            showCoBrandRadioBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            removeCoBrandRadioBtn.backgroundColor = .systemGray
            isShowCoBrandRadioBtnSelected = true
            isRemoveCoBrandRadioBtnSelected = false
            coBrandRadioValue = true
            
        case 2:
            showCoBrandRadioBtn.backgroundColor = .systemGray
            removeCoBrandRadioBtn.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            isShowCoBrandRadioBtnSelected = false
            isRemoveCoBrandRadioBtnSelected = true
            coBrandRadioValue = false
            
        default:
            return
        }
    }
    
    //Check Box Actions
    @IBAction func estimateCheckBoxTapped(_ sender: UIButton) {
        
        isEstimatedCheckBoxSelected = !isEstimatedCheckBoxSelected
        
        if isEstimatedCheckBoxSelected {
            estimateCheckBox.setImage(UIImage(named: "ic_checkboxselected"), for: .normal)
        }
        else {
            estimateCheckBox.setImage(UIImage(named: "ic_checkbox"), for: .normal)
        }
        
        if let index = whereToDisplayArr.lastIndex(of: "estimates") {
            whereToDisplayArr.remove(at: index)
        }
        else {
           whereToDisplayArr.append("estimates")
        }
        
    }
    
    @IBAction func invoiceCheckBoxTapped(_ sender: UIButton) {
        
        isInvoiceCheckBoxSelected = !isInvoiceCheckBoxSelected
        
        if isInvoiceCheckBoxSelected {
            invoicesCheckBox.setImage(UIImage(named: "ic_checkboxselected"), for: .normal)
        }
        else {
            invoicesCheckBox.setImage(UIImage(named: "ic_checkbox"), for: .normal)
        }
        
        if let index = whereToDisplayArr.lastIndex(of: "invoices") {
            whereToDisplayArr.remove(at: index)
        }
        else {
           whereToDisplayArr.append("invoices")
        }
        
    }
    
    @IBAction func postInspectionCheckBoxTapped(_ sender: UIButton) {
        
        isPostInspectionCheckBoxSelected = !isPostInspectionCheckBoxSelected
        
        if isPostInspectionCheckBoxSelected {
            postInspectionCheckBox.setImage(UIImage(named: "ic_checkboxselected"), for: .normal)
        }
        else {
            postInspectionCheckBox.setImage(UIImage(named: "ic_checkbox"), for: .normal)
        }
        
        if let index = whereToDisplayArr.lastIndex(of: "post_inspections") {
            whereToDisplayArr.remove(at: index)
        }
        else {
           whereToDisplayArr.append("post_inspections")
        }
        
    }
    
    @IBAction func emailSignatureCheckBoxTapped(_ sender: UIButton) {
        
        isEmailSignatureCheckBoxSelected = !isEmailSignatureCheckBoxSelected
        
        if isEmailSignatureCheckBoxSelected {
            emailSignatureCheckBox.setImage(UIImage(named: "ic_checkboxselected"), for: .normal)
        }
        else {
            emailSignatureCheckBox.setImage(UIImage(named: "ic_checkbox"), for: .normal)
        }
        
        if let index = whereToDisplayArr.lastIndex(of: "email_signature") {
            whereToDisplayArr.remove(at: index)
        }
        else {
           whereToDisplayArr.append("email_signature")
        }
        
    }
    
    @IBAction func uploadLogoBtnTapped(_ sender: UIButton) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }
            else {
                self.view.makeToast("Camera Not Available", duration: 2.0, position: .bottom)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    @objc
    func saveBtnTapped(_ sender: UIBarButtonItem) {
        
        if whereToDisplayArr.isEmpty {
            print("Please selecion where to display")
        }
        else {
            //All Ready to hit api
            addUpdateBrandingSettings(image: logoImageView.image!)
        }
        
    }
    
    @IBAction func emailSettingBtnTapped(_ sender: UIButton) {
        guard let emailSettingVC = storyboard?.instantiateViewController(withIdentifier: "EmailSettingsVC") as? EmailSettingsVC else {return}
        navigationController?.pushViewController(emailSettingVC, animated: true)
    }
    
}

extension BrandingSettingsVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //if text field is client then add picker view
        
        switch textField {
        case countryTextField:
            countriesListArr.count == 0 ? view.makeToast("No Countries Found", duration: 2.0, position: .center) : addPickerToCountryTextField()
        case stateTextField:
            statesListArr.count == 0 ? view.makeToast("No States Found", duration: 2.0, position: .center) : addPickerToStatesTextField()
        default:
            return
        }
        
    }
    
    
    func addPickerToCountryTextField() {
        pickerView.tag = 1
        countryTextField.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func addPickerToStatesTextField() {
        pickerView.tag = 2
        stateTextField.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
}

//MARK: PICKER VIEW IMPLEMENTATION
extension BrandingSettingsVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 1 {
            return countriesListArr.count
        }
        else {
            return statesListArr.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1 {
            return countriesListArr[row].name
        }
        else {
            return statesListArr[row].name
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            countryTextField.text = countriesListArr[row].name
            parameters["countries_id"] = countriesListArr[row].id
            getStatesListWith(countryId: countriesListArr[row].id) //Get States List With Selected Country Id Service
        }
        else {
            stateTextField.text = statesListArr[row].name
            parameters["state_id"] = statesListArr[row].id
        }
        
    }
    
}

//MARK: IMAGE PICKER CONTROLLER DELEGATE IMPLEMENTATION
extension BrandingSettingsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        logoImageView.image = image
        //upload(image: image, purposeString: "estimate")
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension BrandingSettingsVC {
    
    //Get Countries List
    func getCountriesList() {
        
        let header = ["Accept" : "application/json",
        "x-api-key": appXApiKey]
        
        let parameters: [String:Any] = [:]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_countries, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: header) { [weak self] (countriesList: ListCountries!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = countriesList {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        self.countriesListArr = returnedResponse.data
                        //print(self.countriesListArr)
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //Get States List
    func getStatesListWith(countryId: Int) {
        
        let header = ["Accept" : "application/json",
        "x-api-key": appXApiKey]
        
        let parameters: [String:Any] = ["country_id": countryId]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_states, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: header) { [weak self] (statesList: ListStates!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = statesList {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        self.statesListArr = returnedResponse.data
                        //print(self.statesListArr)
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //Add/Update Branding Settings
    //TODO: UPLOAD SELECTED AREA IMAGE TO SERVER
    func addUpdateBrandingSettings(image: UIImage) {
        
        let headers = [
            "Accept": "application/json",
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        guard let imageData = image.jpegData(compressionQuality: 0.5) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        
        whereToDisplayArr.enumerated().forEach { (index, value) in
          parameters["where_to_display\([index])"] = value
        }
        
        parameters["logo"] = "file.jpeg"
        parameters["what_to_display"] = whatToDisplayRadioValue
        parameters["company_name"] = companyNameTextField.text
        parameters["email"] = emailTextField.text
        parameters["phone"] = phoneTextField.text
        parameters["city"] = cityTextField.text
        parameters["zip_code"] = zipTextFiled.text
        parameters["address_1"] = addressTextField.text
        parameters["address_2"] = ""
        parameters["show_co_brand"] = isShowCoBrandRadioBtnSelected
        
        
        Alamofire.upload(multipartFormData: { [weak self] multipartData in
            
            guard let self = self else {return}
            
            for(key,value) in self.parameters {
                multipartData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            
            multipartData.append(imageData, withName: "logo", fileName: "file.jpeg", mimeType: "image/jpeg")
        },
                         //usingThreshold: UInt64.init(),
            to: Webservices.add_update_branding_setting,
            method: .post,
            headers: headers,
            encodingCompletion: {result in
                
                switch result {
                case .success(let upload, _, _):
                    
                    self.view.makeToastActivity(.center)
                    
                    upload.uploadProgress(closure: { (progress) in
                        //print("Upload Progress: \(progress.fractionCompleted)")
                        //let progressPercent = Int(progress.fractionCompleted * 100)
                        //print(progressPercent)
                    })
                    
                    upload.responseJSON { response in
                        //print(response.result.value)
                        
                        guard let data = response.data else {return}
                        let decoder = JSONDecoder()
                        
                        do{
                            let returnedResponse = try decoder.decode(DeleteEstimate.self, from: data)
                            
                            if returnedResponse.success == "true" {
                                print(returnedResponse.message)
                                DispatchQueue.main.async {
                                    self.view.hideToastActivity()
                                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                                }
                                
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.view.hideToastActivity()
                                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                                }
                            }
                            
                        }
                        catch let jsonError {
                            //print(jsonError.localizedDescription)
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.view.makeToast(jsonError.localizedDescription, duration: 2.0, position: .bottom)
                            }
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    self.view.hideToastActivity()
                    self.view.makeToast(encodingError.localizedDescription, duration: 2.0, position: .bottom)
                }
                
        })
        
    }
    
}
