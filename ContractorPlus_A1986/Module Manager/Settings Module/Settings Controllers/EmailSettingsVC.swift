//
//  EmailSettingsVC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 08/04/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class EmailSettingsVC: UIViewController {
    
    @IBOutlet weak var connectionSecurityTextField: UITextField!
    @IBOutlet weak var hostNameTextField: UITextField!
    @IBOutlet weak var portTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Email Settings"
        underlineTextField()
    }
    
    private func underlineTextField() {
        connectionSecurityTextField.underlined()
        hostNameTextField.underlined()
        portTextField.underlined()
        usernameTextField.underlined()
        passwordTextField.underlined()
    }

}
