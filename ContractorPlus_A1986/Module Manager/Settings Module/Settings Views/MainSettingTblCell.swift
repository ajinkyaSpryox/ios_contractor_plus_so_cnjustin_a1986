//
//  MainSettingTblCell.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 14/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class MainSettingTblCell: UITableViewCell {
    
    @IBOutlet weak var iconImageview: UIImageView!
    @IBOutlet weak var iconTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }
    
    func configureCell(settingsData: MainSettingList) {
        iconImageview.image = UIImage(named: settingsData.displayIcon)
        iconTitleLabel.text = settingsData.title
    }
    
}
