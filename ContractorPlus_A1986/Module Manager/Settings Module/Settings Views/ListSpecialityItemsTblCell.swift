//
//  ListSpecialityItemsTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 06/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

class ListSpecialityItemsTblCell: UITableViewCell {
    
    @IBOutlet weak var bottomOptionsView: UIView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemSupplierNameLbl: UILabel!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func configureCell(itemDetails: SpecialityItemDetail) {
        
        guard let itemImageUrl = URL(string: itemDetails.productImage) else {return}
        itemImageView.kf.indicatorType = .activity
        itemImageView.kf.setImage(with: itemImageUrl, placeholder: UIImage(named: "image1"))
        
        itemNameLbl.text = itemDetails.productName
        itemSupplierNameLbl.text = itemDetails.supplierName
        itemPriceLbl.text = "$ \(itemDetails.retailPrice)"
        
    }
    
}
