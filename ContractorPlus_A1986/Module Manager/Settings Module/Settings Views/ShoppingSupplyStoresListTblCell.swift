//
//  ShoppingSupplyStoresListTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 06/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

protocol ShoppingSupplyStoresListTblCellDelegate: class {
    func didTapCheckMarkBtn(supplyStoreData: SupplyStoresListData, at indexpath: IndexPath)
}

class ShoppingSupplyStoresListTblCell: UITableViewCell {
    
    @IBOutlet weak var checkMarkBtn: UIButton!
    @IBOutlet weak var supplyStoreNameLbl: UILabel!
    @IBOutlet weak var supplyStoreImage: UIImageView!
    
    weak var delegate: ShoppingSupplyStoresListTblCellDelegate?
    var selectedSupplyStore: SupplyStoresListData!
    var selectedIndexPath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func congigureCell(supplyStoreData: SupplyStoresListData, delegate: ShoppingSupplyStoresListTblCellDelegate, indexpath: IndexPath) {
        
        self.delegate = delegate
        self.selectedSupplyStore = supplyStoreData
        self.selectedIndexPath = indexpath
        
        supplyStoreNameLbl.text = supplyStoreData.name
        
        guard let supplyStoreImageUrl = URL(string: supplyStoreData.image) else {return}
        supplyStoreImage.kf.indicatorType = .activity
        supplyStoreImage.kf.setImage(with: supplyStoreImageUrl, placeholder: UIImage(named: "image1"))
        
        if supplyStoreData.isChecked == "true" {
            checkMarkBtn.setImage(UIImage(named: "ic_checkboxselected"), for: .normal)
        }
        else {
            checkMarkBtn.setImage(UIImage(named: "ic_checkbox"), for: .normal)
        }
        
    }
    
    @IBAction func checkMarkBtnTapped(_ sender: UIButton) {
        print("Check Mark Btn Tapped")
        delegate?.didTapCheckMarkBtn(supplyStoreData: self.selectedSupplyStore, at: self.selectedIndexPath)
    }
    
}
