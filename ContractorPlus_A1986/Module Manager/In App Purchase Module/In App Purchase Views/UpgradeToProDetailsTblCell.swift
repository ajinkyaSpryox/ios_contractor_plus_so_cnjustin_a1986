//
//  UpgradeToProDetailsTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 18/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

class UpgradeToProDetailsTblCell: UITableViewCell {
    
    @IBOutlet weak var detailIconImage: UIImageView!
    @IBOutlet weak var detailTitleLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(upgradeProDetails: UpgradeProDetails) {
        
        detailIconImage.image = UIImage(named: upgradeProDetails.detailsIcon)
        detailTitleLbl.text = upgradeProDetails.detailTitle
    }
    
}
