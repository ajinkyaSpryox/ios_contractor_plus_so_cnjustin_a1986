//
//  UpgradeToProVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 25/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class UpgradeToProVC: UIViewController {
    
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var upgradeProTableView: UITableView!
    @IBOutlet weak var upgradeProTableHeight: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        IAPServiceHelper.instance.delegate = self
        IAPServiceHelper.instance.loadProducts()
    }
    
    private func setupInitialUI() {
        title = "UPGRADE TO PRO"
        topImageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        topImageView.image = UIImage(named: "upgrade_to_pro_display_image")
        upgradeProTableView.delegate = self
        upgradeProTableView.dataSource = self
        upgradeProTableView.register(UINib(nibName: "UpgradeToProDetailsTblCell", bundle: nil), forCellReuseIdentifier: "UpgradeToProDetailsTblCell")
        updateViewConstraints()
        
    }
    
    override func updateViewConstraints() {
        upgradeProTableHeight.constant = upgradeProTableView.contentSize.height
        super.updateViewConstraints()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    
    
    
    @IBAction func getProService(_ sender: UIButton) {
    }

}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension UpgradeToProVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return upgradeProDetailsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UpgradeToProDetailsTblCell") as? UpgradeToProDetailsTblCell else {return UITableViewCell()}
        cell.configureCell(upgradeProDetails: upgradeProDetailsArr[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension UpgradeToProVC: IAPServiceHelperDelegate {
    
    func inAppPurchaseProductsLoaded() {
        print("IN App Delegate Products Loaded")
    }
    
}
