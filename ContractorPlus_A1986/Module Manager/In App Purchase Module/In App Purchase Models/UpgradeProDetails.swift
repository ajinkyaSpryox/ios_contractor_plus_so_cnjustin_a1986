//
//  UpgradeProDetails.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 18/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

struct UpgradeProDetails {
    let detailsIcon: String
    let detailTitle: String
}

let upgradeProDetailsArr = [
     UpgradeProDetails(detailsIcon: "collabrate", detailTitle: "Collabrate with your entire team. Work smarted. Not harder."),
     UpgradeProDetails(detailsIcon: "roles", detailTitle: "Assign roles and permissions to individual team members."),
     UpgradeProDetails(detailsIcon: "controls", detailTitle: "Gain more controller over features and settings."),
     UpgradeProDetails(detailsIcon: "send more", detailTitle: "Send estimate, invoices & post inspections from your own email address/domain."),
     UpgradeProDetails(detailsIcon: "automate", detailTitle: "Automate your accounting with Quickbooks Sync."),
     UpgradeProDetails(detailsIcon: "powered_by", detailTitle: "Remove \"Powered by Contractor Plus\" branding."),
     UpgradeProDetails(detailsIcon: "featured_listing", detailTitle: "Get a featured listing in our contractor directory for more leads."),
     
    ]
