//
//  OTPViewController.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 11/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

@available(iOS 12.0, *)
class OTPViewController: UIViewController {

      @IBOutlet weak var otpNoTF: UITextField!
      
      @IBOutlet weak var sendOTPBtn: UIButton!
      
      
      @IBAction func resendBtnAction(_ sender: UIButton) {
        
        api_Resend_Function()
        
    }
    
    
     
    @IBAction func otpBtnAction(_ sender: UIButton) {
       
        if otpNoTF.text != ""{
            apiFunction()
        }else{
            self.errorAlert(message: textfieldEmptyError)
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        sendOTPBtn.cornerRadius1(value: 12.0, shadowColor: UIColor.red.cgColor, borderColor: UIColor.clear.cgColor)
        otpNoTF.addBottomBorderWithColor(color: UIColor.red, width: 3)
       
        
        // Do any additional setup after loading the view.
    }
    func apiFunction(){
            
            let header = ["Accept" : "application/json",
                          "x-api-key": appXApiKey]
            
            let parameter : [String : Any] = [
                           
    //                       "email" : emailTF.text!, //"akashp.spryox@gmail.com",//Constant.phone,  //UserDefaults.standard.value(forKey: "user_id")!
    //                       "password" : passwordTF.text! //  "12345678" //Constant.UUID! as Any
                
                "user_id" :  "\(UserDefaults.standard.string(forKey: user_id)!)",
                "otp" : otpNoTF.text!
                
                       
            ]
            print(parameter)
            
            API.callAPI(Webservices.verify_otp, headers: header, params: parameter) { (response, errorStr) in
                
                guard errorStr == nil else {
                    SVProgressHUD.dismiss()
                    self.errorAlert(message: errorStr!)
                    return
                }
                
                if let json = response as? JSON {
                    
                    print(json)
                    SVProgressHUD.dismiss()
                   
    //                 let json = try JSON(data: newData)
                       let postdata = json["data"]
                       let Success = json["success"].boolValue
                       let Message = json["message"].stringValue
                       let eventsData = json["data"]["events"]
                      
                    
                       if(Success == true){
                        UserDefaults.standard.set(postdata["token"].stringValue, forKey: token )
//                        self.successAlert(AppName, message: Message) {
                        
                        self.performSegue(withIdentifier: "MenuVC", sender: nil)
//                        DispatchQueue.main.async {
//                            self.userAuthenticated()
//                        }
//                        }
                       }else{
                        self.errorAlert(message: Message)
                    }
                    
                }
            }
            
        }
    
    func userAuthenticated(){
             
              var window: UIWindow?
             let navController = UIStoryboard(name: "LOGIN", bundle: nil).instantiateViewController(withIdentifier: "MenuVC")
             //let navController = UINavigationController(rootViewController: VC1)
             //navController.isNavigationBarHidden = true
             window = UIWindow(frame: UIScreen.main.bounds)
             window?.rootViewController = navController
             window?.makeKeyAndVisible()
             
             
         }

    func api_Resend_Function(){
            
            let header = ["Accept" : "application/json",
                          "x-api-key": appXApiKey]
            
            let parameter : [String : Any] = [
                           
    //                       "email" : emailTF.text!, //"akashp.spryox@gmail.com",//Constant.phone,  //UserDefaults.standard.value(forKey: "user_id")!
    //                       "password" : passwordTF.text! //  "12345678" //Constant.UUID! as Any
                
                "mobile_no" :  "\(UserDefaults.standard.string(forKey: mobile_no)!)",
                 "isd_code" : "\(UserDefaults.standard.string(forKey: isd_code)!)"
                
                       
            ]
            
            
        API.callAPI(Webservices.send_otp, headers: header, params: parameter) { (response, errorStr) in
                
                guard errorStr == nil else {
                    SVProgressHUD.dismiss()
                    self.errorAlert(message: errorStr!)
                    return
                }
                
                if let json = response as? JSON {
                    
                    print(json)
                    SVProgressHUD.dismiss()
                   
    //                 let json = try JSON(data: newData)
                       let postdata = json["data"]
                       let Success = json["success"].boolValue
                       let Message = json["message"].stringValue
                       let eventsData = json["data"]["events"]
                      
                    
                       if(Success == true){
                        self.successAlert(message: Message)
                       }else{
                        self.errorAlert(message: Message)
                    }
                    
                }
            }
            
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
