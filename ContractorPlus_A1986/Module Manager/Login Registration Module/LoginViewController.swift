
import UIKit
import SwiftHash
import SwiftyJSON
import SVProgressHUD
import FacebookLogin
import FacebookCore
import Toast_Swift
import Alamofire
import GoogleSignIn

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var logoImgView: UIImageView!
    
    @IBOutlet weak var forgetBtn: UIButton!
    
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var emailIcon: UIImageView!
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var passwordIcon: UIImageView!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var facebookUiview: UIView!
    @IBOutlet weak var googleUiView: UIView!
    @IBOutlet weak var registerBtn: UIButton!
    
    @IBOutlet weak var emailUiView: UIView!
    
    @IBOutlet weak var passwordUiView: UIView!
    
    
    @IBAction func forgetBtnAction(_ sender: UIButton) {
        guard let forgotPasswordvc = storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as? ForgetPasswordViewController else {return}
        navigationController?.pushViewController(forgotPasswordvc, animated: true)
    }
    
    
    @IBAction func loginBtnAction(_ sender: UIButton) {
        
        guard let emailId = emailTF.text, let password = passwordTF.text else {return}
        
        if emailId != "" && password != "" {
           normalLoginWith(emailId: emailId, password: password)
        }
        else {
            view.makeToast("Email Id and Password are required.", duration: 2.0, position: .bottom)
        }
        
    }
    
    //VARIABLES
    var socialLoginType = ""
    var socialLoginParameters = [String:Any]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        let loginManager = LoginManager()
        loginManager.logOut()
        
        emailUiView.cornerRadius1(value: 12.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.clear.cgColor)
        passwordUiView.cornerRadius1(value: 12.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.clear.cgColor)
        loginBtn.cornerRadius1(value: 12.0, shadowColor: UIColor.red.cgColor, borderColor: UIColor.clear.cgColor)
        facebookUiview.cornerRadius1(value: 12.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.clear.cgColor)
        googleUiView.cornerRadius1(value: 12.0, shadowColor: customeLightGrayColor.cgColor, borderColor: UIColor.clear.cgColor)
        // Do any additional setup after loading the view.
        
        configureInitialSetup()
        
    }
    
    private func configureInitialSetup() {
        
        let facebookTapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapFacebookLogin(_:)))
        facebookUiview.addGestureRecognizer(facebookTapGesture)
        
        let googleTapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapGoogleLogin(_:)))
        googleUiView.addGestureRecognizer(googleTapGesture)
        
    }
    
}

//MARK: FACEBOOK LOGIN IMPLEMENTATION
extension LoginViewController: GraphRequestConnectionDelegate {
    
    @objc
    func didTapFacebookLogin(_ sender: UITapGestureRecognizer) {
        
        socialLoginType = SocialLogin.FACEBOOK.rawValue
        
        let loginManager = LoginManager()
        loginManager.logIn(
            permissions: [.publicProfile, .email],
            viewController: self
        ) { result in
            
            switch result {
            case .cancelled:
                print("User Cancelled the Login Process")
            case .failed(let error):
                print("Login Process Failed: \(error)")
            case .success(_, _, let accessToken):
                //print("Access Token: \(accessToken.tokenString)")
                self.fetchFbData(accessToken: accessToken)
                
            }
        }
        
    }
    
    @objc
    func didTapGoogleLogin(_ sender: UITapGestureRecognizer) {
        
        print("Google Login BtnTapped")
        socialLoginType = SocialLogin.GOOGLE.rawValue
        GIDSignIn.sharedInstance()?.clientID = googleSignInClientId
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.signIn()
        
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension LoginViewController {
    
    //MARK: LOGIN SERVICE FOR FACEBOOK & GOOGLE
    func getSocialLoginDetails() {
        
        view.makeToastActivity(.center)
        
        let headers = [
            "Content-Type": "application/json",
            "x-api-key": appXApiKey
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.social_login, method: .post, parameters: socialLoginParameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (socialLoginDetails: SocialLoginDetail!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = socialLoginDetails {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        //print(returnedResponse.data)
                        UserDefaults.standard.set(returnedResponse.data.token, forKey: token)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.id, forKey: user_id)
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            
                            if returnedResponse.data.isOtpVerified == "false" {
                                //Navigate and get the mobile number to be saved
                                UserDefaults.standard.set(returnedResponse.data.userDetails.id, forKey: user_id)
                                UserDefaults.standard.set(false , forKey: "is_authenticated")
                                guard let mobileNumberinputvc = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as? ForgetPasswordViewController else {return}
                                mobileNumberinputvc.isFromSocialLogin = true
                                self.navigationController?.pushViewController(mobileNumberinputvc, animated: true)
                                
                            }
                            else {
                                //Save the data and move forward
                                
                                UserDefaults.standard.set(returnedResponse.data.token, forKey: token)
                                UserDefaults.standard.set(returnedResponse.data.role, forKey: role)
                                UserDefaults.standard.set(returnedResponse.data.isOtpVerified, forKey: is_otp_verified)
                                UserDefaults.standard.set(returnedResponse.data.userDetails.firstName, forKey: first_name)
                                UserDefaults.standard.set(returnedResponse.data.userDetails.lastName, forKey: last_name)
                                UserDefaults.standard.set(returnedResponse.data.userDetails.email, forKey: email)
                                UserDefaults.standard.set(returnedResponse.data.userDetails.fullName, forKey: full_name)
                                UserDefaults.standard.set(returnedResponse.data.userDetails.id, forKey: user_id)
                                
                                if UserDefaults.standard.string(forKey: user_profile_photo) == noUserDefaultPhotoUrl {
                                   UserDefaults.standard.set(returnedResponse.data.userDetails.profilePicture, forKey: user_profile_photo)
                                }
                                
                                UserDefaults.standard.set(true, forKey: "is_authenticated")
                                
                                //Saving Estimate Settings Values
                                if let settings = returnedResponse.data.settings {
                                   
                                    if let brandingSettings = settings.brandingSetting {
                                        self.saveBrandingSettingsToUserDefaults(brandingSettings: brandingSettings)
                                    }
                                    
                                    if let estimateSettings = settings.estimateSetting {
                                        self.saveEstimateSettingsToUserDefaults(estimateSettings: estimateSettings)
                                        
                                    }
                                    
                                    if let invoiceSettings = settings.invoiceSetting {
                                        self.saveInvoiceSettingToUserDefaults(invoiceSettings: invoiceSettings)
                                        
                                    }
                                    
                                    if let mileageSettings = settings.mileageLogSetting {
                                        self.saveMileageSettingToUserDefaults(mileageSettings: mileageSettings)
                                    }
                                    
                                    if let quickBookSettings = settings.quickBookSetting {
                                        self.saveQuickBookSettingToUserDefaults(quickBookSettings: quickBookSettings)
                                    }
                                    
                                    if let timeClockSettings = settings.timeClockSetting {
                                        self.saveTimeSettingsToUserDefaults(timeSettings: timeClockSettings)
                                    }
                                    
                                    if let shoppingSettings = settings.shoppingSetting {
                                        self.saveShoppingSettingToUserDefaults(shoppingSettings: shoppingSettings)
                                    }
                                    
                                }
                                
                                DispatchQueue.main.async {
                                    self.view.hideToastActivity()
                                    //Navigate to Main Dashboard
                                    //Todo Issue presenting the sidemenu.. needs fixes
                                    
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "LOGIN", bundle:nil)
                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MenuVC") as! UINavigationController
                                    nextViewController.modalPresentationStyle = .fullScreen
                                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                    appdelegate.window!.rootViewController?.present(nextViewController, animated: true, completion: nil)
                                    
                                }
                                
                            }
                            
                        }
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
    }
    
    //MARK: NORMAL LOGIN
    
    func normalLoginWith(emailId: String, password: String) {
        
        view.makeToastActivity(.center)
        
        let headers = [
            "Content-Type": "application/json",
            "x-api-key": appXApiKey
        ]
        
        let parameter : [String : Any] = [
            "email_address" : emailId,
            "password" : MD5(password).lowercased()
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.login, method: .post, parameters: parameter, encodingType: JSONEncoding.default, headers: headers) { [weak self] (loginResponse: SocialLoginDetail!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = loginResponse {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        
                        UserDefaults.standard.set(returnedResponse.data.token, forKey: token)
                        UserDefaults.standard.set(returnedResponse.data.role, forKey: role)
                        UserDefaults.standard.set(returnedResponse.data.isOtpVerified, forKey: is_otp_verified)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.firstName, forKey: first_name)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.lastName, forKey: last_name)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.email, forKey: email)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.fullName, forKey: full_name)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.id, forKey: user_id)
                        UserDefaults.standard.set(returnedResponse.data.userDetails.profilePicture, forKey: user_profile_photo)
                        UserDefaults.standard.set(true, forKey: "is_authenticated")
                        
                        //Saving Estimate Settings Values
                        if let settings = returnedResponse.data.settings {
                           
                            if let brandingSettings = settings.brandingSetting {
                                self.saveBrandingSettingsToUserDefaults(brandingSettings: brandingSettings)
                            }
                            
                            if let estimateSettings = settings.estimateSetting {
                                self.saveEstimateSettingsToUserDefaults(estimateSettings: estimateSettings)
                            }
                            
                            if let invoiceSettings = settings.invoiceSetting {
                                self.saveInvoiceSettingToUserDefaults(invoiceSettings: invoiceSettings)
                            }
                            
                            if let mileageSettings = settings.mileageLogSetting {
                                self.saveMileageSettingToUserDefaults(mileageSettings: mileageSettings)
                            }
                            
                            if let quickBookSettings = settings.quickBookSetting {
                                self.saveQuickBookSettingToUserDefaults(quickBookSettings: quickBookSettings)
                            }
                            
                            if let timeClockSettings = settings.timeClockSetting {
                                self.saveTimeSettingsToUserDefaults(timeSettings: timeClockSettings)
                            }
                            
                            if let shoppingSettings = settings.shoppingSetting {
                                self.saveShoppingSettingToUserDefaults(shoppingSettings: shoppingSettings)
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            let storyBoard : UIStoryboard = UIStoryboard(name: "LOGIN", bundle:nil)
                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MenuVC") as! UINavigationController
                            //        let navigationController = UINavigationController(rootViewController: nextViewController)
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                            appdelegate.window!.rootViewController = nextViewController
                            
                        }
                        
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
}

//MARK: FACEBOOK METHODS IMPLEMENTATION
extension LoginViewController {
    
    func fetchFbData(accessToken: AccessToken) {
        
        let request = GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"], httpMethod: .get)
        request.start { (connection, result, error) in
            
            if let error = error {
                debugPrint(error.localizedDescription)
                return
            }
            else {
                
                guard let userDictionary = result as? [String:Any] else {return}
                
                guard let firstName = userDictionary["first_name"] as? String,
                    let lastName = userDictionary["last_name"] as? String,
                    let id = userDictionary["id"] as? String,
                    let email = userDictionary["email"] as? String else {return}
                
                self.socialLoginParameters = ["type": self.socialLoginType,
                                              "id": id, "name": "\(firstName) \(lastName)",
                    "email": email, "fcm_token": UserDefaults.standard.string(forKey: fcmTokenString)!]
                
                self.getSocialLoginDetails()
                
            }
            
        }
        
    }
    
}

//MARK: GOOGLE SIGN IN DELEGATE IMPLEMENTATION
extension LoginViewController: GIDSignInDelegate {
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        //let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        //let givenName = user.profile.givenName
        //let familyName = user.profile.familyName
        let email = user.profile.email
        
        if user.profile.hasImage {
            if UserDefaults.standard.string(forKey: user_profile_photo) == noUserDefaultPhotoUrl {
                
                UserDefaults.standard.set(user.profile.imageURL(withDimension: 100)!.absoluteString, forKey: user_profile_photo)
                
            }
        }
        
        self.socialLoginParameters = ["type": self.socialLoginType,
                                      "id": userId ?? "", "name": fullName ?? "",
                                      "email": email ?? "", "fcm_token": UserDefaults.standard.string(forKey: fcmTokenString)!]
        
        self.getSocialLoginDetails()
        
        
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        print(error.localizedDescription)
    }
    
}

//MARK: ADDITION FUNCTIONS TO SAVE SETTINGS TO USER DEFAULTS
extension LoginViewController {
    
    func saveEstimateSettingsToUserDefaults(estimateSettings: EstimateSetting) {
        UserDefaults.standard.set(estimateSettings.defaultLabourRate, forKey: EstimateSettingsSavedDefaults.defaultLabourRate)
        UserDefaults.standard.set(estimateSettings.supplyMarkup, forKey: EstimateSettingsSavedDefaults.supplyMarkupRate)
        UserDefaults.standard.set(estimateSettings.supplyMarkupUnit, forKey: EstimateSettingsSavedDefaults.supplyMarkupUnit)
    }
    
    func saveMileageSettingsToUserDefaults(mileageSettings: MileageLogSetting) {
        UserDefaults.standard.set(mileageSettings.trackRoutes, forKey: MileageSettingsSavedDefaults.trackRoutes)
        UserDefaults.standard.set(mileageSettings.trackDrivingSpeed, forKey: MileageSettingsSavedDefaults.trackDrivingSpeed)
        UserDefaults.standard.set(mileageSettings.trackDrivingTime, forKey: MileageSettingsSavedDefaults.trackDrivingTime)
    }
    
    func saveTimeSettingsToUserDefaults(timeSettings: TimeClockSetting) {
        UserDefaults.standard.set(timeSettings.requireEmployeeClockinClockout, forKey: TimeClockSettingsSavedDefaults.requiredEmployeeClockInOut)
        UserDefaults.standard.set(timeSettings.acceptableDistance, forKey: TimeClockSettingsSavedDefaults.acceptableDistance)
    }
    
    func saveBrandingSettingsToUserDefaults(brandingSettings: BrandingSetting) {
        #warning("Save Branding Setting to user default from here")
    }
    
    func saveInvoiceSettingToUserDefaults(invoiceSettings: InvoiceSetting) {
        #warning("Save Invoice Setting to user default from here")
    }
    
    func saveMileageSettingToUserDefaults(mileageSettings: MileageLogSetting) {
        #warning("Save Mileage Setting to user default from here")
    }
    
    func saveQuickBookSettingToUserDefaults(quickBookSettings: QuickBookSetting) {
        #warning("Save Quickbook Setting to user default from here")
    }
    
    func saveTimeClockSettingToUserDefaults(timeClockSettings: TimeClockSetting) {
        #warning("Save Time Clock Setting to user default from here")
    }
    
    func saveShoppingSettingToUserDefaults(shoppingSettings: ShoppingSetting) {
        #warning("Save Shopping Setting to user default from here")
    }
    
}
