//
//  ForgetPasswordViewController.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 11/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//


import UIKit
import SwiftyJSON
import SVProgressHUD
import CountryList
import Alamofire
import Toast_Swift

class ForgetPasswordViewController: UIViewController ,CountryListDelegate {
    var countryList = CountryList()
    @IBOutlet weak var codeNoTF: UITextField!
    @IBOutlet weak var mobileNoTF: UITextField!
    
    @IBOutlet weak var otpBtn: UIButton!
    
    //Variables
    var isFromSocialLogin = false
    
    
    //    @IBAction func resendBtnAction(_ sender: UIButton) {
    //
    //        api_Resend_Function()
    //
    //    }
    
    
    @IBAction func selecteCountryBtnAction(_ sender: UIButton) {
        
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
        
    }
    func selectedCountry(country: Country) {
        
        codeNoTF.text = "+\(country.phoneExtension)"
    }
    
    @IBAction func otpBtnAction(_ sender: UIButton) {
        
        if mobileNoTF.text != "" && codeNoTF.text != "" {
            
            if isFromSocialLogin {
                setMobileNumber()
            }
            else {
                api_Resend_Function()
            }
            
        }else{
            self.errorAlert(message: textfieldEmptyError)
        }
    }
    
    @IBAction func backBtnActon(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryList.delegate = self
        otpBtn.cornerRadius1(value: 12.0, shadowColor: UIColor.red.cgColor, borderColor: UIColor.clear.cgColor)
        mobileNoTF.addBottomBorderWithColor(color: UIColor.red, width: 3)
        codeNoTF.addBottomBorderWithColor(color: UIColor.red, width: 3)
        // Do any additional setup after loading the view.
    }
    
    func api_Resend_Function(){
        
        let header = ["Accept" : "application/json",
                      "x-api-key": appXApiKey]
        
        let parameter : [String : Any] = [
            
            //                       "email" : emailTF.text!, //"akashp.spryox@gmail.com",//Constant.phone,  //UserDefaults.standard.value(forKey: "user_id")!
            //                       "password" : passwordTF.text! //  "12345678" //Constant.UUID! as Any
            
            "mobile_no" :  mobileNoTF.text!,
            "isd_code" : codeNoTF.text!
            
            
        ]
        
        
        API.callAPI(Webservices.send_otp, headers: header, params: parameter) { (response, errorStr) in
            
            guard errorStr == nil else {
                SVProgressHUD.dismiss()
                self.errorAlert(message: errorStr!)
                return
            }
            
            if let json = response as? JSON {
                
                print(json)
                SVProgressHUD.dismiss()
                
                //                 let json = try JSON(data: newData)
                let postdata = json["data"]
                let Success = json["success"].boolValue
                let Message = json["message"].stringValue
                let eventsData = json["data"]["events"]
                
                
                if(Success == true){
                    UserDefaults.standard.set(self.codeNoTF.text!, forKey: isd_code )
                    UserDefaults.standard.set(self.mobileNoTF.text!, forKey: mobile_no )
                    UserDefaults.standard.set(postdata["user_id"].stringValue, forKey: user_id)
                    
                    self.performSegue(withIdentifier: "NewPassword", sender: nil)
                    
                }else{
                    self.errorAlert(message: Message)
                }
                
            }
        }
        
    }
    
    func setMobileNumber() {
        
        let header = ["Accept" : "application/json",
                      "x-api-key": appXApiKey]
        
        let parameter : [String : Any] = [
            "user_id" : UserDefaults.standard.integer(forKey: user_id),
            "mobile_no" :  mobileNoTF.text!,
            "isd_code" : codeNoTF.text!
        ]
        
        API.callAPI(Webservices.setMobileNumber, headers: header, params: parameter) { (response, errorStr) in
            
            guard errorStr == nil else {
                SVProgressHUD.dismiss()
                self.errorAlert(message: errorStr!)
                return
            }
            
            if let json = response as? JSON {
                
                print(json)
                SVProgressHUD.dismiss()
                
                //                 let json = try JSON(data: newData)
                let postdata = json["data"]
                let Success = json["success"].boolValue
                let Message = json["message"].stringValue
                let eventsData = json["data"]["events"]
                
                
                if(Success == true){
                    UserDefaults.standard.set(self.codeNoTF.text!, forKey: isd_code )
                    UserDefaults.standard.set(self.mobileNoTF.text!, forKey: mobile_no )
                    
                    DispatchQueue.main.async {
                        guard let otpConfirmationvc = self.storyboard?.instantiateViewController(withIdentifier: "NewPasswordViewController") as? NewPasswordViewController else {return}
                        otpConfirmationvc.isFromSocialLogin = true
                        self.navigationController?.pushViewController(otpConfirmationvc, animated: true)
                    }
                    
                }else{
                    self.errorAlert(message: Message)
                }
                
            }
        }
        
    }
    
}
