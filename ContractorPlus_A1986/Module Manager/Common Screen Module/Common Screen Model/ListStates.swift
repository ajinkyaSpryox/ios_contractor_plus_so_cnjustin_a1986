//
//  ListStates.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 24/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListStates
struct ListStates: Codable {
    let success, message: String
    let data: [ListStatesData]
}

// MARK: - Datum
struct ListStatesData: Codable {
    let id: Int
    let name: String
    let countryID: Int

    enum CodingKeys: String, CodingKey {
        case id, name
        case countryID = "country_id"
    }
}
