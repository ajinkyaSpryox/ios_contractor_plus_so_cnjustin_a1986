//
//  ListCountries.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 24/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListCountries
struct ListCountries: Codable {
    let success, message: String
    let data: [ListCountriesData]
}

// MARK: - Datum
struct ListCountriesData: Codable {
    let id: Int
    let name, isdCode: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case isdCode = "isd_code"
    }
}
