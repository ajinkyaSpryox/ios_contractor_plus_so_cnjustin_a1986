//
//  CommonAlertVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 19/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class CommonAlertVC: UIViewController {
    
    @IBOutlet weak var alertTitleLbl: UILabel!
    @IBOutlet weak var alertMessageLbl: UILabel!
    @IBOutlet weak var actionBtn: UIButton!
    
    //Received variables
    var alertTitle: String?
    var alertMessage: String?
    var actionButtonTitle: String?
    
    var onOkBtnTap: ((_ success: Bool) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertTitleLbl.text = alertTitle ?? ""
        alertMessageLbl.text = alertMessage ?? "Are You Sure"
        actionBtn.setTitle(actionButtonTitle ?? "Yes", for: .normal)
        
    }
    
    //MARK: IB-ACTIONS IMPLEMENTATION
    @IBAction func okBtnTapped(_ sender: UIButton) {
        onOkBtnTap?(true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

}
