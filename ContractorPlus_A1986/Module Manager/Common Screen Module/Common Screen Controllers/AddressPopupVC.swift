//
//  AddressPopupVC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 31/12/19.
//  Copyright © 2019 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

protocol AddressPopupVCDelegate: class {
    func userDidSelectAddress(selectedAddress: AddressDetail)
}

class AddressPopupVC: UIViewController {
    
    //Required Variables
    var clientId: Int!
    
    //IB-Outlets
    @IBOutlet weak var clientAddressTableView: UITableView!
    
    //Variables
    var clientAddressDetailsArray = [AddressDetail]()
    
    //Delegate
    weak var delegate: AddressPopupVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    fileprivate func setupInitialUI() {
        serviceCalls()
        
        clientAddressTableView.delegate = self
        clientAddressTableView.dataSource = self
        
        clientAddressTableView.register(UINib(nibName: "ClientDetailsTblCell", bundle: nil), forCellReuseIdentifier: "ClientDetailsTblCell")
    }
    
    fileprivate func serviceCalls() {
       getClientDetails()
    }
    
    
    
}

//MARK: IB-ACTIONS
extension AddressPopupVC {
    
    @IBAction func addAddressBtnTapped(_ sender: UIButton) {
        guard let addNewAddressvc = storyboard?.instantiateViewController(withIdentifier: "AddNewAddressPopVC") as? AddNewAddressPopVC else {return}
        addNewAddressvc.clientId = self.clientId
        addNewAddressvc.delegate = self
        addNewAddressvc.modalPresentationStyle = .overCurrentContext
        addNewAddressvc.modalTransitionStyle = .crossDissolve
        self.present(addNewAddressvc, animated: true, completion: nil)
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension AddressPopupVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clientAddressDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ClientDetailsTblCell", for: indexPath) as? ClientDetailsTblCell else {return UITableViewCell()}
        cell.configureCell(addressDetails: clientAddressDetailsArray[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.userDidSelectAddress(selectedAddress: clientAddressDetailsArray[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: Add NEW ADDRESS VIEW CONTROLLER DELEGATE IMPLEMENTATION
extension AddressPopupVC: AddNewAddressPopVCDelegate {
    
    func didUpdateAddressFor(clientId: Int) {
        self.clientId = clientId
        serviceCalls()
    }
    
}


//MARK: API SERVICE CALLS IMPLEMENTATION
extension AddressPopupVC {
    
    func getClientDetails() {
        
        view.makeToastActivity(.center)
        
        let parameters: [String:Any] = ["client_id": self.clientId]
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.get_client_details, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [weak self] (clientDetails: ClientDetails!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                self.view.hideToastActivity()
                self.view.makeToast(error, duration: 2.0, position: .bottom)
            }
            else {
                if let returnedResponse = clientDetails {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        
                        if let clientData = returnedResponse.data {
                            
                            if let clientAddressDetailsArr = clientData.addressDetails {
                               self.clientAddressDetailsArray = clientAddressDetailsArr
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.clientAddressTableView.reloadData()
                        }
                        //print(self.clientAddressDetailsArray)
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                            print(returnedResponse.success)
                        }
                    }
                }
            }
            
            
        }
        
    }
    
}
