//
//  AddNewAddressPopVC.swift
//  ContractorPlus_A1986
//
//  Created by !Girish on 15/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

protocol AddNewAddressPopVCDelegate: class {
    func didUpdateAddressFor(clientId: Int)
}

class AddNewAddressPopVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var address1TextView: UITextView!
    @IBOutlet weak var address2TextView: UITextView!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipcodeTextField: UITextField!
    
    //Received Variables
    var clientId: Int!
    
    //Variables
    var countriesListArr = [ListCountriesData]()
    var statesListArr = [ListStatesData]()
    var parameters = [String:Any]()
    
    //Delegate
    weak var delegate: AddNewAddressPopVCDelegate?
    
    //PickerViews
    var pickerView = UIPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        
        setupDelegates()
        
        address1TextView.text = "Enter Address"
        address1TextView.textColor = .lightGray
        
        address2TextView.text = "Enter Address"
        address2TextView.textColor = .lightGray
        
        countryCodeTextField.isHidden = true
        
        getCountriesList()
    }
    
    private func setupDelegates() {
        
        address1TextView.delegate = self
        address2TextView.delegate = self
        
        countryTextField.delegate = self
        stateTextField.delegate = self
        
    }

}

//MARK: IB-ACTIONS IMPLEMENTATION
extension AddNewAddressPopVC {
    
    @IBAction func confirmAddressBtnTapped(_ sender: UIButton) {
        addNewAddress()
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension AddNewAddressPopVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //if text field is client then add picker view
        
        switch textField {
        case countryTextField:
            countriesListArr.count == 0 ? view.makeToast("No Countries Found", duration: 2.0, position: .center) : addPickerToCountryTextField()
        case stateTextField:
            statesListArr.count == 0 ? view.makeToast("No States Found", duration: 2.0, position: .center) : addPickerToStatesTextField()
        default:
            return
        }
        
    }
    
    
    func addPickerToCountryTextField() {
        pickerView.tag = 1
        countryTextField.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func addPickerToStatesTextField() {
        pickerView.tag = 2
        stateTextField.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
}

extension AddNewAddressPopVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Enter Address"
            address1TextView.textColor = .lightGray
        }
        
    }
    
}

extension AddNewAddressPopVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 1 {
            return countriesListArr.count
        }
        else {
            return statesListArr.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1 {
            return countriesListArr[row].name
        }
        else {
            return statesListArr[row].name
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            countryTextField.text = countriesListArr[row].name
            countryCodeTextField.text = countriesListArr[row].isdCode
            countryCodeTextField.isHidden = false
            parameters["country_id"] = countriesListArr[row].id
            getStatesListWith(countryId: countriesListArr[row].id) //Get States List With Selected Country Id Service
        }
        else {
            stateTextField.text = statesListArr[row].name
            parameters["state_id"] = statesListArr[row].id
        }
        
    }
    
}


//MARK: API SERVICES IMPLEMENTATION
extension AddNewAddressPopVC {
    
    //Get Countries List
    func getCountriesList() {
        
        let header = ["Accept" : "application/json",
        "x-api-key": appXApiKey]
        
        let parameters: [String:Any] = [:]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_countries, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: header) { [weak self] (countriesList: ListCountries!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = countriesList {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        self.countriesListArr = returnedResponse.data
                        //print(self.countriesListArr)
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //Get States List
    func getStatesListWith(countryId: Int) {
        
        let header = ["Accept" : "application/json",
        "x-api-key": appXApiKey]
        
        let parameters: [String:Any] = ["country_id": countryId]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_states, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: header) { [weak self] (statesList: ListStates!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if let returnedResponse = statesList {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        self.statesListArr = returnedResponse.data
                        //print(self.statesListArr)
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func addNewAddress() {
        
        guard let address1 = address1TextView.text, let address2 = address2TextView.text, let city = cityTextField.text, let zipcode = zipcodeTextField.text, let state = stateTextField.text, let country = countryTextField.text else {
            self.view.makeToast("All Details are mandatory")
            return}
        
        view.makeToastActivity(.center)
        
        parameters["address_1"] = address1
        parameters["address_2"] = address2
        parameters["city"] = city
        parameters["zipcode"] = zipcode
        parameters["client_id"] = self.clientId
        parameters["is_new_address"] = 1
        print(country)
        print(state)
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.add_client, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { [weak self] (addNewAddress: DeleteEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            
            if let returnedResponse = addNewAddress {
                
                if returnedResponse.success == "true" {
                    //Success
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 1.0, position: .bottom)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self.delegate?.didUpdateAddressFor(clientId: self.clientId)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                else {
                    //Failure
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
}
