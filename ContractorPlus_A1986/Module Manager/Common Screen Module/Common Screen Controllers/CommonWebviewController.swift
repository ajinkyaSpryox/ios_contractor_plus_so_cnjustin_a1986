//
//  CommonWebviewController.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 13/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import WebKit

class CommonWebviewController: UIViewController, WKNavigationDelegate {
    
    //Outlets
    @IBOutlet weak var webView: WKWebView!
    
    //Received Variables
    var urlLinkToOpen: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        guard let urlLinkToOpen = urlLinkToOpen else {return}
        guard let url = URL(string: urlLinkToOpen) else {return}
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        
    }

}
