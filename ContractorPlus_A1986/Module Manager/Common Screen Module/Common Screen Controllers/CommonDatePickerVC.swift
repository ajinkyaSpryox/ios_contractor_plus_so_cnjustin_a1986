//
//  CommonDatePickerVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 21/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

protocol CommonDatePickerVCDelegate: class {
    func getSelectedDateFromPicker(date: String, monthYear: String, weekDay: String, isFromDate: Bool, originalSelectedDate: String)
}

class CommonDatePickerVC: UIViewController {
    
    var isFromDate = false
    @IBOutlet weak var datePicker: UIDatePicker!
    weak var delegate: CommonDatePickerVCDelegate?
    
    //Variables
    let dateFormatter = DateFormatter()
    private var selectedDate = ""
    private var selectedMonthYear = ""
    private var weekDay = ""
    private var originalSelectedDate = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        
        datePicker.addTarget(self, action: #selector(datePickerDateChange(_:)), for: .valueChanged)
    }

}

extension CommonDatePickerVC {
    
    @objc
    func datePickerDateChange(_ sender: UIDatePicker) {
        
        setupDatesWith(date: sender.date)
        
    }
    
    func setupDatesWith(date: Date) {
        
        dateFormatter.dateFormat = "d"
        let selectedDate = dateFormatter.string(from:date)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let selectedMonthYear = dateFormatter.string(from:date)
        
        
        dateFormatter.dateFormat = "EEEE"
        let selectedWeekday = dateFormatter.string(from:date)
        
        self.selectedDate = selectedDate
        self.selectedMonthYear = selectedMonthYear
        self.weekDay = selectedWeekday
        
        dateFormatter.dateFormat = formattedDateString
        self.originalSelectedDate = dateFormatter.string(from:date)
        
    }
    
    
}


//MARK: IB-ACTIONS IMPLEMENTATION
extension CommonDatePickerVC {
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmBtnTapped(_ sender: UIButton) {
        delegate?.getSelectedDateFromPicker(date: self.selectedDate, monthYear: self.selectedMonthYear, weekDay: self.weekDay, isFromDate: self.isFromDate, originalSelectedDate: self.originalSelectedDate)
        
        dismiss(animated: true, completion: nil)
    }
    
}
