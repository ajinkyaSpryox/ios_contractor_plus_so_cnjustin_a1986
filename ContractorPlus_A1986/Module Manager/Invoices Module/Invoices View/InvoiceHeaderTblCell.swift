//
//  InvoiceHeaderTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 03/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

class InvoiceHeaderTblCell: UITableViewCell {
    
    @IBOutlet weak var invoiceCreationDateLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(invoiceList: InvoiceList) {
        invoiceCreationDateLbl.text = invoiceList.date
    }
    
}
