//
//  AddLineItemTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 04/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

protocol AddLineItemTblCellDelegate: class {
    func didTapRemoveItemBtn(itemDetails: AddLineItem, at indexpath: IndexPath)
}

class AddLineItemTblCell: UITableViewCell {
    
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!
    
    weak var delegate: AddLineItemTblCellDelegate?
    var selectedLineItem: AddLineItem!
    var selectedIndexpath: IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(itemDetails: AddLineItem, delegate: AddLineItemTblCellDelegate, indexpath: IndexPath) {
        
        self.delegate = delegate
        self.selectedLineItem = itemDetails
        self.selectedIndexpath = indexpath
        
        itemNameLabel.text = itemDetails.description
        itemPriceLabel.text = "$ \(itemDetails.totalCount)"
    }
    
    @IBAction func removeItemBtnTapped(_ sender: UIButton) {
        delegate?.didTapRemoveItemBtn(itemDetails: selectedLineItem, at: selectedIndexpath)
    }
    
}
