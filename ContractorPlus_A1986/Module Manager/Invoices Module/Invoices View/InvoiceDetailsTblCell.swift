//
//  InvoiceDetailsTblCell.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 03/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit

protocol InvoiceDetailsTblCellDelegate : class {
    func didTapDeleteInvoiceBtn(invoiceDetails: Invoice, at indexPath: IndexPath)
    func didTapEditInvoiceBtn(invoiceDetails: Invoice, at indexPath: IndexPath)
    func didTapSendEmailToCustomerBtn(invoiceDetails: Invoice, at indexpath: IndexPath)
}

class InvoiceDetailsTblCell: UITableViewCell {
    
    @IBOutlet weak var clientNameLbl: UILabel!
    @IBOutlet weak var invoiceNumberLbl: UILabel!
    @IBOutlet weak var clientAddressLbl: UILabel!
    @IBOutlet weak var generatorNameLbl: UILabel!
    @IBOutlet weak var generatorImage: UIImageView!
    @IBOutlet weak var bidAmountPriceLbl: UILabel!
    @IBOutlet weak var paidPercentageLbl: UILabel!
    @IBOutlet weak var bottomOptionsView: UIView!
    
    //Variables
    var delegate: InvoiceDetailsTblCellDelegate?
    var selectedIndexpath: IndexPath!
    var selectedInvoiceDetails: Invoice!

    override func awakeFromNib() {
        super.awakeFromNib()
        bottomOptionsView.clipsToBounds = true
        bottomOptionsView.layer.cornerRadius = 10
        bottomOptionsView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func configureCell(invoiceDetail: Invoice, indexPath: IndexPath, delegate: InvoiceDetailsTblCellDelegate) {
        
        self.delegate = delegate
        self.selectedIndexpath = indexPath
        self.selectedInvoiceDetails = invoiceDetail
        
        clientNameLbl.text = "\(invoiceDetail.clientName)"
        invoiceNumberLbl.text = invoiceDetail.invoiceNo
        clientAddressLbl.text = invoiceDetail.clientAddress
        generatorNameLbl.text = invoiceDetail.generatedBy.name
        bidAmountPriceLbl.text = "$ \(invoiceDetail.cost)"
        paidPercentageLbl.text = invoiceDetail.paid
    }
    
    @IBAction func deleteInvoiceBtnTapped(_ sender: UIButton) {
        delegate?.didTapDeleteInvoiceBtn(invoiceDetails: selectedInvoiceDetails, at: selectedIndexpath)
    }
    
    @IBAction func editInvoiceBtnTapped(_ sender: UIButton) {
        delegate?.didTapEditInvoiceBtn(invoiceDetails: selectedInvoiceDetails, at: selectedIndexpath)
    }
    
    @IBAction func sendEmailToCustomerBtnTapped(_ sender: UIButton) {
        delegate?.didTapSendEmailToCustomerBtn(invoiceDetails: selectedInvoiceDetails, at: selectedIndexpath)
    }
    
}
