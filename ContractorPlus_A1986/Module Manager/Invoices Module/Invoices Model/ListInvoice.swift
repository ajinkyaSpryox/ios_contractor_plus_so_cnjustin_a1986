//
//  ListInvoice.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 03/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - ListInvoice
struct ListInvoice: Codable {
    let success, message: String
    let data: ListInvoiceData?
}

// MARK: - DataClass
struct ListInvoiceData: Codable {
    var invoiceList: [InvoiceList]
    let pageCount: Int
    
    enum CodingKeys: String, CodingKey {
        case invoiceList = "invoice_list"
        case pageCount = "page_count"
    }
}

// MARK: - InvoiceList
struct InvoiceList: Codable {
    let date: String
    var invoices: [Invoice]
}

// MARK: - Invoice
struct Invoice: Codable {
    let id: Int
    let clientName, invoiceNo, invoiceDate, clientAddress: String
    let generatedBy: GeneratedBy
    let cost: Double
    let paid: String
    let paymentCompleted: Bool
    let displayNameInList: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case clientName = "client_name"
        case invoiceNo = "invoice_no"
        case invoiceDate = "invoice_date"
        case clientAddress = "client_address"
        case generatedBy = "generated_by"
        case cost, paid
        case paymentCompleted = "payment_completed"
        case displayNameInList = "display_in_list"
    }
}

// MARK: - GeneratedBy
struct GeneratedBy: Codable {
    let id: Int
    let name: String
    let picture: String
}
