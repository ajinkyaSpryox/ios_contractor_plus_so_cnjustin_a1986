//
//  InvoiceDetails.swift
//  ContractorPlus_A1986
//
//  Created by SpryOX MacMini Admin on 07/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import Foundation

// MARK: - InvoiceDetails
struct InvoiceDetails: Codable {
    let success, message: String
    let data: InvoiceDetailsData
}

// MARK: - DataClass
struct InvoiceDetailsData: Codable {
    let invoiceID, clientID, clientAddressId: Int
    let clientName, discountUnit: String
    let discount: Int
    let estimateID: Int?
    let invoiceNo, dateOfInvoice, poNumber: String
    let paymentTerms: Int
    let pdfLink: String
    let lineItems: [AddLineItem]

    enum CodingKeys: String, CodingKey {
        case invoiceID = "invoice_id"
        case clientID = "client_id"
        case clientName = "client_name"
        case estimateID = "estimate_id"
        case discountUnit = "discount_unit"
        case discount
        case invoiceNo = "invoice_no"
        case dateOfInvoice = "date_of_invoice"
        case poNumber = "po_number"
        case paymentTerms = "payment_terms"
        case pdfLink = "pdf_link"
        case lineItems = "line_items"
        case clientAddressId = "client_address_id"
    }
}
