//
//  ManageInvoicesVC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 02/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ManageInvoicesVC: UIViewController {
    
    //IB-Outlets
    @IBOutlet weak var invoiceTableView: UITableView!
    @IBOutlet weak var emptyInvoiceStack: UIStackView!
    @IBOutlet weak var fromDateStackView: UIStackView!
    @IBOutlet weak var toDateStackView: UIStackView!
    
    //From Date Outlets
    @IBOutlet weak var fromDateLbl: UILabel!
    @IBOutlet weak var fromMonthYearLbl: UILabel!
    @IBOutlet weak var fromWeekDayLbl: UILabel!
    
    //To Date Outlets
    @IBOutlet weak var toDateLbl: UILabel!
    @IBOutlet weak var toMonthYearLbl: UILabel!
    @IBOutlet weak var toWeekDayLbl: UILabel!
    
    //Variables
    var invoicesDataArray = [InvoiceList]()
    let dateFormatter = DateFormatter()
    var selectedFromDateString = ""
    var selectedToDateString = ""
    
    //Pagination Variables
    var currentPageCount = 1
    var maxPageCount = 0
    var isLoadingMoreInvoices = false
    var cellHeights = [IndexPath: CGFloat]()
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        serviceCalls()
        setupInitialUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        invoicesDataArray.removeAll()
        currentPageCount = 1
    }
    
    fileprivate func setupInitialUI() {
        title = "Invoices"
        emptyInvoiceStack.isHidden = true
        invoiceTableView.delegate = self
        invoiceTableView.dataSource = self
        invoiceTableView.register(UINib(nibName: "InvoiceDetailsTblCell", bundle: nil), forCellReuseIdentifier: "InvoiceDetailsTblCell")
        invoiceTableView.register(UINib(nibName: "InvoiceHeaderTblCell", bundle: nil), forCellReuseIdentifier: "InvoiceHeaderTblCell")
        
        let fromDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectFromDate(_:)))
        fromDateStackView.addGestureRecognizer(fromDateTapGesture)
        
        let toDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectToDate(_:)))
        toDateStackView.addGestureRecognizer(toDateTapGesture)
        
        setCurrentDateOnFromDate()
        setTomorrowDateOnToDate()
    }
    
    fileprivate func serviceCalls() {
        getInvoiceListFor(pageNumber: currentPageCount, fromDate: "", toDate: "")
    }
    
    //MARK: IB-ACTIONS
    @IBAction func createInvoiceBtnTapped(_ sender: UIButton) {
        //Go to New Invoice 1 View Controller
        guard let newInvoiceVC1 = storyboard?.instantiateViewController(withIdentifier: "NewInvoice1VC") as? NewInvoice1VC else {return}
        navigationController?.pushViewController(newInvoiceVC1, animated: true)
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ManageInvoicesVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return invoicesDataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoicesDataArray[section].invoices.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceHeaderTblCell") as? InvoiceHeaderTblCell else {return UIView()}
        
        cell.configureCell(invoiceList: invoicesDataArray[section])
        
        return cell.contentView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceDetailsTblCell") as? InvoiceDetailsTblCell else {return UITableViewCell()}
        
        let invoicesData = invoicesDataArray[indexPath.section].invoices[indexPath.row]
        
        cell.configureCell(invoiceDetail: invoicesData, indexPath: indexPath, delegate: self)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if currentPageCount < maxPageCount && !isLoadingMoreInvoices {
            currentPageCount += 1
            getInvoiceListFor(pageNumber: currentPageCount, fromDate: selectedFromDateString, toDate: selectedToDateString)
        }
        
    }
    
}

//MARK: Invoice Detail Table Cell Delegate Implementation
extension ManageInvoicesVC: InvoiceDetailsTblCellDelegate {
    
    func didTapDeleteInvoiceBtn(invoiceDetails: Invoice, at indexPath: IndexPath) {
        
        invoicesDataArray[indexPath.section].invoices.remove(at: indexPath.row)
        let indexPath = IndexPath(item: indexPath.row, section: indexPath.section)
        invoiceTableView.deleteRows(at: [indexPath], with: .fade)
        deleteInvoice(for: invoiceDetails.id)
    }
    
    func didTapEditInvoiceBtn(invoiceDetails: Invoice, at indexPath: IndexPath) {
        guard let editInvoicevc = storyboard?.instantiateViewController(withIdentifier: "NewInvoice2VC") as? NewInvoice2VC else {return}
        editInvoicevc.invoiceToEditId = invoiceDetails.id
        navigationController?.pushViewController(editInvoicevc, animated: true)
    }
    
    func didTapSendEmailToCustomerBtn(invoiceDetails: Invoice, at indexpath: IndexPath) {
        sendMailToCustomer(forType: "invoice", invoiceId: invoiceDetails.id)
    }
    
}

//MARK: ALL DATE SELECTION LOGIC HERE
extension ManageInvoicesVC {
    
    @objc
    func selectFromDate(_ sender: UITapGestureRecognizer) {
        //print("From Date To Be Set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = true
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    @objc
    func selectToDate(_ sender: UITapGestureRecognizer) {
        //print("To Date to be set")
        guard let commonDatePickervc = STORYBOARDNAMES.COMMON_VIEW_STORYBOARD.instantiateViewController(withIdentifier: "CommonDatePickerVC") as? CommonDatePickerVC else {return}
        commonDatePickervc.modalPresentationStyle = .overCurrentContext
        commonDatePickervc.modalTransitionStyle = .crossDissolve
        commonDatePickervc.isFromDate = false
        commonDatePickervc.delegate = self
        present(commonDatePickervc, animated: true, completion: nil)
    }
    
    func setCurrentDateOnFromDate() {
        
        let todaysDate = Date()
        
        dateFormatter.dateFormat = "d"
        let currentDate = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let currentMonthYear = dateFormatter.string(from: todaysDate)
        
        
        dateFormatter.dateFormat = "EEEE"
        let currentWeekday = dateFormatter.string(from: todaysDate)
        
        fromDateLbl.text = currentDate
        fromMonthYearLbl.text = currentMonthYear
        fromWeekDayLbl.text = currentWeekday
        dateFormatter.dateFormat = formattedDateString
        selectedFromDateString = dateFormatter.string(from: todaysDate)
                
    }
    
    func setTomorrowDateOnToDate() {
        
        let nextDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        
        dateFormatter.dateFormat = "d"
        let tomorrowDate = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "MMMM yyyy"
        let tomorrowMonthYear = dateFormatter.string(from: nextDate!)
        
        
        dateFormatter.dateFormat = "EEEE"
        let tomorrowWeekday = dateFormatter.string(from: nextDate!)
        
        toDateLbl.text = tomorrowDate
        toMonthYearLbl.text = tomorrowMonthYear
        toWeekDayLbl.text = tomorrowWeekday
        dateFormatter.dateFormat = formattedDateString
        selectedToDateString = dateFormatter.string(from: nextDate!)
                
    }
    
    
}

//MARK: COMMON DATE PICKER VIEW CONTROLLER DELEGATE IMPLEMENTATION
extension ManageInvoicesVC: CommonDatePickerVCDelegate {
    
    func getSelectedDateFromPicker(date: String, monthYear: String, weekDay: String, isFromDate: Bool, originalSelectedDate: String) {
        
        if isFromDate {
            
            if originalSelectedDate == "" {
                setCurrentDateOnFromDate()
            }
            else {
                fromDateLbl.text = date
                fromMonthYearLbl.text = monthYear
                fromWeekDayLbl.text = weekDay
                selectedFromDateString = originalSelectedDate
            }
            
        }
        else {
            
            if originalSelectedDate == "" {
                setTomorrowDateOnToDate()
            }
            else {
                toDateLbl.text = date
                toMonthYearLbl.text = monthYear
                toWeekDayLbl.text = weekDay
                selectedToDateString = originalSelectedDate
            }
            
        }
        
        if selectedFromDateString != "" || selectedToDateString != "" { //Service call after date filteration is complete
            currentPageCount = 1
            isLoadingMoreInvoices = false
            invoicesDataArray.removeAll()
            getInvoiceListFor(pageNumber: currentPageCount, fromDate: selectedFromDateString, toDate: selectedToDateString)
            
        }
        
    }
    
}



//MARK: API SERVICE CALLS IMPLEMENTATION
extension ManageInvoicesVC {
    
    //Get All Invoices
    func getInvoiceListFor(pageNumber: Int, fromDate: String, toDate: String) {
        
        view.makeToastActivity(.bottom)
        
        isLoadingMoreInvoices = true
        
        let parameters: [String:Any] = ["page_number": pageNumber, "from_date": fromDate, "to_date": toDate]
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_invoice, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [weak self] (invoices: ListInvoice!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                
                DispatchQueue.main.async {
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
                
            }
            else {
                
                if let returnedResponse = invoices {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        
                        if let data = returnedResponse.data {
                            
                            self.maxPageCount = data.pageCount
                            
                            returnedResponse.data?.invoiceList.forEach({ (invoice) in
                                self.invoicesDataArray.append(invoice)
                            })
                            
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.invoiceTableView.reloadData()
                                self.emptyInvoiceStack.isHidden = self.invoicesDataArray.isEmpty ? false : true
                            }
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.invoiceTableView.reloadData()
                            self.emptyInvoiceStack.isHidden = self.invoicesDataArray.isEmpty ? false : true
                            
                        }
                        
                    }
                    
                }
                
            }
            self.isLoadingMoreInvoices = false
        }
        
    }
    
    func deleteInvoice(for id: Int) {
        
        view.makeToastActivity(.center)
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let params = ["invoice_id" : id]
        
        GenericWebservice.instance.getServiceData(url: Webservices.delete_invoice, method: .post, parameters: params, encodingType: URLEncoding.default, headers: headers) { [weak self] (deletedInvoice: DeleteInvoice!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = deletedInvoice {
                    
                    if returnedResponse.success == "true" {
                        //Success
                        DispatchQueue.main.async {
                            self.emptyInvoiceStack.isHidden = self.invoicesDataArray.isEmpty ? false : true
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                }
            }
            
            
        }
        
    }
    
    //TODO: Send Email To Customer Service
    func sendMailToCustomer(forType type: String, invoiceId: Int) {
        
        let parameters: [String:Any] = ["id" : invoiceId, "type": type]
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        GenericWebservice.instance.getServiceData(
            url: Webservices.send_email_to_customer,
            method: .post, parameters: parameters,
            encodingType: JSONEncoding.default,
            headers: headers) { [weak self] (returnedResponse: DeleteEstimate!, errorMessage) in
                guard let self = self else {return}
                
                self.view.makeToastActivity(.center)
                
                if let error = errorMessage {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(error, duration: 2.0, position: .bottom)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                }
                
        }
        
    }
    
}
