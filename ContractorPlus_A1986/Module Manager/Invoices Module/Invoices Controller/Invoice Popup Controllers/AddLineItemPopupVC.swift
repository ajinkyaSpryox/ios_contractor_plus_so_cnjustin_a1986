//
//  AddLineItemPopupVC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 04/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Toast_Swift

protocol AddLineItemPopupVCDelegate: class {
    
    func didSendAddLineItem(addLineItem: AddLineItem)
}

class AddLineItemPopupVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var laborTotalTextField: UITextField!
    @IBOutlet weak var supplyTotalTextField: UITextField!
    
    //Variables
    var addLineItemsArray = [AddLineItem]()
    weak var delegate: AddLineItemPopupVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        initialUiSetup()
    }
    
    fileprivate func initialUiSetup() {
        title = "Add Line Item Pop up"
        laborTotalTextField.underlined()
        supplyTotalTextField.underlined()
    }

}

//MARK: IB-ACTIONS
extension AddLineItemPopupVC {
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addBtnTapped(_ sender: UIButton) {
        //Insert values in the add item model array
        
        guard let description = descriptionTextField.text, let supplyTotal = supplyTotalTextField.text, let laborTotal = laborTotalTextField.text else {return}
        
        if description != "" && laborTotal != "" {
            
            let supplyTotal = (supplyTotal as NSString).integerValue
            let laborTotal = (laborTotal as NSString).integerValue
            
            delegate?.didSendAddLineItem(addLineItem: AddLineItem(id: 0, description: description, suppliesCount: supplyTotal, labourCount: laborTotal))
            
            descriptionTextField.text = ""
            supplyTotalTextField.text = ""
            laborTotalTextField.text = ""
            
            dismiss(animated: true, completion: nil)
            //print(addLineItemsArray)
        }
        else {
            self.view.makeToast("Description & Labor Total is required.", duration: 2.0, position: .bottom)
        }
        
    }
    
}
