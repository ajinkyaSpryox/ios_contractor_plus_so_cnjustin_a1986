//
//  AddDiscountPopupVC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 01/02/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Toast_Swift

protocol AddDiscountPopupVCDelegate : class {
    func didselectDiscount(value: Int, unit: String)
}

class AddDiscountPopupVC: UIViewController {

    @IBOutlet weak var discountValueSegment: UISegmentedControl!
    @IBOutlet weak var selectedDiscountValueLbl: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    
    //Variables
    var selectedSegmentUnit = "percent"
    var selectedValue = 0
    weak var delegate: AddDiscountPopupVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        valueTextField.underlined()
        discountValueSegment.selectedSegmentIndex = 0
        selectedDiscountValueLbl.text = "%"
        
    }
   
    
}

//MARK: TEXT FIELD DELEGATE IMPLEMENTATION


//MARK: IB-ACTIONS IMPLEMENTATION
extension AddDiscountPopupVC {
    
    @IBAction func discountSegmentChanged(_ sender: UISegmentedControl) {
        
        selectedSegmentUnit =  sender.selectedSegmentIndex == 0 ? "percent" : "value"
        selectedDiscountValueLbl.text = sender.selectedSegmentIndex == 0 ? "%" : "$"
        
    }
    
    @IBAction func doneBtnTapped(_ sender:UIButton) {
        
        if valueTextField.text != "" {
            selectedValue = Int(valueTextField.text!)!
            delegate?.didselectDiscount(value: selectedValue, unit: selectedSegmentUnit)
            dismiss(animated: true, completion: nil)
        }
        else {
            view.makeToast("Discount value can't be empty", duration: 2.0, position: .center)
        }
        
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
