//
//  NewInvoice1VC.swift
//  ContractorPlus_A1986
//
//  Created by Ajinkya Sonar on 03/01/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire

class NewInvoice1VC: UIViewController {
    
    @IBOutlet weak var estimateSelectionTextField: UITextField!
    
    //Variables
    var estimatePickerDataArr = [Estimate]()
    var selectedEstimateId = 0
    var estimatePicker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        
    }
    
    private func setupInitialUI() {
        serviceCalls()
        title = "NEW INVOICE"
        estimateSelectionTextField.delegate = self
    }
    
    private func serviceCalls() {
        getAllEstimateList()
    }
    
    //MARK: IB_ACTIONS
    @IBAction func createInvoiceBtnTapped(_ sender: UIButton) {
        //Go to New Invoice Controller 2
        guard let newInvoice2Vc = storyboard?.instantiateViewController(withIdentifier: "NewInvoice2VC") as? NewInvoice2VC else {return}
        newInvoice2Vc.esitmateId = selectedEstimateId
        navigationController?.pushViewController(newInvoice2Vc, animated: true)
        
    }
    
}

//MARK: UI TEXT FIELD DELEGATE IMPLEMENTATION
extension NewInvoice1VC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == estimateSelectionTextField {
            estimatePickerDataArr.count == 0 ? view.makeToast("No Estimate Found", duration: 2.0, position: .center) : addPickerToEstimateTextField()
        }
        else {
            return
        }
        
    }
    
    func addPickerToEstimateTextField() {
        let estimatePicker = UIPickerView()
        estimateSelectionTextField.inputView = estimatePicker
        estimatePicker.delegate = self
        estimatePicker.dataSource = self
    }
    
}

//MARK: UIPICKER VIEW DELEGATE IMPLEMENTATION
extension NewInvoice1VC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return estimatePickerDataArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return estimatePickerDataArr[row].displayNameInList
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        estimateSelectionTextField.text = estimatePickerDataArr[row].displayNameInList
        self.selectedEstimateId = estimatePickerDataArr[row].id
        
    }
    
}


//MARK: API SERVICE IMPLEMENTATIONS
extension NewInvoice1VC {
    
    func getAllEstimateList() {
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters = ["page_number": 0]
        
        GenericWebservice.instance.getServiceData(url: Webservices.list_estimate, method: .post, parameters: parameters, encodingType: URLEncoding.default, headers: headers) { [weak self] (listEstimates: ListEstimate!, errorMessage) in
            
            guard let self = self else {return}
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                if let returnedResponse = listEstimates {
                    
                    if returnedResponse.success == "true" {
                        //success
                        
                        if let data = returnedResponse.data {
                            
                            data.estimatesDetailData.forEach { (estimate) in
                                self.estimatePickerDataArr = estimate.estimates
                            }
                            
                        }
                         //print(self.estimatePickerDataArr)
                        
                    }
                    else {
                        //Failure
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                    }
                }
            }
            
            
        }
        
    }
    
}
