//
//  MyAccountEditVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 04/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class MyAccountEditVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userProfilePictureImageView: UIImageView!
    
    //Variables
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "MY ACCOUNT"
        
        firstNameTextField.text = defaults.string(forKey: first_name)
        lastNameTextField.text = defaults.string(forKey: last_name)
        emailTextField.text = defaults.string(forKey: email)
        
        let profileImageTap = UITapGestureRecognizer(target: self, action: #selector(userProfileImageTapped(_:)))
        userProfilePictureImageView.isUserInteractionEnabled = true
        userProfilePictureImageView.addGestureRecognizer(profileImageTap)
        guard let userProfileImageUrl = URL(string: "\(defaults.string(forKey: user_profile_photo) ?? "")") else {return}
        userProfilePictureImageView.kf.indicatorType = .activity
        userProfilePictureImageView.kf.setImage(with: userProfileImageUrl)
        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
    }

}

//MARK: TEXTFIELD DELEGATE IMPLEMENTATION
extension MyAccountEditVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
}

//MARK: IMAGE PICKER IMPLEMENTATION
extension MyAccountEditVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc
    func userProfileImageTapped(_ sender: UITapGestureRecognizer) {
        
        print("User Profile Button Tapped")
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }
            else {
                self.view.makeToast("Camera Not Available", duration: 2.0, position: .bottom)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        userProfilePictureImageView.image = image
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}


//MARK: IB-Actions
extension MyAccountEditVC {
    
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        guard let firstName = firstNameTextField.text, let lastname = lastNameTextField.text, let email = emailTextField.text else {return}
        
        if firstName != "" && lastname != "" && email != "" {
            guard let selectedImage = userProfilePictureImageView.image else {return}
            updateProfile(image: selectedImage)
            
        }
        else {
            view.makeToast("FirstName, LastName & Email are required", duration: 2.0, position: .bottom)
        }
        
    }
    
    @IBAction func changePasswordBtnTapped(_ sender: UIButton) {
        guard let changePasswordVc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC else {return}
        navigationController?.pushViewController(changePasswordVc, animated: true)
    }
    
    @IBAction func upgradeProBtnTapped(_ sender: UIButton) {
        
    }
    
}

//MARK: API SERVICE IMPLEMENTATION
extension MyAccountEditVC {
    
    //TODO: UPLOAD SELECTED AREA IMAGE TO SERVER
    func updateProfile(image: UIImage) {
        
        guard let firstName = firstNameTextField.text, let lastName = lastNameTextField.text, let email = emailTextField.text else {return}
        
        let headers = [
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        guard let imageData = image.jpegData(compressionQuality: 0.5) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        
        print(imageData)
        
        let parameters: Parameters = [
            "profile_picture" : "file.jpeg",
            "first_name" : firstName,
            "last_name" : lastName,
            "email" : email,
            "user_id" : defaults.integer(forKey: user_id)
        ]
        
        Alamofire.upload(multipartFormData: {multipartData in
            for(key,value) in parameters {
                multipartData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            
            multipartData.append(imageData, withName: "profile_picture", fileName: "file.jpeg", mimeType: "image/jpeg")
        },
                         //usingThreshold: UInt64.init(),
            to: Webservices.update_profile,
            method: .post,
            headers: headers,
            encodingCompletion: {result in
                
                switch result {
                case .success(let upload, _, _):
                    
                    self.view.makeToastActivity(.center)
                    
                    upload.uploadProgress(closure: { (progress) in
                        //print("Upload Progress: \(progress.fractionCompleted)")
                        //let progressPercent = Int(progress.fractionCompleted * 100)
                        //print(progressPercent)
                    })
                    
                    upload.responseJSON { response in
                        //print(response.result.value)
                        
                        guard let data = response.data else {return}
                        let decoder = JSONDecoder()
                        
                        do{
                            let returnedResponse = try decoder.decode(SocialLoginDetail.self, from: data)
                            
                            if returnedResponse.success == "true" {
                                print(returnedResponse.data)
//
                                self.defaults.set(returnedResponse.data.userDetails.profilePicture, forKey: user_profile_photo)
//                                self.defaults.set(returnedResponse.data.token, forKey: token)
//                                self.defaults.set(returnedResponse.data.role, forKey: role)
//                                self.defaults.set(returnedResponse.data.isOtpVerified, forKey: is_otp_verified)
//                                self.defaults.set(returnedResponse.data.userDetails.firstName, forKey: first_name)
//                                self.defaults.set(returnedResponse.data.userDetails.lastName, forKey: last_name)
//                                self.defaults.set(returnedResponse.data.userDetails.email, forKey: email)
//                                self.defaults.set(returnedResponse.data.userDetails.fullName, forKey: full_name)
//                                self.defaults.set(returnedResponse.data.userDetails.id, forKey: user_id)
//                                self.defaults.set(returnedResponse.data.userDetails.profilePicture, forKey: user_profile_photo)
                                
                                DispatchQueue.main.async {
                                    self.view.hideToastActivity()
                                    self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                                    
//                                    guard let userProfileImageUrl = URL(string: "\(self.defaults.string(forKey: user_profile_photo) ?? "")") else {return}
//                                    self.userProfilePictureImageView.kf.indicatorType = .activity
//                                    self.userProfilePictureImageView.kf.setImage(with: userProfileImageUrl)
//
//                                    self.firstNameTextField.text = self.defaults.string(forKey: first_name)
//                                    self.lastNameTextField.text = self.defaults.string(forKey: last_name)
//                                    self.emailTextField.text = self.defaults.string(forKey: email)
                                }
                                
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.view.hideToastActivity()
                                    self.view.makeToast("Unable to Update Profile", duration: 2.0, position: .bottom)
                                }
                            }
                            //print(self.areaImageUploadedImagesArr)
                        }
                        catch let jsonError {
                            //print(jsonError.localizedDescription)
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.view.makeToast(jsonError.localizedDescription, duration: 2.0, position: .bottom)
                            }
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    self.view.hideToastActivity()
                    self.view.makeToast(encodingError.localizedDescription, duration: 2.0, position: .bottom)
                }
                
        })
        
    }
    
}
