//
//  MyAccountVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 04/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Kingfisher

class MyAccountVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var firstNameLbl: UILabel!
    @IBOutlet weak var lastNameLbl: UILabel!
    @IBOutlet weak var userEmailLbl: UILabel!
    @IBOutlet weak var userProfilePicImageView: UIImageView!
    
    //Variables
    let defaults = UserDefaults.standard
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupInitialUI()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    private func setupInitialUI() {
        title = "MY ACCOUNT"
        firstNameLbl.text = defaults.string(forKey: first_name)
        lastNameLbl.text = defaults.string(forKey: last_name)
        userEmailLbl.text = defaults.string(forKey: email)
        guard let userProfileImageUrl = URL(string: "\(defaults.string(forKey: user_profile_photo) ?? "")") else {return}
        userProfilePicImageView.kf.indicatorType = .activity
        userProfilePicImageView.kf.setImage(with: userProfileImageUrl)
    }

}

//MARK: IB-ACTIONS
extension MyAccountVC {
    
    @IBAction func editBtnTapped(_ sender: UIButton) {
        guard let myAccountEditVc = storyboard?.instantiateViewController(withIdentifier: "MyAccountEditVC") as? MyAccountEditVC else {return}
        navigationController?.pushViewController(myAccountEditVc, animated: true)
    }
    
    @IBAction func changePasswordBtnTapped(_ sender: UIButton) {
        guard let changePasswordVc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC else {return}
        navigationController?.pushViewController(changePasswordVc, animated: true)
    }
    
    @IBAction func upgradeProBtnTapped(_ sender: UIButton) {
        
    }
    
}
