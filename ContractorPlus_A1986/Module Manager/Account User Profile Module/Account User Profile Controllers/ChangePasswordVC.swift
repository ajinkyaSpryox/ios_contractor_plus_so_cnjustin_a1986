//
//  ChangePasswordVC.swift
//  ContractorPlus_A1986
//
//  Created by Spryox on 04/03/20.
//  Copyright © 2020 SpryOX MacMini Admin. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import SwiftHash

class ChangePasswordVC: UIViewController {
    
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var retypePasswordTextField: UITextField!
    
    //Variables
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        title = "CHANGE PASSWORD"
        
        oldPasswordTextField.delegate = self
        newPasswordTextField.delegate = self
        retypePasswordTextField.delegate = self
    }
    
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        
        guard let oldPassword = oldPasswordTextField.text, let newPassword = newPasswordTextField.text, let retypePassword = retypePasswordTextField.text else {return}
        
        if oldPassword != "" && newPassword != "" && retypePassword != "" {
            
            if newPassword == retypePassword {
                changePassword(oldPassword: oldPassword, newPassword: newPassword, userId: defaults.integer(forKey: user_id))
            }
            else {
                view.makeToast("New Password & Retype Password dont match", duration: 2.0, position: .bottom)
            }
            
        }
        else {
            view.makeToast("All Fields must be filled", duration: 2.0, position: .bottom)
        }
        
    }

}

//MARK: Text Field Delegate Implementation
extension ChangePasswordVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true
    }
    
}

//MARK: API SERVICES IMPLEMENTATION
extension ChangePasswordVC {
    
    func changePassword(oldPassword: String, newPassword: String, userId: Int) {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(UserDefaults.standard.string(forKey: token)!)"
        ]
        
        let parameters: [String:Any] = ["user_id":userId, "old_password":MD5(oldPassword.lowercased()), "new_password":MD5(newPassword.lowercased())]
        
        view.makeToastActivity(.center)
        
        GenericWebservice.instance.getServiceData(url: Webservices.change_password, method: .post, parameters: parameters, encodingType: JSONEncoding.default, headers: headers) { (returnedResponse: DeleteEstimate!, errorMessage) in
            
            if let error = errorMessage {
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
            else {
                
                if returnedResponse.success == "true" {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                    
                }
                else {
                    DispatchQueue.main.async {
                        self.view.hideToastActivity()
                        self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                    }
                }
                
            }
            
            
        }
        
    }
    
}
